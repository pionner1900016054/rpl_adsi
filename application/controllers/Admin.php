<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_template');
        $this->set_konfigurasi();
        if ($this->session->isLogin == false) {
            redirect('login');
        }
        if ($this->session->role == 3) {
            redirect('desa');
        }
        // echo $this->session->role;
    }
    public function set_konfigurasi()
    {
        $data = $this->M_template->view_where('konfigurasies', array('id_konfigurasi' => 1))->result();
        $konfigurasi = array(
            'Nama_app' => $data[0]->Nama_app,
            'logo_app' => base_url() . 'logo/' . $data[0]->logo_app,
        );
        $this->session->set_userdata($konfigurasi);
    }
    public function index()
    {
        $session = ['func' => 'dash', 'wrap' => 'Dashboard'];
        $this->session->set_userdata($session);
        $data['user'] = $this->M_template->view_where('users', array('role' => 3))->result();
        $data['event'] = $this->M_template->view('events')->result();
        $data['berita'] = $this->M_template->view('artikels')->result();
        $data['transaksi'] = $this->M_template->join('transaksies', 'members.id_member=transaksies.id_member', 'members')->result();
        $data['transaksi_done'] = $this->M_template->view_where('transaksies', array('statustransaksi' => 2))->result();
        $data['transaksi_cancel'] = $this->M_template->view_where('transaksies', array('statustransaksi' => 3))->result();
        $data['produk_limit'] = $this->M_template->view_where('produks', array('stok <= ' => 10))->result();
        $graph = $this->M_template->graph()->result();
        $data['graph'] = json_encode($graph);
        $this->load->view('Admin/index', $data);
    }
    public function order()
    {
        $session = ['func' => 'orders', 'wrap' => 'Pesanan'];
        $this->session->set_userdata($session);
        $data['transaksi'] = $this->M_template->join('transaksies', 'members.id_member=transaksies.id_member', 'members')->result();
        $data['transaksi_done'] = $this->M_template->view_where('transaksies', array('statustransaksi' => 2))->result();
        $data['transaksi_cancel'] = $this->M_template->view_where('transaksies', array('statustransaksi' => 3))->result();
        $this->load->view('Admin/order/order', $data);
    }
  
    public function produk()
    {
        $session = ['func' => 'produk', 'wrap' => 'Produk'];
        $this->session->set_userdata($session);

        $data['produk'] = $this->M_template->view('produks')->result();
        $this->load->view('Admin/produk/produk', $data);
    }

    public function kegiatan()
    {
        $session = ['func' => 'kegiatan', 'wrap' => 'Kegiatan'];
        $this->session->set_userdata($session);

        $data['produk'] = $this->M_template->view('kegiatans')->result();
        $this->load->view('Admin/kegiatan/kegiatan', $data);
    }

    public function rekening()
    {
        $session = ['func' => 'rekening', 'wrap' => 'Bank'];
        $this->session->set_userdata($session);

        $data['rekening'] = $this->M_template->view('rekenings')->result();
        $this->load->view('Admin/rekening/rekening', $data);
    }
    public function artikel()
    {
        $session = ['func' => 'artikel', 'wrap' => 'Artikel'];
        $this->session->set_userdata($session);

        $data['artikel'] = $this->M_template->view('artikels')->result();
        $this->load->view('Admin/artikel/artikel', $data);
    }
    public function event()
    {
        $session = ['func' => 'event', 'wrap' => 'Event'];
        $this->session->set_userdata($session);

        $data['event'] = $this->M_template->view('events')->result();
        $this->load->view('Admin/event/event', $data);
    }
    public function user()
    {
        $session = ['func' => 'user', 'wrap' => 'Pengguna'];
        $this->session->set_userdata($session);

        $data['user'] = $this->M_template->get_user()->result();
        $this->load->view('Admin/user/user', $data);
    }
    public function konfigurasi()
    {
        $session = ['func' => 'konfigurasi', 'wrap' => 'Konfigurasi'];
        $this->session->set_userdata($session);

        $data['konfigurasi'] = $this->M_template->view('konfigurasies')->result();
        // print_r($data);
        // echo $data['konfigurasi']->id_konfigurasi;
        $this->load->view('Admin/konfigurasi/konfigurasi', $data);
    }
    public function galeri()
    {
        $session = ['func' => 'galeri', 'wrap' => 'Galeri Wedding'];
        $this->session->set_userdata($session);

        $config['base_url'] = base_url('Admin/galeri/');
        $config['total_rows'] = $this->M_template->view('galeries')->num_rows(); //total row
        $config['per_page'] = 8; //show record per halaman
        $config["uri_segment"] = 3; // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model.
        // $data['data']         = $this->mahasiswa_model->get_mahasiswa_list($config["per_page"], $data['page']);

        $data['pagination'] = $this->pagination->create_links();
        $data['galeri'] = $this->M_template->view_limit('galeries', $config["per_page"], $data['page'])->result();

        $this->load->view('Admin/galeri/galeri', $data);
    }
    public function profile()
    {
        $session = ['func' => 'dash', 'wrap' => 'Profil Anda'];
        $this->session->set_userdata($session);
        $data['profile'] = $this->M_template->join_where('users', 'Admins.id_Admin=users.id_Admin', 'Admins', array('users.id_Admin' => $this->session->id_Admin))->result();
        $this->load->view('Admin/profile/profile', $data);
    }
    public function update_foto()
    {
        $config = array(
            'upload_path' => './avatar/',
            'overwrite' => false,
            'remove_spaces' => true,
            'allowed_types' => 'png|jpg|gif|jpeg',
            'max_size' => 5000,
            'xss_clean' => true,
        );
        $this->load->library('upload');
        $this->upload->initialize($config);
        // if ($_FILES['file']['name'] != "") {
        if ($this->upload->do_upload('gambar')) {
            $file_data = $this->upload->data();
            $this->M_template->update('users', array('id_Admin' => $this->session->id_Admin), array('avatar' => $file_data['file_name']));
            $this->session->set_userdata('avatar', $file_data['file_name']);
        }
        redirect('Admin/profile');
    }
    public function ganti_password()
    {
        $user = $this->M_template->view_where('users', array('id_Admin' => $this->session->id_Admin, 'password' => md5($this->input->post('password_lama'))))->result();
        // print_r($user);
        // print_r($this->input->post());
        if (md5($this->input->post('password_lama')) == $user[0]->password) {
            $this->M_template->update('users', array('id_Admin' => $this->session->id_Admin), array('password' => md5($this->input->post('password_baru'))));
        }
        redirect('Admin/profile');
    }
    public function update_profil()
    {
        $updated = array(
            'Admin' => $this->input->post('ama'),
            'no_telpon' => $this->input->post('no_telpon'),
            'alamat' => $this->input->post('alamat'),
        );
        // print_r($updated);
        $this->M_template->update('Admins', array('id_Admin' => $this->session->id_Admin), $updated);
        $this->session->set_userdata('Admin', $this->input->post('Nama'));
        redirect('Admin/profile');

    }

}