<?php
ob_start();
defined('BASEPATH') or exit('No direct script access allowed');

class Desa extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_template');
        $this->set_konfigurasi();
        if ($this->session->role == 1 || $this->session->role == 2) {
            redirect('Admin');
        }
    }
    public function set_konfigurasi()
    {
        $data = $this->M_template->view_where('konfigurasies', array('id_konfigurasi' => 1))->result();
        $konfigurasi = array(
            'Nama_app' => $data[0]->Nama_app,
            'logo_app' => base_url() . 'logo/' . $data[0]->logo_app,
            'kontak_satu' => $data[0]->kontak_satu,
            'kontak_dua' => $data[0]->kontak_dua,
            'kontak_tiga' => $data[0]->kontak_tiga,
            'embed_maps' => $data[0]->embed_maps,
            'email' => $data[0]->email,
        );
        $this->session->set_userdata($konfigurasi);
    }
    public function index()
    {
        $this->session->set_flashdata('active', '');
        // $session=['func'=>'orders','wrap'=>'Orders'];
        // $this->session->set_userdata($session);
        $data['produk'] = $this->M_template->get_produk_index()->result();
        $data['tiket'] = $this->M_template->getDetailTiket()->result();
        $data['tiket_pilihan'] = $this->M_template->view_where('produks', array('kategori' => 'Tiket', 'produk' => 'Paket Pilihan'))->result();
        $data['galeri'] = $this->M_template->view_limit('galeries', 6, 0)->result();
        $data['kegiatan'] = $this->M_template->view('kegiatans')->result();

        $this->load->view('public/index2', $data);
    }
    public function read_profile($kategori)
    {
        $this->session->set_flashdata('active', 'profil');
        if ($kategori == 'Contact-Us') {
            $data['artikel'] = $this->M_template->view_where('artikels', array('kategori' => $kategori))->result();
            $data['contact'] = $this->M_template->view('konfigurasies')->result();
            $this->load->view('public/contact', $data);
        } else {
            $data['artikel'] = $this->M_template->view_where('artikels', array('kategori' => $kategori))->result();
            $this->load->view('public/artikel/detail', $data);
        }
    }
    public function galeri()
    {
        $this->session->set_flashdata('active', 'galeri');
        $config['base_url'] = base_url('desa/galeri/');
        $config['total_rows'] = $this->M_template->view('galeries')->num_rows(); //total row
        $config['per_page'] = 12; //show record per halaman
        $config["uri_segment"] = 3; // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

       

        $data['pagination'] = $this->pagination->create_links();
        $data['galeri'] = $this->M_template->view_limit('galeries', $config["per_page"], $data['page'])->result();
        $this->load->view('public/galeri/galeri', $data);
    }
    public function produk()
    {
        $this->session->set_flashdata('active', 'produk');
        $config['base_url'] = base_url('desa/produks/');
        $config['total_rows'] = $this->M_template->view_where('produks', array('kategori !=' => 'Tiket'))->num_rows(); //total row
        $config['per_page'] = 12; //show record per halaman
        $config["uri_segment"] = 3; // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

       
        $data['pagination'] = $this->pagination->create_links();
        $data['produk'] = $this->M_template->view_limit_where('produks', $config["per_page"], $data['page'], array('kategori !=' => 'Tiket'))->result();
        $this->load->view('public/produk/produk', $data);
    }
    public function tiket()
    {
        $this->session->set_flashdata('active', 'produk');
        $data['tiket'] = $this->M_template->view_where('produks', array('kategori' => 'Tiket'))->result();
        $data['kegiatan'] = $this->M_template->view('kegiatans')->result();

        $this->load->view('public/tiket/tiket', $data);
    }
    public function produk_detail($id)
    {
        $this->session->set_flashdata('active', 'produk');
        $data['produk'] = $this->M_template->view_where('produks', array('id_produk' => $id))->result();
        $this->load->view('public/produk/detail', $data);
    }
    public function read_artikel($id)
    {
        $this->session->set_flashdata('active', 'berita');
        $data['artikel'] = $this->M_template->view_where('artikels', array('id_artikel' => $id))->result();
        $this->load->view('public/artikel/detail', $data);
    }
    public function read_event($id)
    {
        $this->session->set_flashdata('active', 'berita');
        $data['event'] = $this->M_template->view_where('events', array('id_event' => $id))->result();
        $this->load->view('public/event/detail', $data);
    }
    public function new_desa()
    {
        $data['produk'] = $this->M_template->get_produk_index()->result();
        $data['tiket'] = $this->M_template->view_where('produks', array('kategori' => 'Tiket'))->result();
        $data['contact'] = $this->M_template->view('konfigurasies')->result();
        $this->load->view('public/index2', $data);
        // $this->load->view('tmp_pub/new_header');
    }
}