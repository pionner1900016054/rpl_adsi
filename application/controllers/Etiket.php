<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Etiket extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_template');
		$this->load->library('pdf');

		if ($this->session->isLogin == FALSE) {
			redirect('login');
		}
	}
	public function test_pdf($id){
		$auth=$this->M_template->authEtiket($id)->result();
		// echo $auth[0]->id_member;
		// echo $this->session->id_member;
		if ($auth[0]->id_member == $this->session->id_member) {
			// echo "codes";
			// code...
	  	$etiket=$this->M_template->getEtiket($id)->result();
	  	// print_r($etiket);
		foreach ($etiket as $key ) {
	  		$data['etiket']=$this->M_template->getEtiket($key->id_etiket)->result();
		  	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		  	$pdf->SetTitle('Etiket Kampung Sinau');
			$pdf->setPrintFooter(false);
			$pdf->setPrintHeader(false);
			$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
			$pdf->setPageOrientation('L');
			$pdf->AddPage('');
			$pdf->Write(0, 'E-Tiket '.$key->produk, '', 0, 'L', true, 0, false, false, 0);
			$pdf->SetFont('');
				 
			// $tabel = '
			// <table>
			// 	<tr>
			// 		<td colspan="2"><h3 align="center">Kode Unik Tiket Anda</h3></td>
			// 	</tr>
			// 	<tr>
			// 		<td colspan="2"><h5 align="center">'.$key->kodetiket.'</h5><br></td>
			// 	</tr>
			// 	<tr align="center">
			// 		<td colspan="2"><br><img src="'.base_url().'picture/'.$key->gambar.'" height="200" width="auto"><br></td>
			// 	</tr>
			// 	<tr>
			//             <th> <b>Order by Member</b> </th>
			//             <th> '.$key->Nama_etiket.' </th>
			//     </tr>
			//     <tr>
			//             <td> <b>Nama Tiket</b> </td>
			//             <td> '.$key->produk.' </td>
			//     </tr>
			//     <tr>
			//             <td> <b>Tanggal Booking/Berlaku pada</b> </td>
			//             <td> '.date('d M Y',strtotime($key->booking)).' </td>
			//     </tr>
			// </table>
			// <p style="font-size:8px">PDF Created at '.date('d M Y H:i:s').'</p>
			// ';
	  		// $html=$this->load->view('pdf/etiket',$data);
			ob_start();
			$pdf->writeHTML($this->load->view('pdf/etiket',$data,true), true, false, true, false, '');
			ob_end_clean();
			$pdf->Output($key->etiket.'.pdf', 'I');
			// echo "string";
		}
		}else{
			echo "You're not allowed to access";
		}
	}
}