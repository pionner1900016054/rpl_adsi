<?php 

/**
 * 
 */
class Galeri extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_template');
		if ($this->session->isLogin == FALSE) {
			redirect('login');
		}
		if ($this->session->role == 3) {
			redirect('desa');
		}
		$session=['func'=>'galeri','wrap'=>'Galeri'];
		$this->session->set_userdata($session);
	}
	public function tambah()
	{
		// $data['galeri']=$this->M_template->view('galeri')->result();
		$this->load->view('Admin/galeri/tambah');
	}
	public function save()
	{
		$this->form_validation->set_rules('Nama', 'judul galeri', 'required');

		if ($this->form_validation->run() == FALSE){
			$this->load->view('Admin/galeri/tambah');
        }else{
            $config= array(
	            'upload_path' => './galeri/',
	            'overwrite' => false,
	            'remove_spaces' => true,
	            'allowed_types' => 'png|jpg|gif|jpeg',
	            'max_size' => 10000,
	            'xss_clean' => true,
	        );
	        $this->load->library('upload');
	        $this->upload->initialize($config);
	        // if ($_FILES['file']['name'] != "") {
	            if ($this->upload->do_upload('gambar')) {
	                $file_data = $this->upload->data();
	                $data=array(
	                    'judul'  			=> $this->input->post('Nama'),
	                    'gambar'   			=> $file_data['file_name'],
	                );
	                // print_r($data);
	                $this->M_template->insert('galeries',$data);

	                redirect('Admin/galeri');
	            }else{
					$this->load->view('Admin/galeri/tambah');
					// echo "Sini";
	            }
	        // }
        }
	}
	public function edit($id)
	{
		$data['galeri']=$this->M_template->view_where('galeries',array('id_galeri'=>$id))->result();
		$this->load->view('Admin/galeri/edit',$data);
	}
	public function update($id)
	{
		$this->form_validation->set_rules('Nama', 'judul galeri', 'required');

		if ($this->form_validation->run() == FALSE){
			$data['galeri']=$this->M_template->view_where('galeries',array('id_galeri'=>$id))->result();
			$this->load->view('galeri/edit',$data);
        }else{
            $config= array(
	            'upload_path' => './galeri/',
	            'overwrite' => false,
	            'remove_spaces' => true,
	            'allowed_types' => 'png|jpg|gif|jpeg',
	            'max_size' => 10000,
	            'xss_clean' => true,
	        );
	        $this->load->library('upload');
	        $this->upload->initialize($config);
	        if ($_FILES['gambar']['name'] != "") {
	            if ($this->upload->do_upload('gambar')) {
	                $file_data = $this->upload->data();
	                $data=array(
	                    'judul'  			=> $this->input->post('Nama'),
	                    'gambar'   			=> $file_data['file_name'],
	                );

	                $this->M_template->update('galeries',array('id_galeri'=>$id),$data);

	                redirect('Admin/galeri');
	            }else{
					$data['galeri']=$this->M_template->view_where('galeries',array('id_galeri'=>$id))->result();
					$this->load->view('galeri/edit',$data);
	            }
	        }else{
	        	$data=array(
	            	'judul'  			=> $this->input->post('Nama'),
	            );
	            $this->M_template->update('galeries',array('id_galeri'=>$id),$data);
	            // redirect('Admin/galeri');
	        }
        }
	}
	public function hapus($id)
	{
		// echo "bisa hapus";
		$this->M_template->delete('galeries',array('id_galeri'=>$id));
		redirect('Admin/galeri');
	}

}