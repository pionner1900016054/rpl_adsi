<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfigurasi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_template');
		if ($this->session->isLogin == FALSE) {
			redirect('login');
		}
		if ($this->session->role == 3) {
			redirect('desa');
		}
		$session=['func'=>'konfigurasi','wrap'=>'Konfigurasi Wedding'];
		$this->session->set_userdata($session);
	}
	public function update($id)
	{
		$this->form_validation->set_rules('Nama', 'Nama Aplikasi', 'required');
		// $this->form_validation->set_rules('embed', 'embed', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		// $this->form_validation->set_rules('gambar', 'Gambar', 'required');

		if ($this->form_validation->run() == FALSE){
			$data['konfigurasi']=$this->M_template->view_where('konfigurasies',array('id_konfigurasi'=>$id))->result();
			$this->load->view('konfigurasi/edit',$data);
        }else{
            $config= array(
	            'upload_path' => './logo/',
	            'overwrite' => false,
	            'remove_spaces' => true,
	            'allowed_types' => 'png|jpg|gif|jpeg',
	            'max_size' => 10000,
	            'xss_clean' => true,
	        );
	        $this->load->library('upload');
	        $this->upload->initialize($config);
	        if ($_FILES['gambar']['name'] != "") {
	            if ($this->upload->do_upload('gambar')) {
	                $file_data = $this->upload->data();
	                $data=array(
	                    'Nama_app'  	=> $this->input->post('Nama'),
	                    'email'	 		=> $this->input->post('email'),
	                    'embed_maps' 	=> $this->input->post('embed'),
	                    'kontak_satu' 	=> $this->input->post('kontak_satu'),
	                    'kontak_dua' 	=> $this->input->post('kontak_dua'),
	                    'kontak_tiga' 	=> $this->input->post('kontak_tiga'),
	                    'logo_app'    	=> $file_data['file_name']
	                );
	                // print_r($data);

	                $this->M_template->update('konfigurasies',array('id_konfigurasi'=>$id),$data);

	                redirect('Admin/konfigurasi');
	            }else{
					$data['konfigurasi']=$this->M_template->view_where('konfigurasies',array('id_konfigurasi'=>$id))->result();
					$this->load->view('konfigurasi/konfigurasi',$data);
	            }
	        }else{
	        	$data=array(
	                	'
	            );
	            $this->M_template->update('konfigurasies',array('id_konfigurasi'=>$id),$data);
	            redirect('Admin/konfigurasi');
	        }
        }
	}
}