<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_template');
			require APPPATH.'libraries/phpmailer/src/Exception.php';
	        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
	        require APPPATH.'libraries/phpmailer/src/SMTP.php';
	    $this->set_konfigurasi();
	}
	public function set_konfigurasi()
	{
		$data=$this->M_template->view_where('konfigurasies',array('id_konfigurasi'=>1))->result();
		$konfigurasi=array(
			'Nama_app'=>$data[0]->Nama_app,
			'logo_app'=>base_url().'logo/'.$data[0]->logo_app,
            'kontak_satu'=>$data[0]->kontak_satu,
            'kontak_dua'=>$data[0]->kontak_dua,
            'kontak_tiga'=>$data[0]->kontak_tiga,
            'embed_maps'=>$data[0]->embed_maps,
            'email'=>$data[0]->email,
		);
		$this->session->set_userdata($konfigurasi);
	}
	public function index()
	{
		$this->load->view('login/new_login');
	}
	public function lupa_password()
	{
		$this->load->view('login/lupa_password');
	}
	public function forgot()
	{
		$response = false;
	    $mail = new PHPMailer();

		$cek=$this->M_template->join_where('users','members.id_member=users.id_member','members',array('users.email'=>$this->input->post('email'),'members.no_telpon'=>$this->input->post('no_telpon')))->result();
		// print_r($cek);
		if (count($cek) == 1) {

	        $mail->isSMTP();
	        $mail->Host     = 'kampungsinau.com'; //sesuaikan sesuai Nama domain hosting/server yang digunakan
	        $mail->SMTPAuth = true;
	        $mail->Username = 'Admin@kampungsinau.com'; // user email
	        $mail->Password = 'bedohoAdmin'; // password email
	        $mail->SMTPSecure = 'ssl';
	        $mail->Port     = 465;

	        $mail->setFrom('Admin@kampungsinau.com', $this->session->Nama_app); // user email
	        $mail->addReplyTo('Admin@kampungsinau.com', $this->session->Nama_app);

	        $mail->addAddress($cek[0]->email);
	        $mail->Subject ='Buat Kata Sandi Baru';

	        $mail->isHTML(true);

	        $mail->Body ='Permintaan kata sandi baru anda akan segera kami proses. Silahkan klik <a href="'.base_url().'login/new_pass/'.$cek[0]->token.'" >di sini!</a>';

	        if(!$mail->send()){
	            echo $mail->ErrorInfo."<br>";
	            echo 'Error di server. Silahkan hubungi kontak kami.';
	        }else{
	           // $this->load->view('login/lupa_password');
				$this->session->set_flashdata('error_log','<div class="alert alert-success" role="alert">Silahkan cek email anda!</div>');
	           redirect('login/lupa_password');
	        }
		}else{
	           // $this->load->view('login/lupa_password');
			$this->session->set_flashdata('error_log','<div class="alert alert-danger" role="alert">Email atau No Telepon anda salah</div>');
	       redirect(base_url().'login/lupa_password');

		}
	}
	public function register()
	{
		$this->load->view('login/new_register');
	}
	public function check()
	{
		$login=$this->M_template->view_where('users',
			array(
				'email'=>$this->input->post('email'),
				'password'=>md5($this->input->post('password'))
			))->result();
		if (count($login)==1) {
			//check status akun 1 aktif 2 off 3 banned
			if ($login[0]->status == 1) {
				if ($login[0]->role == 3) {
					$member=$this->M_template->view_where('members',array('id_member'=>$login[0]->id_member))->result();
					$session=array(
						'id_user'	=> $login[0]->id_user,
						'id_member'	=> $login[0]->id_member,
						'member'	=> $member[0]->member,
						'email'		=> $login[0]->email,
						'avatar'	=> $login[0]->avatar,
						'notelpon'	=> $member[0]->no_telpon,
						'role'		=> $login[0]->role,
						'isLogin'	=> TRUE
					);
					$this->session->set_userdata($session);
					redirect('desa');
				}elseif ($login[0]->role == 1 || $login[0]->role == 2){
					$Admin=$this->M_template->view_where('Admins',array('id_Admin'=>$login[0]->id_Admin))->result();
					$session=array(
						'id_user'	=> $login[0]->id_user,
						'id_Admin'	=> $login[0]->id_Admin,
						'Admin'		=> $Admin[0]->Admin,
						'avatar'	=> $login[0]->avatar,
						'role'		=> $login[0]->role,
						'isLogin'	=> TRUE
					);
					$this->session->set_userdata($session);
					redirect('Admin');
				}
			}else{
				$this->session->set_flashdata('error_log','<div class="alert alert-danger" role="alert">Akun anda diblokir, silahkan hubungi kontak kami untuk membuka blokir</div>');
				redirect('login');
			}
		}else{
			$this->session->set_flashdata('error_log','<div class="alert alert-danger" role="alert">Email atau Password anda salah</div>');
			redirect('login');
		}
	}
	public function register_proses()
	{
		$this->form_validation->set_rules('Nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('notelpon', 'notelpon', 'required');
		if ($this->form_validation->run() == FALSE){
			$this->load->view('login/new_register');
			echo validation_errors();
        }else{
        	$check_email = $this->M_template->view_where('users',array('email'=>$this->input->post('email')))->num_rows();
        	if ($check_email > 0) {
        		// $error_email_used
        		$this->session->set_flashdata('error_email_used','Email sudah digunakan!');
				$this->load->view('login/register');
        	}else{
	        	$register_member = array(
	        		'member'=>$this->input->post('Nama'),
	        		'no_telpon'=>$this->input->post('notelpon'),
	        	);

	        	$member_id 	= $this->M_template->insert_id('members',$register_member);
	        	$register_user	= array(
	        		'id_member'=>$member_id,
	        		'email'=>$this->input->post('email'),
	        		'password'=>md5($this->input->post('password')),
	        		'token'=>sha1($this->input->post('email')),
	        		'status'=> 1,
	        		'role'=> 3
	        	);
	        	// print_r($register_member);
	        	// print_r($register_user);

	        	$this->M_template->insert('users',$register_user);

        		$this->session->set_flashdata('coba_login','<div class="alert alert-success" role="alert">Terimakasih telah mendaftar, silahkan login!</div>');
				redirect('login');
        	}
        }
	}
	public function new_pass($token)
	{
		$data['token']='<input type="hidden" name="token" value="'.$token.'">';
		$this->load->view('login/new_pass',$data);
	}
	public function new_pass_save()
	{
		$cek=$this->M_template->view_where('users',array('token'=>$this->input->post('token')))->result();
		print_r($cek);
		// print_r($this->input->post());
		// echo sha1($this->input->post('email'))."<br>";
		// echo $this->input->post('token');
		if ($cek[0]->token == $this->input->post('token')) {
			$this->M_template->update('users',array('id_user'=>$cek[0]->id_user),array('password'=>md5($this->input->post('password'))));
			$this->session->set_flashdata('coba_login','<div class="alert alert-success" role="alert">Kata sandi berhasil diperbarui, silahkan coba login!</div>');
				redirect('login');
		}
	}
	public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }
    // public function test()
    // {
    // 	$this->load->view('login/new_register');
    // 	// $this->load->view('login/new_login');
    // }
}
