<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends CI_Controller {

    /**
     * Kirim email dengan SMTP Gmail.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_template');
    }
    public function send($id_member,$type,$id_transaksi)
    {
    $set_from=$set_to=$subjek=$message='';
        if ($type == 1) { //Email Pembayaran Valid
            $data_member=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
            $data_members=$this->M_template->view_where('members',array('id_member'=>$id_member))->result();
            $data_transaksi=$this->M_template->view_where('transaksies',array('id_transaksi'=>$id_transaksi))->result();
            $set_to=$data_member[0]->email;
            // $set_from='404notfounddddddd@gmail.com';
            $subjek="Verifikasi Pembayaran";
            $message="Hai,".$data_members[0]->member."! \r\nPembayaran Anda Valid, pesanan akan segera dikemas dan dikirim!";
        }elseif ($type == 2) { //Email Pembayaran Tidak Valid
            $data_member=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
            $data_members=$this->M_template->view_where('members',array('id_member'=>$id_member))->result();
            $set_to=$data_member[0]->email;
            // $set_from='404notfounddddddd@gmail.com';
            $subjek="Verifikasi Pembayaran Tidak Valid";
            $message="Hai,".$data_members[0]->member."! \r\nPembayaran Anda Tidak Valid, silahkan upload Ulang bukti pembayaran!";
        }elseif ($type == 3) { // Email Pesanan Dikirim
            $data_member=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
            $data_members=$this->M_template->view_where('members',array('id_member'=>$id_member))->result();
            $data_transaksi=$this->M_template->view_where('alur_transaksies',array('id_transaksi'=>$id_transaksi))->result();
            $set_to=$data_member[0]->email;
            // print_r($data_transaksi);
            // $set_from='404notfounddddddd@gmail.com';
            $subjek="Pesanan Dikirim";
            $message="Hai,".$data_members[0]->member."! \r\n Pesanan anda sudah dikirim! No resi ".$data_transaksi[0]->noresi;
        }elseif ($type == 4) { //Email Pesanan Diterima ke Admin
            $data_member=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
            $data_members=$this->M_template->view_where('members',array('id_member'=>$id_member))->result();
            $data_transaksi=$this->M_template->view_where('transaksies',array('id_transaksi'=>$id_transaksi))->result();
            $set_to=$data_member[0]->email;
            $subjek="Pesanan Sudah Diterima";
            // $set_from='404notfounddddddd@gmail.com';
            $message="Pesanan Users sudah diterima! id transaksi = ".$data_transaksi[0]->id_transaksi;
        }elseif ($type == 5) { // Email Order masuk ke Admin
            $data_transaksi=$this->M_template->view_where('transaksies',array('id_transaksi'=>$id_transaksi))->result();
            $set_to='404notfounddddddd@gmail.com';
            // $set_from='404notfounddddddd@gmail.com';
            $subjek="Orderan Masuk";
            $message="Ada orderan masuk! id transaksi = ".$data_transaksi[0]->id_transaksi." Pada tanggal ".$data_transaksi[0]->date;
        }elseif ($type == 6) { // Email Segera lakukan Pembayaran Valid ke users
            $data_member=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
            $data_members=$this->M_template->view_where('members',array('id_member'=>$id_member))->result();
            $set_to=$data_member[0]->email;
            $subjek="Pembayaran";
            // $set_from='404notfounddddddd@gmail.com';
            $message="Segera selesaikan pembayaran kemudian upload bukti pembayaran di menu Transaksi Pesanan!";
        }
        elseif ($type == 7) { // Email ada butki pembayaran baru ke Admin
            $data_transaksi=$this->M_template->view_where('transaksies',array('id_transaksi'=>$id_transaksi))->result();
            $set_to='404notfounddddddd@gmail.com';
            // $set_from='404notfounddddddd@gmail.com';
            $subjek="Segera Verifikasi Bukti Pembayaran Baru";
            $message="Ada bukti pembayaran baru saja diupload!id transaksi = ".$data_transaksi[0]->id_transaksi;
        }
      // Konfigurasi email
        $config = array(
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'protocol'  => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_user' => '404notfounddddddd@gmail.com',  // Email gmail
            'smtp_pass'   => 'innovillage01',  // Password gmail
            'smtp_crypto' => 'ssl',
            'smtp_port'   => 465,
            'crlf'    => "\r\n",
            // 'newline' => "\r\n"
        );

        // Load library email dan konfigurasinya
        $this->load->library('email', $config);
        // $this->load->library('email');
        // $this->load->initialize($config);

        // Email dan Nama pengirim
        $this->email->from('404notfounddddddd@gmail.com', $this->session->Nama_app);

        // Email penerima
        $this->email->to($set_to); // Ganti dengan email tujuan

        // Lampiran email, isi dengan url/path file
        // $this->email->attach('https://masrud.com/content/images/20181215150137-codeigniter-smtp-gmail.png');

        // Subject email
        $this->email->subject($subjek);

        // Isi email
        $this->email->message($message);
        // echo "adasda";
        // Tampilkan pesan sukses atau error
        if ($this->email->send()) {
            echo 'Sukses! email berhasil dikirim.';
            echo $set_to;
            echo "<br>";
            echo $subjek;
            echo "<br>";
            echo $message;
        } else {
            echo 'Error! email tidak dapat dikirim.';
        }
    }
    public function test()
    {
        echo $this->session->role;
        echo "<BR>";
        echo str_pad(rand(0, pow(10, 3)-1), 3, '0', STR_PAD_LEFT);
    }
    public function call_test()
    {
        // $this->load->library('./controllers/Sendmail');
        require APPPATH.'./controllers/Sendmail.php';
        $sendmail = new Sendmail();

        $sendmail->sendetiket(1,4);
        // echo APPPATH.'controllers/Sendmail.php';
    }
}