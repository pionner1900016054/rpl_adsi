<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Order extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_template');
		$this->load->library('pdf');
		require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';
		if ($this->session->isLogin == FALSE) {
			redirect('login');
		}
		if ($this->session->role == 3) {
			redirect('desa');
		}
		$session=['func'=>'orders','wrap'=>'Orders'];
		// $session=['func'=>'order','wrap'=>'order'];
		$this->session->set_userdata($session);
	}
	public function detail($id)
	{
		$data['transaksi']=$this->M_template->get_detail_order(array('id_transaksi'=>$id))->result();
	  	$data['alur_transaksi']=$this->M_template->view_where('alur_transaksies',array('id_transaksi'=>$id))->result();
	  	$data['produk_order']=$this->M_template->join_where('produk_orders','produk_orders.id_produk=produks.id_produk','produks',array('id_transaksi'=>$id))->result();
	  	$data['etiket']=$this->M_template->getEtiketTransaksi($id)->result();
	  	$this->load->view('Admin/order/detail',$data);
	}
	public function ubahverifikasibukti($id,$status)
	  {
	  	$this->M_template->update('transaksies',array('id_transaksi'=>$id),array('verifikasibukti'=>$status));
	  	$this->M_template->update('alur_transaksies',array('id_transaksi'=>$id),array('dikemas'=>1,'tanggal_dikemas'=>date('Y-m-d H:i:s')));

	  	$check_tiket=$this->M_template->view_where('produk_orders',array('id_transaksi'=>$id,'kategori'=>'Tiket'))->result();
	  	$new_stok=$this->M_template->view_where('produk_orders',array('id_transaksi'=>$id))->result();
		$Nama_member=$this->M_template->getNamaEtiket($id)->result();
	  	$get_member=$this->M_template->view_where('transaksies',array('id_transaksi'=>$id))->result();
// 		echo $check_tiket[0]->jumlah;
	  	if ($status == 1) {
	  		foreach ($new_stok as $key) {
				$get_stok=$this->M_template->view_where('produks',array('id_produk'=>$key->id_produk))->result();
				$update_stok=$get_stok[0]->stok-$key->jumlah;
  				$this->M_template->update('produks',array('id_produk'=>$key->id_produk),array('stok'=>$update_stok));
	  		}
		  	if (count($check_tiket) >0) {
		  		$no=1;
		  		foreach ($check_tiket as $key) {
			  			$check_tiket_after=$this->M_template->view_where('etikets',array('id_produk_orders'=>$key->id_produk_order))->result();
			  			// echo count($check_tiket_after);
			  			// echo "string";
			  		if (count($check_tiket_after) == 0) {
		  				for ($i=0; $i < $check_tiket[0]->jumlah ; $i++) { 
			  			// print_r($check_tiket_after);
				 			$data_etiket=array(
				 				'id_produk_orders'=>$key->id_produk_order,
				 				'Nama_etiket'=>$Nama_member[0]->member,
				 				'kodetiket'=>$id.$key->id_produk_order.date('YmdHis'),
				 				'etiket'=>'Tiket '.$Nama_member[0]->member.'-'.$id.'-'.$key->id_produk_order.'-'.date('YmdHis').$no,
				 				'booking'=>$Nama_member[0]->tanggal_booking,
				 				'expired'=>0,
				 			);
				 			echo "<pre>";
				 			print_r($data_etiket);
				 			echo "</pre>";
				 			$this->M_template->insert('etikets',$data_etiket);
				 			$no++;
		  				}
			  		}
			  			// echo "<pre>";
			  			// print_r($check_tiket_after);
			  			// echo "</pre>";
			  	}
			 // 	$get_member=$this->M_template->view_where('transaksies',array('id_transaksi'=>$id))->result();
		        $this->sendetiket($get_member[0]->id_member,$id);
		  	}
		  //	$get_member=$this->M_template->view_where('transaksies',array('id_transaksi'=>$id))->result();
		    $this->send_mail($get_member[0]->id_member,1,$id);
	  	}else{
		    $this->send_mail($get_member[0]->id_member,2,$id);
	  		$this->M_template->update('transaksies',array('id_transaksi'=>$id),array('statusbukti'=>0));
	  	}
	  	redirect('order/detail/'.$id);
	  }
	public function test_pdf($id){
	  	$etiket=$this->M_template->getEtiket($id)->result();
	  	// print_r($etiket);
		foreach ($etiket as $key ) {
	  		$data['etiket']=$this->M_template->getEtiket($key->id_etiket)->result();
		  	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		  	$pdf->SetTitle('Etiket Kampung Sinau');
			$pdf->setPrintFooter(false);
			$pdf->setPrintHeader(false);
			$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
			$pdf->setPageOrientation('L');
        	$pdf->Image('https://images.unsplash.com/photo-1541273438837-d62849aa13ce?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80', 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
			$pdf->AddPage('');
			$pdf->Write(0, 'E-Tiket '.$key->produk, '', 0, 'L', true, 0, false, false, 0);
			$pdf->SetFont('');
				 
			// $tabel = '
			// <table>
			// 	<tr>
			// 		<td colspan="2"><h3 align="center">Kode Unik Tiket Anda</h3></td>
			// 	</tr>
			// 	<tr>
			// 		<td colspan="2"><h5 align="center">'.$key->kodetiket.'</h5><br></td>
			// 	</tr>
			// 	<tr align="center">
			// 		<td colspan="2"><br><img src="'.base_url().'picture/'.$key->gambar.'" height="200" width="auto"><br></td>
			// 	</tr>
			// 	<tr>
			//             <th> <b>Order by Member</b> </th>
			//             <th> '.$key->Nama_etiket.' </th>
			//     </tr>
			//     <tr>
			//             <td> <b>Nama Tiket</b> </td>
			//             <td> '.$key->produk.' </td>
			//     </tr>
			//     <tr>
			//             <td> <b>Tanggal Booking/Berlaku pada</b> </td>
			//             <td> '.date('d M Y',strtotime($key->booking)).' </td>
			//     </tr>
			// </table>
			// <p style="font-size:8px">PDF Created at '.date('d M Y H:i:s').'</p>
			// ';
	  		// $html=$this->load->view('pdf/etiket',$data);
			ob_start();
			$pdf->writeHTML($this->load->view('pdf/etiket',$data,true), true, false, true, false, '');
			ob_end_clean();
			$pdf->Output($key->etiket.'.pdf', 'I');
			// echo "string";
		}
	}
	public function cancel_restore($id)
	{
		$produk_order=$this->M_template->view_where('produk_orders',['id_transaksi'=>$id])->result();
		foreach($produk_order as $key ){
		    $produk=$this->M_template->view_where('produks',['id_produk'=>$key->id_produk])->result();
		    $final_stok=$produk[0]->stok + $key->jumlah;
		    echo $final_stok;
		    $this->M_template->update('produks',['id_produk'=>$key->id_produk],array('stok'=>$final_stok));
		}
// 		print_r($produk_order);
		$this->M_template->update('transaksies',array('id_transaksi'=>$id),array('statustransaksi'=>3));
		redirect('Admin/order/');
	}
	public function ubahalurtransaksi($id,$resi)
	{
		$this->M_template->update('alur_transaksies',array('id_transaksi'=>$id),array('noresi'=>$resi,'dikirim'=>1,'tanggal_dikirim'=>date('Y-m-d H:i:s')));
		$this->M_template->update('transaksies',array('id_transaksi'=>$id),array('statustransaksi'=>1));
		$this->send_mail($this->session->id_member,3,$id_transaksi);
		redirect('order/detail/'.$id);
	}
	private function send_mail($id_member,$type,$id_transaksi)
    {
        $response = false;
        $mail = new PHPMailer();

    $set_from=$set_to=$subjek=$message='';
        if ($type == 1) { //Email Pembayaran Valid
            $data_member=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
            $data_members=$this->M_template->view_where('members',array('id_member'=>$id_member))->result();
            $data_transaksi=$this->M_template->view_where('transaksies',array('id_transaksi'=>$id_transaksi))->result();
            $set_to=$data_member[0]->email;
            // $set_from='404notfounddddddd@gmail.com';
            $subjek="Verifikasi Pembayaran";
            $message="Hai,".$data_members[0]->member."! \r\nPembayaran Anda Valid, pesanan akan segera dikemas dan dikirim!";
        }elseif ($type == 2) { //Email Pembayaran Tidak Valid
            $data_member=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
            $data_members=$this->M_template->view_where('members',array('id_member'=>$id_member))->result();
            $set_to=$data_member[0]->email;
            // $set_from='404notfounddddddd@gmail.com';
            $subjek="Verifikasi Pembayaran Tidak Valid";
            $message="Hai,".$data_members[0]->member."! \r\nPembayaran Anda Tidak Valid, silahkan upload Ulang bukti pembayaran!";
        }elseif ($type == 3) { // Email Pesanan Dikirim
            $data_member=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
            $data_members=$this->M_template->view_where('members',array('id_member'=>$id_member))->result();
            $data_transaksi=$this->M_template->view_where('alur_transaksies',array('id_transaksi'=>$id_transaksi))->result();
            $set_to=$data_member[0]->email;
            // print_r($data_transaksi);
            // $set_from='404notfounddddddd@gmail.com';
            $subjek="Pesanan Dikirim";
            $message="Hai,".$data_members[0]->member."! \r\n Pesanan anda sudah dikirim! No resi ".$data_transaksi[0]->noresi;
        }elseif ($type == 4) { //Email Pesanan Diterima ke Admin
            $data_member=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
            $data_members=$this->M_template->view_where('members',array('id_member'=>$id_member))->result();
            $data_transaksi=$this->M_template->view_where('transaksies',array('id_transaksi'=>$id_transaksi))->result();
            $set_to=$data_member[0]->email;
            $subjek="Pesanan Sudah Diterima";
            // $set_from='404notfounddddddd@gmail.com';
            $message="Pesanan Users sudah diterima! id transaksi = ".$data_transaksi[0]->id_transaksi;
        }elseif ($type == 5) { // Email Order masuk ke Admin
            $data_transaksi=$this->M_template->view_where('transaksies',array('id_transaksi'=>$id_transaksi))->result();
            $set_to='404notfounddddddd@gmail.com';
            // $set_from='404notfounddddddd@gmail.com';
            $subjek="Orderan Masuk";
            $message="Ada orderan masuk! id transaksi = ".$data_transaksi[0]->id_transaksi." Pada tanggal ".$data_transaksi[0]->date;
        }elseif ($type == 6) { // Email Segera lakukan Pembayaran Valid ke users
            $data_member=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
            $data_members=$this->M_template->view_where('members',array('id_member'=>$id_member))->result();
            $set_to=$data_member[0]->email;
            $subjek="Pembayaran";
            // $set_from='404notfounddddddd@gmail.com';
            $message="Segera selesaikan pembayaran kemudian upload bukti pembayaran di menu Transaksi Pesanan!";
        }
        elseif ($type == 7) { // Email ada butki pembayaran baru ke Admin
            $data_transaksi=$this->M_template->view_where('transaksies',array('id_transaksi'=>$id_transaksi))->result();
            $set_to='404notfounddddddd@gmail.com';
            // $set_from='404notfounddddddd@gmail.com';
            $subjek="Segera Verifikasi Bukti Pembayaran Baru";
            $message="Ada bukti pembayaran baru saja diupload!id transaksi = ".$data_transaksi[0]->id_transaksi;
        }

        $mail->isSMTP();
        $mail->Host     = 'kampungsinau.com'; //sesuaikan sesuai Nama domain hosting/server yang digunakan
        $mail->SMTPAuth = true;
        $mail->Username = 'Admin@kampungsinau.com'; // user email
        $mail->Password = 'bedohoAdmin'; // password email
        $mail->SMTPSecure = 'ssl';
        $mail->Port     = 465;

        $mail->setFrom('Admin@kampungsinau.com', $this->session->Nama_app); // user email
        $mail->addReplyTo('Admin@kampungsinau.com', $this->session->Nama_app);

        $mail->addAddress($set_to);
        $mail->Subject =$subjek;

        $mail->isHTML(true);

        $mail->Body =$message;

        // $mail->addAddress('to@hostdomain.com');

        if(!$mail->send()){
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'Message has been sent';
        }
    }
    private function sendetiket($id_member,$id_transaksi)
    {
        $data_user=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
        $data_etiket=$this->M_template->etiket_detail($id_transaksi)->result();

        echo "<pre>";
        print_r($data_user);
        echo "</pre>";
        echo "<pre>";
        print_r($data_etiket);
        echo "</pre>";

        $message="Terima kasih telah memesan eTiket di ".$this->session->Nama_app." <br> Detail Tiket Anda <br>";
        for ($i=0; $i < count($data_etiket); $i++) { 
            $message .="<br> TIKET MASUK DESA WISATA KAMPUNG SINAU DESA BEDOHO, KECAMATAN JIWAN, KABUPATEN MADIUN <br> ".$data_etiket[$i]->produk."<br> Berlaku tanggal <b>".$data_etiket[$i]->booking."</b><br> Untuk 1 Orang<br>";
        }
        $message .="<br>Download eTiket PDF di website <a href='".base_url()."'>kami, klik disini</a> kemudian Pilih menu Transaksi Pesanan.";
        echo $message;
        $response = false;
        $mail = new PHPMailer();

         $mail->isSMTP();
        $mail->Host     = 'kampungsinau.com'; //sesuaikan sesuai Nama domain hosting/server yang digunakan
        $mail->SMTPAuth = true;
        $mail->Username = 'Admin@kampungsinau.com'; // user email
        $mail->Password = 'bedohoAdmin'; // password email
        $mail->SMTPSecure = 'ssl';
        $mail->Port     = 465;

        $mail->setFrom('Admin@kampungsinau.com', $this->session->Nama_app); // user email
        $mail->addReplyTo('Admin@kampungsinau.com', $this->session->Nama_app);

        $mail->addAddress($data_user[0]->email);
        $mail->Subject ='Pesanan Etiket Anda '.$this->session->Nama_app;

        $mail->isHTML(true);

        $mail->Body =$message;

        // $mail->addAddress('to@hostdomain.com');

        if(!$mail->send()){
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'Message has been sent';
        }
    }
}