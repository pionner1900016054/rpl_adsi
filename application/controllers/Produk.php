<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_template');
        if ($this->session->isLogin == false) {
            redirect('login');
        }
        if ($this->session->role == 3) {
            redirect('desa');
        }
        $session = ['func' => 'produk', 'wrap' => 'Produk'];
        $this->session->set_userdata($session);
    }
    public function detail($id)
    {
        $data['produk'] = $this->M_template->view_where('produks', array('id_produk' => $id))->result();
        $this->load->view('Admin/produk/detail', $data);
    }
    public function tambah()
    {
        $this->load->view('Admin/produk/tambah');
    }
    public function save()
    {
        $this->form_validation->set_rules('Nama', 'Nama Produk', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required');
        $this->form_validation->set_rules('berat', 'berat', 'required');
        $this->form_validation->set_rules('keterangan', 'keterangan', 'required');
        $this->form_validation->set_rules('stok', 'Stok', 'required');
        $this->form_validation->set_rules('kategori', 'Kategori', 'required');

        if ($this->form_validation->run() == false) {
            // echo validation_errors();
            $this->load->view('Admin/produk/tambah');
            // echo "asdasd";
        } else {
            $config = array(
                'upload_path' => './picture/',
                'overwrite' => false,
                'remove_spaces' => true,
                'allowed_types' => 'png|jpg|gif|jpeg',
                'max_size' => 10000,
                'xss_clean' => true,
            );
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload('gambar')) {
                $file_data = $this->upload->data();
                $data = array(
                    'produk' => $this->input->post('Nama'),
                    'kategori' => $this->input->post('kategori'),
                    'harga' => $this->input->post('harga'),
                    'berat' => $this->input->post('berat'),
                    'stok' => $this->input->post('stok'),
                    'keterangan' => $this->input->post('keterangan'),
                    'status' => 1,
                    'gambar' => $file_data['file_name'],
                );

                $this->M_template->insert('produks', $data);

                redirect('Admin/produk');
            } else {
                $this->load->view('Admin/produk/tambah');
            }
        }
    }
    public function edit($id)
    {
        $data['produk'] = $this->M_template->view_where('produks', array('id_produk' => $id))->result();
        $data['det_kegiatan'] = $this->M_template->get_kegiatan_tiket($id);

        $kegiatan_listed = [];
        foreach ($data['det_kegiatan'] as $item) {
            array_push($kegiatan_listed, $item->id_kegiatan);
        }

        $data['kegiatan'] = $this->M_template->get_kegiatan_not_listed($kegiatan_listed)->result();

        $this->load->view('Admin/produk/edit', $data);
    }
    public function update($id)
    {
        $this->form_validation->set_rules('Nama', 'Nama Produk', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required');
        $this->form_validation->set_rules('berat', 'berat', 'required');
        // $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
        $this->form_validation->set_rules('stok', 'Stok', 'required');
        $this->form_validation->set_rules('kategori', 'Kategori', 'required');

        if ($this->form_validation->run() == false) {
            $data['produk'] = $this->M_template->view_where('produks', array('id_produk' => $id))->result();
            $this->load->view('Admin/produk/edit', $data);
        } else {
            $config = array(
                'upload_path' => './picture/',
                'overwrite' => false,
                'remove_spaces' => true,
                'allowed_types' => 'png|jpg|gif|jpeg',
                'max_size' => 10000,
                'xss_clean' => true,
            );
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($_FILES['gambar']['name'] != "") {
                if ($this->upload->do_upload('gambar')) {
                    $file_data = $this->upload->data();
                    $data = array(
                        'produk' => $this->input->post('Nama'),
                        'kategori' => $this->input->post('kategori'),
                        'harga' => $this->input->post('harga'),
                        'berat' => $this->input->post('berat'),
                        'stok' => $this->input->post('stok'),
                        'keterangan' => $this->input->post('keterangan'),
                        'status' => 1,
                        'gambar' => $file_data['file_name'],
                    );

                    $this->M_template->update('produks', array('id_produk' => $id), $data);

                    redirect('Admin/Produk');
                } else {
                    $data['produk'] = $this->M_template->view_where('produks', array('id_produk' => $id))->result();
                    $this->load->view('Admin/produk/edit', $data);
                }
            } else {
                $data = array(
                    'produk' => $this->input->post('Nama'),
                    'kategori' => $this->input->post('kategori'),
                    'harga' => $this->input->post('harga'),
                    'berat' => $this->input->post('berat'),
                    'stok' => $this->input->post('stok'),
                    'keterangan' => $this->input->post('keterangan'),
                    'status' => 1,
                );
                $this->M_template->update('produks', array('id_produk' => $id), $data);
                redirect('Admin/produk');
            }
        }
    }
    public function status($id, $status)
    {
        $this->M_template->update('produks', array('id_produk' => $id), array('status' => $status));
        redirect('Admin/event');
    }
    public function hapus($id)
    {
        $this->M_template->delete('produks', array('id_produk' => $id));
        redirect('Admin/produk');
    }

    public function tambah_kegiatan()
    {

        $id_produk = $this->input->post('id_produk');
        $kegiatan = $this->input->post('kegiatan');

        $data = [
            'id_produk' => $id_produk,
            'id_kegiatan' => $kegiatan,
        ];

        $this->M_template->insert('acara', $data);

        redirect('produk/edit/' . $id_produk);

    }

    public function hapus_kegiatan_tiket($id_kegiatan, $id_produk)
    {
        $where = [
            'id_produk' => $id_produk,
            'id_kegiatan' => $id_kegiatan,
        ];

        $this->M_template->delete('acara', $where);

        redirect('produk/edit/' . $id_produk);
    }
}