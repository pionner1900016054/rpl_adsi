<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_template');
		if ($this->session->isLogin == FALSE ) {
			redirect('login');
		}
		if ( $this->session->role  != 3) {
			redirect('Admin');
		}
	}
	public function index()
	{
		$data['profile']=$this->M_template->join_where('users','members.id_member=users.id_member','members',array('users.id_member'=>$this->session->id_member))->result();
		// print_r($data);
		$this->load->view('public/profile/profile',$data);
	}
	public function ubah_foto()
	{
		$config= array(
	        'upload_path' => './avatar/',
	        'overwrite' => false,
            'remove_spaces' => true,
            'allowed_types' => 'png|jpg|gif|jpeg',
            'max_size' => 5000,
            'xss_clean' => true,
	    );
	    $this->load->library('upload');
        $this->upload->initialize($config);
	        // if ($_FILES['file']['name'] != "") {
	    if ($this->upload->do_upload('gambar')) {
	    	$file_data = $this->upload->data();
	    	$this->M_template->update('users',array('id_member'=>$this->session->id_member),array('avatar'=>$file_data['file_name']));
	    	$this->session->set_userdata('avatar',$file_data['file_name']);
        }
        redirect('profile');
	}
	public function ganti_password()
	{
		$user=$this->M_template->view_where('users',array('id_member'=>$this->session->id_member,'password'=>md5($this->input->post('password_lama'))))->result();
		// print_r($user);
		// print_r($this->input->post());
		if (md5($this->input->post('password_lama'))==$user[0]->password) {
			$this->M_template->update('users',array('id_member'=>$this->session->id_member),array('password'=>md5($this->input->post('password_baru'))));
		}
        redirect('profile');
	}
	public function ubah_data()
	{
	    $updated=array(
	    	'member'=>$this->input->post('Nama'),
	    	'tanggal_lahir'=>$this->input->post('tanggal'),
	    	'no_telpon'=>$this->input->post('no_telpon'),
	    	'alamat'=>$this->input->post('alamat')
	    );
	    // print_r($updated);
	    $this->M_template->update('members',array('id_member'=>$this->session->id_member),$updated);
	    $this->session->set_userdata('member',$this->input->post('Nama'));
	    redirect('profile');
		
	}
}