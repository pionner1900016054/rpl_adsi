<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekening extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_template');
		if ($this->session->isLogin == FALSE) {
			redirect('login');
		}
		if ($this->session->role == 3) {
			redirect('desa');
		}
		$session=['func'=>'rekening','wrap'=>'Rekening'];
		$this->session->set_userdata($session);
	}
	public function detail($id)
	{
		$data['rekening']=$this->M_template->view_where('rekenings',array('id_rekening'=>$id))->result();
		$this->load->view('Admin/rekening/detail',$data);
	}
	public function tambah()
	{
		// $data['rekening']=$this->M_template->view('rekening')->result();
		$this->load->view('Admin/rekening/tambah');
	}
	public function save()
	{
		$this->form_validation->set_rules('Nama', 'Nama rekening', 'required');
		$this->form_validation->set_rules('no', 'Nomor Rekebing', 'required');
		$this->form_validation->set_rules('bank', 'bank', 'required');

		if ($this->form_validation->run() == FALSE){
			$this->load->view('Admin/rekening/tambah');
        }else{
	       	$data=array(
	       	    'Nama_rekening'  => $this->input->post('Nama'),
	       	    'no_rekening'	 => $this->input->post('no'),
	       	    'bank' 			 => $this->input->post('bank'),
	       	    'status'		 => 1,
	       	);
	                // print_r($data);
	       	$this->M_template->insert('rekenings',$data);

	       	redirect('Admin/rekening');
        }
	}
	public function edit($id)
	{
		$data['rekening']=$this->M_template->view_where('rekenings',array('id_rekening'=>$id))->result();
		$this->load->view('Admin/rekening/edit',$data);
	}
	public function update($id)
	{
		$this->form_validation->set_rules('Nama', 'Nama rekening', 'required');
		$this->form_validation->set_rules('no', 'Nomor Rekebing', 'required');
		$this->form_validation->set_rules('bank', 'bank', 'required');
		
		if ($this->form_validation->run() == FALSE){
			$data['rekening']=$this->M_template->view_where('rekenings',array('id_rekening'=>$id))->result();
			$this->load->view('rekening/edit',$data);
        }else{
	                $data=array(
	                    'Nama_rekening'  => $this->input->post('Nama'),
	       	    		'no_rekening'	 => $this->input->post('no'),
	       	    		'bank' 			 => $this->input->post('bank'),
	                );

	                // print_r($data);
	                $this->M_template->update('rekenings',array('id_rekening'=>$id),$data);

	                redirect('Admin/rekening');
        }
	}
	public function status($id,$status)
	{
		$this->M_template->update('rekenings',array('id_rekening'=>$id),array('status'=>$status));
	    redirect('Admin/rekening');
	}
	public function hapus($id)
	{
		// echo "bisa";
		$this->M_template->delete('rekenings',array('id_rekening'=>$id));
	    redirect('Admin/rekening');
	}
}