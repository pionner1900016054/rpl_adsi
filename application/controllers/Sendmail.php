<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Sendmail extends CI_Controller {

    /**
     * Kirim email dengan SMTP Gmail.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_template');

        require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';
    }
    public function send($id_member,$type,$id_transaksi)
    {
        $response = false;
        $mail = new PHPMailer();

    $set_from=$set_to=$subjek=$message='';
        if ($type == 1) { //Email Pembayaran Valid
            $data_member=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
            $data_members=$this->M_template->view_where('members',array('id_member'=>$id_member))->result();
            $data_transaksi=$this->M_template->view_where('transaksies',array('id_transaksi'=>$id_transaksi))->result();
            $set_to=$data_member[0]->email;
            // $set_from='404notfounddddddd@gmail.com';
            $subjek="Verifikasi Pembayaran";
            $message="Hai,".$data_members[0]->member."! \r\nPembayaran Anda Valid, pesanan akan segera dikemas dan dikirim!";
        }elseif ($type == 2) { //Email Pembayaran Tidak Valid
            $data_member=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
            $data_members=$this->M_template->view_where('members',array('id_member'=>$id_member))->result();
            $set_to=$data_member[0]->email;
            // $set_from='404notfounddddddd@gmail.com';
            $subjek="Verifikasi Pembayaran Tidak Valid";
            $message="Hai,".$data_members[0]->member."! \r\nPembayaran Anda Tidak Valid, silahkan upload Ulang bukti pembayaran!";
        }elseif ($type == 3) { // Email Pesanan Dikirim
            $data_member=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
            $data_members=$this->M_template->view_where('members',array('id_member'=>$id_member))->result();
            $data_transaksi=$this->M_template->view_where('alur_transaksies',array('id_transaksi'=>$id_transaksi))->result();
            $set_to=$data_member[0]->email;
            // print_r($data_transaksi);
            // $set_from='404notfounddddddd@gmail.com';
            $subjek="Pesanan Dikirim";
            $message="Hai,".$data_members[0]->member."! \r\n Pesanan anda sudah dikirim! No resi ".$data_transaksi[0]->noresi;
        }elseif ($type == 4) { //Email Pesanan Diterima ke Admin
            $data_member=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
            $data_members=$this->M_template->view_where('members',array('id_member'=>$id_member))->result();
            $data_transaksi=$this->M_template->view_where('transaksies',array('id_transaksi'=>$id_transaksi))->result();
            $set_to=$data_member[0]->email;
            $subjek="Pesanan Sudah Diterima";
            // $set_from='404notfounddddddd@gmail.com';
            $message="Pesanan Users sudah diterima! id transaksi = ".$data_transaksi[0]->id_transaksi;
        }elseif ($type == 5) { // Email Order masuk ke Admin
            $data_transaksi=$this->M_template->view_where('transaksies',array('id_transaksi'=>$id_transaksi))->result();
            $set_to='404notfounddddddd@gmail.com';
            // $set_from='404notfounddddddd@gmail.com';
            $subjek="Orderan Masuk";
            $message="Ada orderan masuk! id transaksi = ".$data_transaksi[0]->id_transaksi." Pada tanggal ".$data_transaksi[0]->date;
        }elseif ($type == 6) { // Email Segera lakukan Pembayaran Valid ke users
            $data_member=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
            $data_members=$this->M_template->view_where('members',array('id_member'=>$id_member))->result();
            $set_to=$data_member[0]->email;
            $subjek="Pembayaran";
            // $set_from='404notfounddddddd@gmail.com';
            $message="Segera selesaikan pembayaran kemudian upload bukti pembayaran di menu Transaksi Pesanan!";
        }
        elseif ($type == 7) { // Email ada butki pembayaran baru ke Admin
            $data_transaksi=$this->M_template->view_where('transaksies',array('id_transaksi'=>$id_transaksi))->result();
            $set_to='404notfounddddddd@gmail.com';
            // $set_from='404notfounddddddd@gmail.com';
            $subjek="Segera Verifikasi Bukti Pembayaran Baru";
            $message="Ada bukti pembayaran baru saja diupload!id transaksi = ".$data_transaksi[0]->id_transaksi;
        }

        $mail->isSMTP();
        $mail->Host     = 'kampungsinau.com'; //sesuaikan sesuai Nama domain hosting/server yang digunakan
        $mail->SMTPAuth = true;
        $mail->Username = 'Admin@kampungsinau.com'; // user email
        $mail->Password = 'bedohoAdmin'; // password email
        $mail->SMTPSecure = 'ssl';
        $mail->Port     = 465;

        $mail->setFrom('Admin@kampungsinau.com', $this->session->Nama_app); // user email
        $mail->addReplyTo('Admin@kampungsinau.com', $this->session->Nama_app);

        $mail->addAddress($set_to);
        $mail->Subject =$subjek;

        $mail->isHTML(true);

        $mail->Body =$message;

        // $mail->addAddress('to@hostdomain.com');

        if(!$mail->send()){
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'Message has been sent';
        }
    }
    public function sendetiket($id_member,$id_transaksi)
    {
        $data_user=$this->M_template->view_where('users',array('id_member'=>$id_member))->result();
        $data_etiket=$this->M_template->etiket_detail($id_transaksi)->result();

        echo "<pre>";
        print_r($data_user);
        echo "</pre>";
        echo "<pre>";
        print_r($data_etiket);
        echo "</pre>";

        $message="Terima kasih telah memesan eTiket di ".$this->session->Nama_app." <br> Detail Tiket Anda <br>";
        for ($i=0; $i < count($data_etiket); $i++) { 
            $message .="<br> TIKET MASUK DESA WISATA KAMPUNG SINAU DESA BEDOHO, KECAMATAN JIWAN, KABUPATEN MADIUN <br> ".$data_etiket[$i]->produk."<br> Berlaku tanggal <b>".$data_etiket[$i]->booking."</b><br> Untuk 1 Orang<br>";
        }
        $message .="<br>Download eTiket PDF di website <a href='".base_url()."'>kami, klik disini</a> kemudian Pilih menu Transaksi Pesanan.";
        echo $message;
        $response = false;
        $mail = new PHPMailer();

         $mail->isSMTP();
        $mail->Host     = 'kampungsinau.com'; //sesuaikan sesuai Nama domain hosting/server yang digunakan
        $mail->SMTPAuth = true;
        $mail->Username = 'Admin@kampungsinau.com'; // user email
        $mail->Password = 'bedohoAdmin'; // password email
        $mail->SMTPSecure = 'ssl';
        $mail->Port     = 465;

        $mail->setFrom('Admin@kampungsinau.com', $this->session->Nama_app); // user email
        $mail->addReplyTo('Admin@kampungsinau.com', $this->session->Nama_app);

        $mail->addAddress($data_user[0]->email);
        $mail->Subject ='Pesanan Etiket Anda '.$this->session->Nama_app;

        $mail->isHTML(true);

        $mail->Body =$message;

        // $mail->addAddress('to@hostdomain.com');

        if(!$mail->send()){
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'Message has been sent';
        }
    }
    public function test()
    {
        echo $this->session->role;
        echo "<BR>";
        echo str_pad(rand(0, pow(10, 3)-1), 3, '0', STR_PAD_LEFT);
    }
}