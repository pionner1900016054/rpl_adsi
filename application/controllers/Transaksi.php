<?php
ob_start();
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;

class Transaksi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_template');
        $this->session->set_flashdata('active', 'keranjang');
        // $this->set_konfigurasi();
        if ($this->session->isLogin == false) {
            redirect('login');
        }
        if ($this->session->role != 3) {
            redirect('Admin');
        }
        require APPPATH . 'libraries/phpmailer/src/Exception.php';
        require APPPATH . 'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH . 'libraries/phpmailer/src/SMTP.php';
    }
    public function index()
    {
        $data['transaksi'] = $this->M_template->join_where('transaksies', 'members.id_member=transaksies.id_member', 'members', array('transaksies.id_member' => $this->session->id_member))->result();
        $data['produk'] = $this->M_template->join('produk_orders', 'produks.id_produk=produk_orders.id_produk', 'produks')->result();
        $this->load->view('public/transaksi/transaksi', $data);
    }
    public function detail($id)
    {
        $data['transaksi'] = $this->M_template->get_detail_order(array('id_transaksi' => $id))->result();
        // echo $this->session->id_member;
        //     echo $data['transaksi'][0]->id_member;
        if ($data['transaksi'][0]->id_member == $this->session->id_member) {
            // echo $data['transaksi'][0]->id_member;
            // $data['transaksi']=$this->M_template->view_where('transaksies',array('id_transaksi'=>$id))->result();
            $data['alur_transaksi'] = $this->M_template->view_where('alur_transaksies', array('id_transaksi' => $id))->result();
            $data['produk_order'] = $this->M_template->join_where('produk_orders', 'produk_orders.id_produk=produks.id_produk', 'produks', array('id_transaksi' => $id))->result();
            $data['etiket'] = $this->M_template->getEtiketTransaksi($id)->result();
            // echo "<pre>";
            // print_r($data);
            // echo "</pre>";
            $this->load->view('public/transaksi/detail', $data);
        } else {
            echo "You're not allowed to access";
        }
    }
    public function getKeranjang()
    {
        $data = $this->M_template->join_where('keranjangs', 'produks.id_produk=keranjangs.id_produk', 'produks', array('keranjangs.id_member' => $this->session->id_member))->result();
        echo json_encode($data);
    }
    public function keranjang()
    {

        $this->load->view('public/keranjang/keranjang');
    }
    public function addCart($id, $time = 0)
    {
        $check_same = $this->M_template->view_where('keranjangs', ['id_produk' => $id, 'id_member' => $this->session->id_member])->result();
        if (!empty($check_same)) {
            echo "Sudah ada tinggal tambah";
            $data = array(
                'jumlah' => $check_same[0]->jumlah + 1,
            );
            $this->M_template->update('keranjangs', ['id_keranjang' => $check_same[0]->id_keranjang, 'id_produk' => $id, 'id_member' => $this->session->id_member], $data);
        } else {
            echo "Belum ada, tambah";
            $produk = $this->M_template->view_where('produks', ['id_produk' => $id])->result();
            $data = array(
                'id_member' => $this->session->id_member,
                'id_produk' => $id,
                'jumlah' => 1,
                'harga_satuan' => $produk[0]->harga,
                'waktu' => $time,
                'status' => 1,
            );
            $this->M_template->insert('keranjangs', $data);
        }
        redirect(base_url() . 'transaksi/keranjang');
    }

    public function addCartPaketPilihan($id, $harga, $waktu)
    {
        $check_same = $this->M_template->view_where('keranjangs', ['id_produk' => $id, 'id_member' => $this->session->id_member])->result();
        if (!empty($check_same)) {
            echo "Sudah ada tinggal tambah";
            $data = array(
                'jumlah' => $check_same[0]->jumlah + 1,
            );
            $this->M_template->update('keranjangs', ['id_keranjang' => $check_same[0]->id_keranjang, 'id_produk' => $id, 'id_member' => $this->session->id_member], $data);
        } else {
            echo "Belum ada, tambah";
            $produk = $this->M_template->view_where('produks', ['id_produk' => $id])->result();
            $data = array(
                'id_member' => $this->session->id_member,
                'id_produk' => $id,
                'jumlah' => 1,
                'harga_satuan' => $harga,
                'waktu' => $waktu,
                'status' => 1,
            );
            $this->M_template->insert('keranjangs', $data);
        }
        redirect(base_url() . 'transaksi/keranjang');
    }

    public function hapusKeranjang($id)
    {
        $this->M_template->delete('keranjangs', array('id_keranjang' => $id));
        redirect('transaksi/keranjang');
    }
    public function checkout()
    {

        // var_dump($_COOKIE['harga_pilihan']);die();
        // echo json_encode(array('status'=>'success'));
        // echo $this->input->post('transaksi')."<br>";
        $transaksi_exp = explode(',', $this->input->post('transaksi'));
        $where_produk_checkout = array();
        for ($i = 0; $i < count($transaksi_exp) - 1; $i++) {
            // array_push($where_produk_checkout, var)
            $where_produk_checkout[] = array('id_keranjang' => $transaksi_exp[$i]);
        }
        // echo count($transaksi_exp);
        // echo "<pre>";
        // print_r($transaksi_exp);
        // echo "</pre>";
        // echo "<pre>";
        // print_r($where_produk_checkout);
        // echo "</pre>";
        // echo count($where_produk_checkout);
        $totalberat = 0;
        $totalbayarproduk = 0;
        $istiket = false;
        $isproduk = false;
        $istiketonly = 0;
        // $checkout_for_totalberat=
        $checkout_for_totalberat = $this->M_template->getCheckout($where_produk_checkout)->result_array();
        for ($i = 0; $i < count($checkout_for_totalberat); $i++) {
            $totalberat += $checkout_for_totalberat[$i]['berat'] * $checkout_for_totalberat[$i]['jumlah'];
            $totalbayarproduk += $checkout_for_totalberat[$i]['harga'] * $checkout_for_totalberat[$i]['jumlah'];
        }
        // echo $totalberat."KG";

        $get_distinct = $this->M_template->getDistincKategoriCheckout($where_produk_checkout)->result();
        foreach ($get_distinct as $key) {
            if (count($get_distinct) == 1 && $key->kategori == "Tiket") {
                $istiketonly = 1;
            } else {
                $istiketonly = 0;
            }

            if ($key->kategori == "Tiket") {
                $istiket = true;
            } elseif ($key->kategori == "Produk") {
                $isproduk = true;
            }
        }

        $data_checkout['istiket'] = $istiket;
        $data_checkout['isproduk'] = $isproduk;
        $data_checkout['istiketonly'] = $istiketonly;
        $data_checkout['distict_checkout'] = $this->M_template->getDistincKategoriCheckout($where_produk_checkout)->result();
        $data_checkout['keranjang'] = $this->M_template->getCheckout($where_produk_checkout)->result();
        $data_checkout['rekening'] = $this->M_template->view_where('rekenings', array('status' => 1))->result();
        $data_checkout['totalberat'] = $totalberat;
        $data_checkout['totalbayarproduk'] = $totalbayarproduk;
        $data_checkout['keranjang_id'] = $this->input->post('transaksi');
        $data_checkout['rand_code'] = str_pad(rand(0, pow(10, 3) - 1), 3, '0', STR_PAD_LEFT);
        $this->load->view('public/transaksi/checkout', $data_checkout);
        // echo "<pre>";
        // print_r($data_checkout);
        // echo "</pre>";
        // $produk_checkout=
        // $data['keranjang']=$this->M_template->join_where('keranjangs','produks.id_produk=keranjangs.id_produk','produks',array('keranjangs.id_member'=>$this->session->id_member))->result();
        // $this->session->set_flashdata(['keranjang' => $this->input->post('keranjang')]);
        // redirect('transaksi/process_checkout');
    }
    public function addJumlah($ket)
    {
        switch ($ket) {
            case 'plus':
                $keranjang = $this->M_template->view_where('keranjangs', ['id_keranjang' => $this->input->post('id'), 'id_member' => $this->session->id_member])->result();
                $new_jumlah = $keranjang[0]->jumlah + 1;
                $data = array(
                    'jumlah' => $new_jumlah,
                );
                $this->M_template->update('keranjangs', ['id_keranjang' => $this->input->post('id'), 'id_member' => $this->session->id_member], $data);
                // print_r($keranjang);
                // echo $ket;
                echo json_encode(array('status' => 'success'));
                break;
            case 'min':
                $keranjang = $this->M_template->view_where('keranjangs', ['id_keranjang' => $this->input->post('id'), 'id_member' => $this->session->id_member])->result();
                $new_jumlah = $keranjang[0]->jumlah - 1;
                if ($new_jumlah == 0) {
                    $this->M_template->delete('keranjangs', ['id_keranjang' => $this->input->post('id'), 'id_member' => $this->session->id_member]);
                    echo json_encode(array('status' => 'success'));
                } else {
                    $data = array(
                        'jumlah' => $new_jumlah,
                    );
                    $this->M_template->update('keranjangs', ['id_keranjang' => $this->input->post('id'), 'id_member' => $this->session->id_member], $data);
                    echo json_encode(array('status' => 'success'));
                }
                // echo $ket;
                // print_r($keranjang);
                break;
        }
    }
    public function proses_transaksi($istiketonly)
    {
        if ($istiketonly == 0) {
            $kabupaten = json_decode($this->kota($this->input->post('sel11')));
            // $provinsi=json_decode($this->provinsi());
            // print_r(json_decode($provinsi));
            $Nama_provinsi = '';
            // foreach ($provinsi as $key) {
            //     if ($key->province_id == $this->input->post('sel11')) {
            //         $Nama_provinsi=$key->province;
            //         break;
            //     }
            // }
            $Nama_kabupaten = '';
            foreach ($kabupaten as $key) {
                if ($key->province_id == $this->input->post('sel11') && $key->city_id == $this->input->post('sel22')) {
                    $Nama_provinsi = $key->province;
                    $Nama_kabupaten = $key->type . " " . $key->city_name;
                    echo $Nama_provinsi . "---" . $Nama_kabupaten;
                    // break;
                }
            }
            $transaksi_exp = explode(',', $this->input->post('keranjang_id'));
            $where_produk_checkout = array();
            for ($i = 0; $i < count($transaksi_exp) - 1; $i++) {
                // array_push($where_produk_checkout, var)
                $where_produk_checkout[] = array('id_keranjang' => $transaksi_exp[$i]);
            }
            $data_checkout = $this->M_template->getCheckout($where_produk_checkout)->result();

            $exp_service = explode('--', $this->input->post('service'));

            $data = array(
                // 'id_produk'=>$this->input->post('produk_id'),//keranjang_id
                'id_member' => $this->session->id_member,
                'id_rekening' => $this->input->post('rekening'),
                'tanggal_booking' => $this->input->post('tanggal'),
                'penerima' => $this->input->post('penerima'),
                'notelponpenerima' => $this->input->post('notelponpenerima'),
                'provinsi_penerima' => $Nama_provinsi, //gantiNamaprovinsi
                'kabupaten_penerima' => $Nama_kabupaten, //gantiNamakabupaten
                'kurir' => $this->input->post('kurir'),
                'paket_kurir' => $exp_service[1],
                'kodepos_penerima' => $this->input->post('kodepos'),
                'alamat_penerima' => $this->input->post('alamat_kirim'),
                'berattotal' => $this->input->post('berat'),
                'totalkirim' => $exp_service[2],
                'totalpesanan' => $this->input->post('totalpesanan'),
                'totalbayar' => $this->input->post('totalall'),
                // 'buktibayar'=>$this->input->post('service'),
                // 'statusbukti'=>$this->input->post('service'),//0:belum upload|1:sudah upload
                // 'verifikasibukti'=>$this->input->post('service'),//0:belum|1:valid|2:tidak valid
                // 'statustransaksi'=>$this->input->post('service'),//0:belum|1:proses|2:sudah|3:cancel
            );

            $id_transaksi = $this->M_template->insert_id('transaksies', $data);
            foreach ($data_checkout as $key) {
                $data_produk_order = array(
                    'id_transaksi' => $id_transaksi,
                    'id_produk' => $key->id_produk,
                    'jumlah' => $key->jumlah,
                    'kategori' => $key->kategori,
                );
                $this->M_template->insert('produk_orders', $data_produk_order);
                echo "<pre>";
                print_r($data_produk_order);
                echo "</pre>";
                $get_stok = $this->M_template->view_where('produks', array('id_produk' => $key->id_produk))->result();
                // $this->M_template->update('produks',array('id_produk'=>$key->id_produk),array('stok'=>$get_stok[0]->stok - $key->jumlah));
                $this->M_template->delete('keranjangs', array('id_keranjang' => $key->id_keranjang));
            }

            //loop E-Tiket PDF

            $data_alur_transaksi = array(
                'id_transaksi' => $id_transaksi,
            );

            $this->M_template->insert('alur_transaksies', $data_alur_transaksi);

            $this->send_mail($this->session->id_member, 5, $id_transaksi);
            $this->send_mail($this->session->id_member, 6, $id_transaksi);

            redirect('transaksi/invoice/' . $id_transaksi);
        } else {
            $transaksi_exp = explode(',', $this->input->post('keranjang_id'));
            $where_produk_checkout = array();
            for ($i = 0; $i < count($transaksi_exp) - 1; $i++) {
                // array_push($where_produk_checkout, var)
                $where_produk_checkout[] = array('id_keranjang' => $transaksi_exp[$i]);
            }
            $data_checkout = $this->M_template->getCheckout($where_produk_checkout)->result();
            // echo "<br><br>";
            // print_r(json_decode($kabupaten));
            // echo "<br><br>";
            $exp_service = explode('--', $this->input->post('service'));
            $data = array(
                // 'id_produk'=>$this->input->post('produk_id'),//keranjang_id
                'id_member' => $this->session->id_member,
                'id_rekening' => $this->input->post('rekening'),
                'tanggal_booking' => $this->input->post('tanggal'),
                'totalpesanan' => $this->input->post('totalpesanan'),
                'totalbayar' => $this->input->post('totalall'),
                // 'buktibayar'=>$this->input->post('service'),
                // 'statusbukti'=>$this->input->post('service'),//0:belum upload|1:sudah upload
                // 'verifikasibukti'=>$this->input->post('service'),//0:belum|1:valid|2:tidak valid
                // 'statustransaksi'=>$this->input->post('service'),//0:belum|1:proses|2:sudah|3:cancel
            );

            $id_transaksi = $this->M_template->insert_id('transaksies', $data);
            foreach ($data_checkout as $key) {
                $data_produk_order = array(
                    'id_transaksi' => $id_transaksi,
                    'id_produk' => $key->id_produk,
                    'kategori' => $key->kategori,
                    'jumlah' => $key->jumlah,
                );
                $this->M_template->insert('produk_orders', $data_produk_order);
                echo "<pre>";
                print_r($data_produk_order);
                echo "</pre>";
                $this->M_template->delete('keranjangs', array('id_keranjang' => $key->id_keranjang));
            }

            $data_alur_transaksi = array(
                'id_transaksi' => $id_transaksi,
            );

            $this->M_template->insert('alur_transaksies', $data_alur_transaksi);

            $this->send_mail($this->session->id_member, 5, $id_transaksi);
            $this->send_mail($this->session->id_member, 6, $id_transaksi);
            redirect(base_url() . 'transaksi/invoice/' . $id_transaksi);
        }
    }
    public function _api_ongkir($data)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            //CURLOPT_URL => "https://api.rajaongkir.com/starter/province?id=12",
            //CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
            CURLOPT_URL => "http://api.rajaongkir.com/starter/" . $data,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                /* masukan api key disini*/
                "key:7960ef3eada950edbf2e6609895c0598",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return $err;
        } else {
            return $response;
        }
    }
    public function provinsi()
    {
        $provinsi = $this->_api_ongkir('province');
        $data = json_decode($provinsi, true);
        return json_encode($data['rajaongkir']['results']);
    }

    public function kota($provinsi = "")
    {
        if (!empty($provinsi)) {
            if (is_numeric($provinsi)) {
                $kota = $this->_api_ongkir('city?province=' . $provinsi);
                $data = json_decode($kota, true);
                return json_encode($data['rajaongkir']['results']);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }
    public function invoice($id)
    {
        $data['transaksi'] = $this->M_template->view_where('transaksies', array('id_transaksi' => $id))->result();
        if ($data['transaksi'][0]->id_member == $this->session->id_member) {
            $data['alur_transaksi'] = $this->M_template->view_where('alur_transaksies', array('id_transaksi' => $id))->result();
            $data['produk_order'] = $this->M_template->view_where('produk_orders', array('id_transaksi' => $id))->result();
            echo "<pre>";
            print_r($data);
            echo "</pre>";
            $total = $this->M_template->view_where('transaksies', array('id_transaksi' => $id))->result();
            $rekening = $this->M_template->view_where('rekenings', array('id_rekening' => $total[0]->id_rekening))->result();
            $invoice = '<div class="col-lg-12 text-center aler alert-success">Pesanan anda berhasil ditambahkan, Silahkan selesaikan pembayaran kemudian upload bukti pembayaran.<p>Pembayaran melalui ' . $rekening[0]->bank . '</p><p>No Rekening ' . $rekening[0]->no_rekening . '</p><p>Atas Nama ' . $rekening[0]->Nama_rekening . '</p></div>';
            // echo $invoice;
            $this->session->set_flashdata('invoice', $invoice);
            redirect('transaksi/detail/' . $id);
        } else {
            echo "You're not allowed";
        }
    }
    public function uploadbuktibayar($id)
    {
        $data['transaksi'] = $this->M_template->view_where('transaksies', array('id_transaksi' => $id))->result();
        if ($data['transaksi'][0]->id_member == $this->session->id_member) {
            $config = array(
                'upload_path' => './pembayaran/',
                'overwrite' => false,
                'remove_spaces' => true,
                'allowed_types' => 'png|jpg|gif|jpeg|pdf',
                'max_size' => 10000,
                'xss_clean' => true,
            );
            $this->load->library('upload');
            $this->upload->initialize($config);
            // if ($_FILES['file']['name'] != "") {
            if ($this->upload->do_upload('file')) {
                $file_data = $this->upload->data();
                $data = array(
                    'statusbukti' => 1,
                    'buktibayar' => $file_data['file_name'],
                );
                // print_r($data);
                $this->M_template->update('transaksies', array('id_transaksi' => $id), $data);
                $invoice = '<div class="col text-center aler alert-success">upload Sukses</div>';
                // echo $invoice;
                $this->session->set_flashdata('error', $invoice);
                $this->send_mail($this->session->id_member, 7, $id);
                redirect('transaksi/detail/' . $id);
            } else {
                $invoice = '<div class="col text-center aler alert-danger">Pastikan bukti upload png|jpg|gif|jpeg|pdf. Maksimal 10Mb</div>';
                // echo $invoice;
                $this->session->set_flashdata('error', $invoice);
                redirect('transaksi/detail/' . $id);
            }
        } else {
            echo "You're not allowed";
        }
    }
    public function ubahalurtransaksi($id)
    {
        $data['transaksi'] = $this->M_template->view_where('transaksies', array('id_transaksi' => $id))->result();
        if ($data['transaksi'][0]->id_member == $this->session->id_member) {
            $this->M_template->update('alur_transaksies', array('id_transaksi' => $id), array('diterima' => 1, 'tanggal_diterima' => date('Y-m-d H:i:s')));
            $this->M_template->update('transaksies', array('id_transaksi' => $id), array('statustransaksi' => 2));
            $this->send_mail($this->session->id_member, 4, $id);
            redirect('transaksi/detail/' . $id);
        } else {
            echo "You're not allowed";
        }
    }
    private function send_mail($id_member, $type, $id_transaksi)
    {
        $response = false;
        $mail = new PHPMailer();

        $set_from = $set_to = $subjek = $message = '';
        if ($type == 1) { //Email Pembayaran Valid
            $data_member = $this->M_template->view_where('users', array('id_member' => $id_member))->result();
            $data_members = $this->M_template->view_where('members', array('id_member' => $id_member))->result();
            $data_transaksi = $this->M_template->view_where('transaksies', array('id_transaksi' => $id_transaksi))->result();
            $set_to = $data_member[0]->email;
            // $set_from='404notfounddddddd@gmail.com';
            $subjek = "Verifikasi Pembayaran";
            $message = "Hai," . $data_members[0]->member . "! \r\nPembayaran Anda Valid, pesanan akan segera dikemas dan dikirim!";
        } elseif ($type == 2) { //Email Pembayaran Tidak Valid
            $data_member = $this->M_template->view_where('users', array('id_member' => $id_member))->result();
            $data_members = $this->M_template->view_where('members', array('id_member' => $id_member))->result();
            $set_to = $data_member[0]->email;
            // $set_from='404notfounddddddd@gmail.com';
            $subjek = "Verifikasi Pembayaran Tidak Valid";
            $message = "Hai," . $data_members[0]->member . "! \r\nPembayaran Anda Tidak Valid, silahkan upload Ulang bukti pembayaran!";
        } elseif ($type == 3) { // Email Pesanan Dikirim
            $data_member = $this->M_template->view_where('users', array('id_member' => $id_member))->result();
            $data_members = $this->M_template->view_where('members', array('id_member' => $id_member))->result();
            $data_transaksi = $this->M_template->view_where('alur_transaksies', array('id_transaksi' => $id_transaksi))->result();
            $set_to = $data_member[0]->email;
            // print_r($data_transaksi);
            // $set_from='404notfounddddddd@gmail.com';
            $subjek = "Pesanan Dikirim";
            $message = "Hai," . $data_members[0]->member . "! \r\n Pesanan anda sudah dikirim! No resi " . $data_transaksi[0]->noresi;
        } elseif ($type == 4) { //Email Pesanan Diterima ke Admin
            $data_member = $this->M_template->view_where('users', array('id_member' => $id_member))->result();
            $data_members = $this->M_template->view_where('members', array('id_member' => $id_member))->result();
            $data_transaksi = $this->M_template->view_where('transaksies', array('id_transaksi' => $id_transaksi))->result();
            $set_to = $data_member[0]->email;
            $subjek = "Pesanan Sudah Diterima";
            // $set_from='404notfounddddddd@gmail.com';
            $message = "Pesanan Users sudah diterima! id transaksi = " . $data_transaksi[0]->id_transaksi;
        } elseif ($type == 5) { // Email Order masuk ke Admin
            $data_transaksi = $this->M_template->view_where('transaksies', array('id_transaksi' => $id_transaksi))->result();
            $set_to = '404notfounddddddd@gmail.com';
            // $set_from='404notfounddddddd@gmail.com';
            $subjek = "Orderan Masuk";
            $message = "Ada orderan masuk! id transaksi = " . $data_transaksi[0]->id_transaksi . " Pada tanggal " . $data_transaksi[0]->date;
        } elseif ($type == 6) { // Email Segera lakukan Pembayaran Valid ke users
            $data_member = $this->M_template->view_where('users', array('id_member' => $id_member))->result();
            $data_members = $this->M_template->view_where('members', array('id_member' => $id_member))->result();
            $set_to = $data_member[0]->email;
            $subjek = "Pembayaran";
            // $set_from='404notfounddddddd@gmail.com';
            $message = "Segera selesaikan pembayaran kemudian upload bukti pembayaran di menu Transaksi Pesanan!";
        } elseif ($type == 7) { // Email ada butki pembayaran baru ke Admin
            $data_transaksi = $this->M_template->view_where('transaksies', array('id_transaksi' => $id_transaksi))->result();
            $set_to = '404notfounddddddd@gmail.com';
            // $set_from='404notfounddddddd@gmail.com';
            $subjek = "Segera Verifikasi Bukti Pembayaran Baru";
            $message = "Ada bukti pembayaran baru saja diupload!id transaksi = " . $data_transaksi[0]->id_transaksi;
        }

        $mail->isSMTP();
        $mail->Host = 'kampungsinau.com'; //sesuaikan sesuai Nama domain hosting/server yang digunakan
        $mail->SMTPAuth = true;
        $mail->Username = 'Admin@kampungsinau.com'; // user email
        $mail->Password = 'bedohoAdmin'; // password email
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;

        $mail->setFrom('Admin@kampungsinau.com', $this->session->Nama_app); // user email
        $mail->addReplyTo('Admin@kampungsinau.com', $this->session->Nama_app);

        $mail->addAddress($set_to);
        $mail->Subject = $subjek;

        $mail->isHTML(true);

        $mail->Body = $message;

        // $mail->addAddress('to@hostdomain.com');

        if (!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }
    }
}