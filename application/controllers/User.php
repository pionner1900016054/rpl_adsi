<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_template');
	}
	public function detail($id)
	{
		$data['user']=$this->M_template->get_user_detail($id)->result();
		$this->load->view('Admin/user/detail',$data);
	}
	public function blokir($id,$status)
	{
		$this->M_template->update('users',array('id_user'=>$id),array('status'=>$status));
		redirect('Admin/user');
	}
}