<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_template extends CI_Model
{
    public function insert($tabel, $data)
    {
        $this->db->insert($tabel, $data);
        // return $this->db->insert_id();
    }
    public function insert_id($tabel, $data)
    {
        $this->db->insert($tabel, $data);
        return $this->db->insert_id();
    }
    public function update($tabel, $where, $data)
    {
        $this->db->where($where);
        return $this->db->update($tabel, $data);
    }
    public function delete($tabel, $where)
    {
        $this->db->where($where);
        return $this->db->delete($tabel);
    }
    public function view_where($tabel, $where)
    {
        $this->db->where($where);
        return $this->db->get($tabel);
    }
    public function view($tabel)
    {
        return $this->db->get($tabel);
    }
    public function join($tabel, $using, $with)
    {
        $this->db->select('*');
        $this->db->from($tabel);
        $this->db->join($with, $using);
        $query = $this->db->get();
        return $query;
    }
    public function join_where($tabel, $using, $with, $where)
    {
        $this->db->select('*');
        $this->db->from($tabel);
        $this->db->join($with, $using);
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }
    public function get_detail_order($where)
    {
        $this->db->select('*');
        $this->db->from('transaksies');
        $this->db->join('members', 'members.id_member=transaksies.id_member');
        $this->db->join('rekenings', 'rekenings.id_rekening=transaksies.id_rekening');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }
    public function graph()
    {
        return $this->db->query("SELECT SUM(jumlah) AS jumlah, kategori FROM `produk_orders` GROUP BY kategori");
    }
    public function get_produk_index()
    {
        return $this->db->query("SELECT * FROM `produks` WHERE kategori ='Produk' ORDER BY id_produk DESC LIMIT 4");
    }
    public function get_produk()
    {
        return $this->db->query("SELECT * FROM `produks` WHERE kategori ='Produk' ORDER BY id_produk DESC LIMIT 6");
    }
    public function get_event()
    {
        return $this->db->query("SELECT * FROM `events` WHERE tanggal_mulai >= NOW() LIMIT 5");
    }
    public function view_limit($tabel, $limit, $start)
    {
        $query = $this->db->get($tabel, $limit, $start);
        return $query;
    }
    public function view_limit_where($tabel, $limit, $start, $where)
    {
        $this->db->where($where);
        $query = $this->db->get($tabel, $limit, $start);
        return $query;
    }
    public function get_user()
    {
        return $this->db->query("SELECT * FROM `users` LEFT JOIN members ON members.id_member=users.id_member LEFT JOIN Admins ON Admins.id_Admin=users.id_Admin WHERE users.role > 1");
    }
    public function get_user_detail($id)
    {
        return $this->db->query("SELECT * FROM `users` JOIN members ON members.id_member=users.id_member WHERE users.id_user =" . $id);
    }
    public function get_artikel($limit, $start)
    {
        $this->db->where(array('kategori' => 'Berita'));
        $this->db->order_by('created_at', 'DESC');
        $query = $this->db->get('artikels', $limit, $start);
        return $query;
    }
    public function getCheckout($where)
    {
        $this->db->select('*');
        $this->db->from('keranjangs');
        $this->db->join('produks', 'produks.id_produk=keranjangs.id_produk');
        if (count($where) == 1) {
            $this->db->where($where[0]);
        } elseif (count($where) > 1) {
            for ($i = 0; $i < count($where); $i++) {
                if ($i == 0) {
                    $this->db->where($where[$i]);
                } else {
                    $this->db->or_where($where[$i]);
                }
            }
        }
        $query = $this->db->get();
        return $query;
    }
    public function getDistincKategoriCheckout($where)
    {
        $this->db->distinct();
        $this->db->select('kategori');
        $this->db->from('keranjangs');
        $this->db->join('produks', 'produks.id_produk=keranjangs.id_produk');
        if (count($where) == 1) {
            $this->db->where($where[0]);
        } elseif (count($where) > 1) {
            for ($i = 0; $i < count($where); $i++) {
                if ($i == 0) {
                    $this->db->where($where[$i]);
                } else {
                    $this->db->or_where($where[$i]);
                }
            }
        }
        $query = $this->db->get();
        return $query;
    }
    public function getNamaEtiket($id)
    {
        return $this->db->query("SELECT DISTINCT id_transaksi,tanggal_booking,member,kategori FROM `produk_orders` JOIN transaksies USING(id_transaksi) JOIN members USING(id_member) WHERE kategori='Tiket' AND id_transaksi=" . $id);
    }
    public function getEtiket($id)
    {
        return $this->db->query("SELECT * FROM etikets JOIN produk_orders ON produk_orders.id_produk_order=etikets.id_produk_orders JOIN produks ON produks.id_produk=produk_orders.id_produk WHERE id_etiket=" . $id);
    }
    public function authEtiket($id)
    {
        return $this->db->query("SELECT * FROM `etikets` JOIN produk_orders ON produk_orders.id_produk_order=etikets.id_produk_orders JOIN transaksies USING(id_transaksi) WHERE etikets.id_etiket=" . $id);
    }
    public function getEtiketTransaksi($id)
    {
        return $this->db->query("SELECT * FROM etikets JOIN produk_orders ON produk_orders.id_produk_order=etikets.id_produk_orders JOIN produks ON produks.id_produk=produk_orders.id_produk WHERE id_transaksi=" . $id);
    }
    public function getCheckEtiket($where)
    {
        $this->db->select('*');
        $this->db->from('etikets');
        $this->db->join('produk_orders', 'produk_orders.id_produk_order=etikets.id_produk_orders');
        if (count($where) == 1) {
            $this->db->where($where[0]);
        } elseif (count($where) > 1) {
            for ($i = 0; $i < count($where); $i++) {
                if ($i == 0) {
                    $this->db->where($where[$i]);
                } else {
                    $this->db->or_where($where[$i]);
                }
            }
        }
        $query = $this->db->get();
        return $query;
    }
    public function etiket_detail($id)
    {
        return $this->db->query("SELECT * FROM etikets JOIN produk_orders on produk_orders.id_produk_order=etikets.id_produk_orders JOIN transaksies USING(id_transaksi) JOIN produks USING(id_produk) WHERE id_transaksi = " . $id);
    }

    public function get_kegiatan_tiket($id_produk)
    {

        $this->db->select('*');
        $this->db->from('acara a');
        $this->db->join('kegiatans k', 'id_kegiatan');
        $this->db->where('a.id_produk', $id_produk
        );
        return $this->db->get()->result();
    }

    public function get_tiket_acara()
    {
        $this->db->select('*');
        $this->db->from('produks p');
        $this->db->join('acara a', 'id_produk');
        $this->db->where('p.kategori', "Tiket");
        return $this->db->get()->result();
    }

    public function get_kegiatan_not_listed($data)
    {

        $this->db->from('kegiatans');

        if (count($data) != 0) {
            $this->db->where_not_in('id_kegiatan', $data);
        }

        return $this->db->get();

    }

    public function getDetailTiket()
    {

        $this->db->select('*, sum(durasi_kegiatan) time');
        $this->db->from('produks pd');
        $this->db->join('acara ac', 'on(pd.id_produk = ac.id_produk)');
        $this->db->join('kegiatans kg', 'on(ac.id_kegiatan = kg.id_kegiatan)');
        $this->db->where('pd.kategori', 'Tiket');
        $this->db->group_by('pd.id_produk');
        return $this->db->get();
    }

}