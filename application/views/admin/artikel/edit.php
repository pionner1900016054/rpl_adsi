<?php $this->load->view('template/header'); ?>
<style type="text/css">
	.gagal{
		color: red;
	}
</style>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
      	<div class="row">
      		<div class="col-lg-12">
      			<div class="card">
      				<div class="card-header row">
      					<div class="col-sm-12 col-md-6">
      						<h4 class="card-title">Edit artikel</h4>
      					</div>
      				</div>
      				<div class="card-body">
      					<!-- <form method="post" action="<?=base_url()?>artikel/update/<?=$artikel[0]->id_artikel?>" enctype="multipart/form-data"> -->
						  <?=form_open_multipart('artikel/update/'.$artikel[0]->id_artikel)?>
						 <div class="form-group row">
						    <label for="inp-Nama" class="col-sm-2 col-form-label">Judul Artikel</label>
						    <div class="col-sm-10">
						      <input type="text" class="form-control" name="Nama" id="inp-Nama" value="<?=$artikel[0]->judul;?>"  placeholder="">
						    </div>
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('Nama'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						   
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('kategori'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-penulis" class="col-sm-2 col-form-label">Penulis</label>
						    <div class="col-sm-10">
						      <input type="text" class="form-control" name="penulis" id="inp-penulis" value="<?=$artikel[0]->penulis;?>" placeholder="Sukarman, S.Pd.">
						    </div>
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('penulis'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-gambar" class="col-sm-2 col-form-label">Gambar</label>
						    <div class="col-sm-10">
						      <img src="<?=base_url()?>picture/<?=$artikel[0]->gambar;?>" height="200" width="auto"  alt=""class="img-thumbnail" style="max-width: 50%">
						      <input type="file" class="form-control" name="gambar" id="inp-gambar">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-isi" class="col-sm-2 col-form-label">Isi Artikel</label>
						    <div class="col-sm-10">
						    	<textarea class="form-control" name="isi" id="inp-isi"><?=$artikel[0]->isi;?></textarea>
						    </div>
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('isi'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-submit" class="col-sm-2 col-form-label"></label>
						    <div class="col-sm-10">
						    	<input type="submit" name="submit" class="btn btn-primary btn-block" value="Edit">
						    </div>
						  </div>
						  <?=form_close()?>
						<!-- </form> -->
      				</div>
      			</div>
      		</div>
      	</div>
    </div>
</div>
<script src="<?php echo base_url('asset/ckeditor/ckeditor.js');?>"></script>
<script type="text/javascript">
  $(document).ready(function() {
	 CKEDITOR.replace( 'inp-isi' );
  });
        // $(function () {
        //         CKEDITOR.replace('ckeditor',{
        //             filebrowserImageBrowseUrl : '<?php echo base_url('assets/kcfinder/browse.php');?>',
        //             height: '400px'             
        //         });
        //     });
</script>
<?php $this->load->view('template/footer'); ?>