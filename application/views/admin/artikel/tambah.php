<?php $this->load->view('template/header'); ?>
<style type="text/css">
	.gagal{
		color: red;
	}
</style>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
      	<div class="row">
      		<div class="col-lg-12">
      			<div class="card">
      				<div class="card-header row">
      					<div class="col-sm-12 col-md-6">
      						<h4 class="card-title">Tambah artikel</h4>
      					</div>
      				</div>
      				<div class="card-body">
      					<?=form_open_multipart('artikel/save/')?>
      					<!-- <form method="post" action="<?=base_url()?>artikel/save/" enctype="multipart/form-data"> -->
						  <div class="form-group row">
						    <label for="inp-Nama" class="col-sm-2 col-form-label">Judul artikel</label>
						    <div class="col-sm-10">
						      <input type="text" class="form-control" name="Nama" id="inp-Nama" value="<?=set_value('Nama');?>"  placeholder="">
						    </div>
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('Nama'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-harga" class="col-sm-2 col-form-label">Kategori</label>
						    <div class="col-sm-10">
						      <select class="form-control" required name="kategori" id="inp-kategori" value="<?=set_value('kategori');?>">
						      	<option></option>
						      	<option value="Berita">Berita</option>
						      	<option value="Sejarah-Desa">Sejarah Desa</option>
						      	<option value="Visi-Misi">Visi & Misi</option>
						      	<option value="Profil-Desa">Profil Desa</option>
						      </select>
						    </div>
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('kategori'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-penulis" class="col-sm-2 col-form-label">Penulis</label>
						    <div class="col-sm-10">
						      <input type="text" class="form-control" name="penulis" id="inp-penulis" value="<?=set_value('penulis');?>" placeholder="Sukarman, S.Pd.">
						    </div>
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('penulis'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-gambar" class="col-sm-2 col-form-label">Gambar</label>
						    <div class="col-sm-10">
						      <input type="file" class="form-control" required name="gambar" id="inp-gambar" value="<?=set_value('harga');?>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-isi" class="col-sm-2 col-form-label">Isi Artikel</label>
						    <div class="col-sm-10">
						    	<textarea class="form-control" name="isi" id="inp-isi"><?=set_value('isi');?></textarea>
						    </div>
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('isi'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-submit" class="col-sm-2 col-form-label"></label>
						    <div class="col-sm-10">
						    	<input type="submit" name="submit" class="btn btn-primary btn-block" value="Tambah">
						    </div>
						  </div>
						  <?=form_close()?>
						<!-- </form> -->
      				</div>
      			</div>
      		</div>
      	</div>
    </div>
</div>
<script src="<?php echo base_url('asset/ckeditor/ckeditor.js');?>"></script>
<script type="text/javascript">
  $(document).ready(function() {
	 CKEDITOR.replace( 'inp-isi' );
  });
</script>
<?php $this->load->view('template/footer'); ?>