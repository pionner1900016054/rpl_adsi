<?php $this->load->view('template/header'); ?>
<style type="text/css">
	.gagal{
		color: red;
	}
</style>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
      	<div class="row">
      		<div class="col-lg-12">
      			<div class="card">
      				<div class="card-header row">
      					<div class="col-sm-12 col-md-6">
      						<h4 class="card-title"><?=$event[0]->event;?></h4>
      					</div>
      				</div>
      				<div class="card-body">
						  <div class="form-group row">
						  	<div class="col-6">
						      	<img src="<?=base_url()?>picture/<?=$event[0]->gambar;?>" width="auto"  alt="" class="img-fluid img-thumbnail" style="max-width: 50%">
						  	</div>
						  	<div class="col-6">
						  		<div class="form-group row">
							    <div for="inp-kuota" class="col-sm-4 col-form-div">Kuota Peserta</div>
							    <div class="col-sm-6">
							    	<?=$event[0]->kuota;?> Orang
							    </div>
							  </div>
							  <div class="form-group row">
							  	<?php $mulai =explode(" ", $event[0]->tanggal_mulai);  ?>
							    <div for="inp-tanggal_mulai" class="col-sm-4 col-form-div">Tanggal Mulai</div>
							    <div class="col-sm-4">
							    	<?=$mulai[0];?>
							    </div>
							    <div class="col-sm-4">
							    	<?=$mulai[1];?>
							    </div>
							  </div>
							  <div class="form-group row">
							  	<?php $selesai =explode(" ", $event[0]->tanggal_selesai);  ?>
							    <div for="inp-tanggal_selesai" class="col-sm-4 col-form-div">Tanggal Selesai</div>
							    <div class="col-sm-4">
							    	<?=$selesai[0]?>
							    </div>
							    <div class="col-sm-4">
							    	<?=$selesai[1]?>
							    </div>
							  </div>
							  <div class="form-group row">
							    <div for="inp-keterangan" class="col-sm-4 col-form-div">Keterangan</div>
							    <div class="col-sm-6">
							    		<?=$event[0]->keterangan;?>
							  </div>
						  	</div>
						  </div>
      				</div>
      			</div>
      		</div>
      	</div>
    </div>
</div>
<?php $this->load->view('template/footer'); ?>