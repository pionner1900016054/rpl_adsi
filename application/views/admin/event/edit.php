<?php $this->load->view('template/header'); ?>
<style type="text/css">
	.gagal{
		color: red;
	}
</style>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
      	<div class="row">
      		<div class="col-lg-12">
      			<div class="card">
      				<div class="card-header row">
      					<div class="col-sm-12 col-md-6">
      						<h4 class="card-title">Edit event</h4>
      					</div>
      				</div>
      				<div class="card-body">
      					<?=form_open_multipart('event/update/'.$event[0]->id_event)?>
      					<!-- <form method="post" action="<?=base_url()?>event/update/<?=$event[0]->id_event?>" enctype="multipart/form-data"> -->
						  <div class="form-group row">
						    <label for="inp-Nama" class="col-sm-2 col-form-label">Nama event</label>
						    <div class="col-sm-10">
						      <input type="text" class="form-control" name="Nama" id="inp-Nama" value="<?=$event[0]->event;?>"  placeholder="Tema Retro">
						    </div>
						  </div>
						  <div class="row">
						  	<div class="col-sm-2"></div>
						    <div class="col-sm-10 gagal">
						    	<?php echo form_error('Nama'); ?>
						    		
						    	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-kuota" class="col-sm-2 col-form-label">Kuota Peserta</label>
						    <div class="col-sm-10">
						      <input type="number" class="form-control" minlength="1" name="kuota" id="inp-kuota" value="<?=$event[0]->kuota;?>" placeholder="1000">
						    </div>
						    <div class="row">
						    <div class="col-sm-2"></div>
						    <div class="col-sm-10 gagal">
						    	<?php echo form_error('kuota'); ?>
						    </div>
						  </div>
						  </div>
						  <div class="form-group row">
						  	<?php $mulai =explode(" ", $event[0]->tanggal_mulai);  ?>
						    <label for="inp-tanggal_mulai" class="col-sm-2 col-form-label">Tanggal Mulai</label>
						    <div class="col-sm-6">
						      <input type="date" class="datepicker form-control" name="tanggal_mulai" id="inp-tanggal_mulai" value="<?=$mulai[0];?>">
						    </div>
						    <div class="col-sm-4">
						      <input type="time" class="datepicker form-control" name="waktu_mulai" id="inp-waktu_mulai" value="<?=$mulai[1];?>">
						    </div>
						    <div class="row">
						    <div class="col-sm-2"></div>
						    <div class="col-sm-10 gagal">
						    	<?php echo form_error('tanggal_mulai'); ?>
						    	<?php echo form_error('waktu_mulai'); ?>
						    </div>
						  </div>
						  </div>
						  <div class="form-group row">
						  	<?php $selesai =explode(" ", $event[0]->tanggal_selesai);  ?>
						    <label for="inp-tanggal_selesai" class="col-sm-2 col-form-label">Tanggal Selesai</label>
						    <div class="col-sm-6">
						      <input type="date" class="form-control" name="tanggal_selesai" id="inp-tanggal_selesai" value="<?=$selesai[0]?>">
						    </div>
						    <div class="col-sm-4">
						      <input type="time" class="datepicker form-control" name="waktu_selesai" id="inp-waktu_selesai" value="<?=$selesai[1]?>">
						    </div>
						    <div class="row">
						    <div class="col-sm-2"></div>
						    <div class="col-sm-10 gagal">
						    	<?php echo form_error('tanggal_selesai'); ?>
						    	<?php echo form_error('waktu_selesai'); ?>
						    </div>
						  </div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-gambar" class="col-sm-2 col-form-label">Gambar</label>
						    <div class="col-sm-10">
						      <img src="<?=base_url()?>picture/<?=$event[0]->gambar;?>" height="200" width="auto"  alt=""class="img-thumbnail" style="max-width: 50%">
						      <input type="file" class="form-control" name="gambar" id="inp-gambar">
						    </div>
						  </div>
						  <div class="row">
						    <div class="col-sm-2"></div>
						    <div class="col-sm-10 gagal">
						    	<?php echo form_error('gambar'); ?>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-keterangan" class="col-sm-2 col-form-label">Deskripsi</label>
						    <div class="col-sm-10">
						    	<textarea class="form-control" name="keterangan" id="inp-keterangan"><?=$event[0]->keterangan;?></textarea>
						    </div>
						  </div>
						  <div class="row">
						    <div class="col-sm-2"></div>
						    <div class="col-sm-10 gagal">
						    	<?php echo form_error('keterangan'); ?>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-submit" class="col-sm-2 col-form-label"></label>
						    <div class="col-sm-10">
						    	<input type="submit" name="submit" class="btn btn-primary btn-block" value="Tambah">
						    </div>
						  </div>
						  <?=form_close()?>
						<!-- </form> -->
      				</div>
      			</div>
      		</div>
      	</div>
    </div>
</div>
<script src="<?php echo base_url('asset/ckeditor/ckeditor.js');?>"></script>
<script type="text/javascript">
  $(document).ready(function() {
	 CKEDITOR.replace( 'inp-keterangan' );
  });
        // $(function () {
        //         CKEDITOR.replace('ckeditor',{
        //             filebrowserImageBrowseUrl : '<?php echo base_url('assets/kcfinder/browse.php');?>',
        //             height: '400px'             
        //         });
        //     });
</script>
<?php $this->load->view('template/footer'); ?>