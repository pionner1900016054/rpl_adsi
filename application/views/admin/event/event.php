<?php $this->load->view('template/header'); ?>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
      	<div class="row">
      		<div class="col-lg-12">
      			<div class="card">
      				<div class="card-header ">
		                <div class="row">
      					<div class="col-sm-12 col-md-6">
      						<h4 class="card-title">Tabel event</h4>
      					</div>
		                <div class="col-sm-12 col-md-6"><a type="button" class="btn btn-primary btn-sm float-right" href="<?=base_url()?>event/tambah"><i class="fa fa-plus"></i> event</a>
		                </div>
		            	</div>
      				</div>
      				<div class="card-body">
      					<div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
		                  	<div class="row">
		                    	<div class="col-sm-12 col-md-6"></div>
		                    	<div class="col-sm-12 col-md-6"></div>
		                  	</div>
			                <div class="row">
			                  	<div class="col-sm-12">
			                  		<table id="example2" class="example2 table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
			                  			<thead>
						                  <tr role="row">
						                    <th>Tanggal Mulai</th>
						                    <th>Tanggal Selesai</th>
						                    <th>Nama Event</th>
						                    <!-- <th>Kuota event</th> -->
						                    <th>Status Event</th>
						                    <!-- <th>Deskripsi</th> -->
						                    <!-- <th>Gambar</th> -->
						                    <th>Aksi</th>
						                  </tr>
			                  			</thead>
			                  			<tbody>
			                  				<?php foreach ($event as $key) { ?>
			                  				<tr>
			                  					<td><?=$key->tanggal_mulai;?></td>
			                  					<td><?=$key->tanggal_selesai;?></td>
			                  					<td><?=$key->event;?></td>
			                  					<td><?=$key->kuota;?></td>
			                  					<!-- <td><?=$key->status;?></td> -->
			                  					<!-- <td><?=$key->deskripsi_event;?></td> -->
			                  					<!-- <td><img class="img-thumbnail" height="200" width="auto"  alt="" src="<?=base_url()?>picture/<?=$key->gambar_event?>"></td> -->
			                  					<td>
			                  						<a type="button" class="btn btn-secondary btn-sm" href="<?=base_url()?>event/detail/<?=$key->id_event?>"><i class="fa fa-eye"></i></a>
			                  						<a type="button" class="btn btn-warning btn-sm text-light" href="<?=base_url()?>event/edit/<?=$key->id_event?>"><i class="fa fa-pen"></i></a>
			                  						<a type="button" class="btn btn-danger btn-sm text-light" onclick="hapus(<?=$key->id_event?>)"><i class="fa fa-trash"></i></a>
			                  					</td>
			                  				</tr>
			                  				<?php  } ?>
			                  			</tbody>
			                		</table>
			                	</div>
			                </div>
		            	</div>
      				</div>
      			</div>
      		</div>
      	</div>
    </div>
</div>
<script type="text/javascript">
	function hapus(id) {
		var konfirmasi = confirm('Anda yakin akan menghapus?');
		if (konfirmasi == true) {
			window.location.href = '<?=base_url()?>event/hapus/'+id;
		}
	}
</script>
<?php $this->load->view('template/footer'); ?>