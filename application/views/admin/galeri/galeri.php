<?php $this->load->view('template/header');?>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <h4 class="card-title">Galeri</h4>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <a type="button" class="btn btn-primary btn-sm float-right"
                                    href="<?=base_url()?>galeri/tambah"><i class="fa fa-plus"></i> galeri</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body row">
                        <?php foreach ($galeri as $key) {?>
                        <div class="col-3">
                            <div class="card">
                                <div class="card-body">
                                    <img src="<?=base_url()?>galeri/<?=$key->gambar?>" class="img-thumbnail" alt=""
                                        style="width: 100%;height: 300px;">
                                    <p class="text-small text-center"><?=substr($key->judul, 0, 38) . ".."?></p>
                                    <p class="text-muted text-center"><?=$key->date?></p>

                                </div>
                                <div class="card-footer text-center">
                                    <a type="button" style="color: white" class="btn btn-warning btn-sm"
                                        href="<?=base_url()?>galeri/edit/<?=$key->id_galeri?>"><i
                                            class="fa fa-pen"></i></a>
                                    <a style="color: white" onclick="hapus(<?=$key->id_galeri?>)" type="button"
                                        class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                </div>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                    <div class="card-footer">
                        <!--Tampilkan pagination-->
                        <?php echo $pagination; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('asset/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript">
function hapus(id) {
    var konfirmasi = confirm('Anda yakin akan menghapus?');
    if (konfirmasi == true) {
        window.location.href = '<?=base_url()
?>galeri/hapus/' + id;
    }
}
</script>
<?php $this->load->view('template/footer');?>
