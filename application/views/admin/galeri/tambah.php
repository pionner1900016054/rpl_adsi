<?php $this->load->view('template/header'); ?>
<style type="text/css">
	.gagal{
		color: red;
	}
</style>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
      	<div class="row">
      		<div class="col-lg-12">
      			<div class="card">
      				<div class="card-header row">
      					<div class="col-sm-12 col-md-6">
      						<h4 class="card-title">Tambah galeri</h4>
      					</div>
      				</div>
      				<div class="card-body">
      					<?=form_open_multipart('galeri/save/')?>
      					<!-- <form method="post" action="<?=base_url()?>galeri/save/" enctype="multipart/form-data"> -->
						  <div class="form-group row">
						    <label for="inp-Nama" class="col-sm-2 col-form-label">Judul</label>
						    <div class="col-sm-10">
						      	<input type="text" class="form-control" name="Nama" id="inp-Nama" value="<?=set_value('Nama');?>"  placeholder="Pasar Malam">
						    </div>
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('Nama'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-gambar" class="col-sm-2 col-form-label">Gambar</label>
						    <div class="col-sm-10">
						      <input type="file" class="form-control" required name="gambar" id="inp-gambar" value="<?=set_value('gambar');?>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-submit" class="col-sm-2 col-form-label"></label>
						    <div class="col-sm-10">
						    	<input type="submit" name="submit" class="btn btn-primary btn-block" value="Tambah">
						    </div>
						  </div>
						  <?=form_close()?>
						<!-- </form> -->
      				</div>
      			</div>
      		</div>
      	</div>
    </div>
</div>
<?php $this->load->view('template/footer'); ?>