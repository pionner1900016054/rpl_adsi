<?php $this->load->view('template/header');?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-sm-12 col-md-6">
                            <h4 class="card-title">Pesanan</h4>
                        </div>
                        <!-- <div class="col-sm-12 col-md-6"><a type="button" class="btn btn-primary btn-sm float-right" href="<?=base_url()?>produk/tambah"><i class="fa fa-plus"></i> produk</a></div> -->
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-primary"><i class="fa fa-shopping-cart"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Pesanan</span>
                                        <span class="info-box-number"><?=count($transaksi)?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green"><i class="fa fa-shopping-cart"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Pesanan Selesai</span>
                                        <span class="info-box-number"><?=count($transaksi_done)?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-danger"><i class="fa fa-shopping-cart"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Pesanan Batal</span>
                                        <span class="info-box-number"><?=count($transaksi_cancel)?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-sm-12 col-md-6">
                            <h4 class="card-title">Komponen</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-info"><i class="fa fa-clipboard"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Produk</span>
                                        <span class="info-box-number"><?=count($transaksi)?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-yellow "><i
                                            class="fa fa-calendar text-light"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Event</span>
                                        <span class="info-box-number"><?=count($event)?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-secondary"><i class="fa fa-newspaper"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Berita</span>
                                        <span class="info-box-number"><?=count($berita)?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-orange"><i class="fa fa-users text-light"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">User</span>
                                        <span class="info-box-number"><?=count($user)?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm col-md-6">
                                <h4 class="card-title">Produk Limit</h4>
                            </div>
                            <div class="col-sm col-md-6">
                                <h4 class="card-title ">Grafik Penjualan Tiket dan Produk UMKM</h4>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm table-responsive">
                                <table id="example2"
                                    class="example2 table table-bordered table-hover dataTable dtr-inline" role="grid"
                                    aria-describedby="example2_info">
                                    <thead>
                                        <tr role="row">
                                            <th>No</th>
                                            <th>Nama produk</th>
                                            <th>Kategori</th>
                                            <th>Stok</th>
                                            <!-- <th width="350">Gambar</th> -->
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1;foreach ($produk_limit as $key) {?>
                                        <tr class="table-danger">
                                            <td><?=$no;?></td>
                                            <td><?=$key->produk;?></td>
                                            <td><?=$key->kategori;?></td>
                                            <td><?=$key->stok;?></td>
                                            <!-- <td><?=$key->keterangan;?></td> -->
                                            <!-- <td><img class="img-thumbnail" height="200" width="auto"  alt="" src="<?=base_url()?>picture/<?=$key->gambar?>"></td> -->
                                            <td>
                                                <a type="button" class="btn btn-secondary btn-sm"
                                                    href="<?=base_url()?>produk/detail/<?=$key->id_produk?>"><i
                                                        class="fa fa-eye"></i></a>
                                                <a type="button" style="color: white" class="btn btn-warning btn-sm"
                                                    href="<?=base_url()?>produk/edit/<?=$key->id_produk?>"><i
                                                        class="fa fa-pen"></i></a>
                                                <a type="button" style="color: white" class="btn btn-danger btn-sm"
                                                    onclick="hapus(<?=$key->id_produk?>)"><i
                                                        class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php $no++;}?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm col-md-6">
                                <div id="graph" style="max-width: 90%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
new Morris.Bar({

    element: 'graph',
    // width:100%,
    data: <?=$graph?>,
    xkey: 'kategori',
    ykeys: ['jumlah'],
    labels: ['jumlah']
});
</script>
<?php $this->load->view('template/footer');?>