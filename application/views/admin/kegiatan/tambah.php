<?php $this->load->view('template/header');?>
<style type="text/css">
.gagal {
    color: red;
}
</style>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header row">
                        <div class="col-sm-12 col-md-6">
                            <h4 class="card-title">Tambah produk</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <?=form_open_multipart('kegiatan/save/')?>
                        <!-- <form method="post" action="<?=base_url()?>produk/save/" enctype="multipart/form-data"> -->
                        <div class="form-group row">
                            <label for="inp-Nama" class="col-sm-2 col-form-label">Nama kegiatan</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="Nama" id="inp-Nama"
                                    value="<?=set_value('Nama');?>" placeholder="Belajar Menanam Ubi Cilembu">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10 gagal">
                                <?php echo form_error('Nama'); ?>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inp-harga" class="col-sm-2 col-form-label">Durasi</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="durasi" id="inp-durasi"
                                    value="<?=set_value('durasi');?>" placeholder="30 Menit">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10 gagal">
                                <?php echo form_error('durasi'); ?>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="inp-harga" class="col-sm-2 col-form-label">Harga</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="harga" id="inp-harga"
                                    value="<?=set_value('harga');?>" placeholder="1400000">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10 gagal">
                                <?php echo form_error('harga'); ?>
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="inp-keterangan" class="col-sm-2 col-form-label">keterangan</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="keterangan"
                                    id="inp-keterangan"><?=set_value('keterangan');?></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10 gagal">
                                <?php echo form_error('keterangan'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inp-submit" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-10">
                                <input type="submit" name="submit" class="btn btn-primary btn-block" value="Tambah">
                            </div>
                        </div>

                        <?=form_close();?>
                        <!-- </form> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('asset/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    CKEDITOR.replace('inp-keterangan');
});
</script>
<?php $this->load->view('template/footer');?>