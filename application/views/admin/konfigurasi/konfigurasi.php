<?php $this->load->view('template/header'); ?>
<style type="text/css">
	.gagal{
		color: red;
	}
</style>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
      	<div class="row">
      		<div class="col-lg-12">
      			<div class="card">
      				<div class="card-header">
      					<div class="col-sm-12 col-md-6">
      						<h4 class="card-title">Setting Konfigurasi</h4>
      					</div>
      				</div>
      				<div class="card-body">
      					<?=form_open_multipart('konfigurasi/update/'.$konfigurasi[0]->id_konfigurasi,array('id'=>'form_konf'))?>
      					<!-- <form method="post" action="<?=base_url()?>galeri/save/" enctype="multipart/form-data"> -->
						  <div class="form-group row">
						    <label for="inp-gambar" class="col-sm-2 col-form-label">Logo Aplikasi</label>
						    <div class="col-sm-10">
						      	<img src="<?=base_url()?>logo/<?=$konfigurasi[0]->logo_app;?>" height="200" width="auto"  alt=""class="img-thumbnail" style="max-width: 50%">
						      <input type="file" class="form-control" name="gambar" id="inp-gambar">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-email" class="col-sm-2 col-form-label">Email</label>
						    <div class="col-sm-10">
						      	<input type="email" class="form-control" name="email" id="inp-email" value="<?=$konfigurasi[0]->email;?>" >
						    </div>
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('email'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-email" class="col-sm-2 col-form-label">Embed Google Maps</label>
						    <div class="col-sm-10">
						    	<textarea name="embed" class="form-control"><?=$konfigurasi[0]->embed_maps;?></textarea>
						    </div>
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('embed'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-kontak1" class="col-sm-2 col-form-label">Kontak Person 1</label>
						    <div class="col-sm-10">
						      	<input type="text" class="form-control" name="kontak_satu" id="inp-kontak1" value="<?=$konfigurasi[0]->kontak_satu;?>" >
						    </div>
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('kontak1'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-kontak2" class="col-sm-2 col-form-label">Kontak Person 2</label>
						    <div class="col-sm-10">
						      	<input type="text" class="form-control" name="kontak_dua" id="inp-kontak2" value="<?=$konfigurasi[0]->kontak_dua;?>" >
						    </div>
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('kontak2'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-kontak3" class="col-sm-2 col-form-label">Kontak Person 3</label>
						    <div class="col-sm-10">
						      	<input type="text" class="form-control" name="kontak_tiga" id="inp-kontak3" value="<?=$konfigurasi[0]->kontak_tiga;?>" >
						    </div>
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('kontak3'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-submit" class="col-sm-2 col-form-label"></label>
						    <div class="col-sm-10">
						    	<input type="submit" name="submit" class="btn btn-primary btn-block" value="Simpan">
						    </div>
						  </div>
						  <?=form_close()?>
						  <div class="form-group row">
						    <label for="inp-submit" class="col-sm-2 col-form-label"></label>
						    <div class="col-sm-10">
						    	<button class="btn btn-warning btn-block" id="btnEdit">Edit</button>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-submit" class="col-sm-2 col-form-label"></label>
						    <div class="col-sm-10">
						    	<button class="btn btn-secondary btn-block" id="btnCancel">Cancel</button>
						    </div>
						  </div>
						<!-- </form> -->
      				</div>
      			</div>
      		</div>
      	</div>
    </div>
</div>
<script type="text/javascript">
	function set_disabled() {
		$('input').attr('disabled','disabled');
		$('textarea').attr('disabled','disabled');
		$('#btnCancel').hide();
		$('#btnEdit').show();
	}
	function set_enabled() {
		$('input').removeAttr('disabled');
		$('textarea').removeAttr('disabled');
		$('#btnCancel').show();
		$('#btnEdit').hide();
	}
	$(document).ready(function() {
		set_disabled();
		$('#btnEdit').click(function(){
			set_enabled();
		});
		$('#btnCancel').click(function(){
			set_disabled();
		});

		// $('.form-group').prop("disabled", true);
	});
</script>
<?php $this->load->view('template/footer'); ?>