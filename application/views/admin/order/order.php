<?php $this->load->view('template/header');?>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header row">
                        <div class="col-sm-12 col-md-6">
                            <h4 class="card-title">Tabel Pesanan</h4>
                        </div>
                        <!-- <div class="col-sm-12 col-md-6"><a type="button" class="btn btn-primary btn-sm float-right" href="<?=base_url()?>produk/tambah"><i class="fa fa-plus"></i> produk</a></div> -->
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-primary"><i class="fa fa-shopping-cart"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Pesanan</span>
                                        <span class="info-box-number"><?=count($transaksi)?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green"><i class="fa fa-shopping-cart"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Pesanan Selesai</span>
                                        <span class="info-box-number"><?=count($transaksi_done)?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-danger"><i class="fa fa-shopping-cart"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Pesanan Batal</span>
                                        <span class="info-box-number"><?=count($transaksi_cancel)?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        </div>
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-sm-12 col-md-6"></div>
                                <div class="col-sm-12 col-md-6"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example3"
                                        class="example3 table table-bordered table-hover dataTable dtr-inline"
                                        role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr role="row">
                                                <th>ID</th>
                                                <th>Tanggal order</th>
                                                <th>Dipesan oleh</th>
                                                <th>Status Bukti Bayar</th>
                                                <th>Verifikasi Bukti Bayar</th>
                                                <th>Status Transaksi Pesanan</th>
                                                <!-- <th>Stok</th> -->
                                                <!-- <th width="350">Gambar</th> -->
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 1;foreach ($transaksi as $key) {?>
                                            <tr>
                                                <td><?=$key->id_transaksi;?></td>
                                                <td><?=$key->date;?></td>
                                                <td><?=$key->member;?></td>
                                                <td>
                                                    <?php
if ($key->statusbukti == 0) {
    echo "Menunggu Bukti Pembayaran";
} elseif ($key->statusbukti == 1) {?>
                                                    <a href="<?=base_url()?>pembayaran/<?=$key->buktibayar?>"
                                                        target="_blank">Lihat Bukti Bayar</a>
                                                    <?php }?>
                                                </td>
                                                <td>
                                                    <?php
if ($key->verifikasibukti == 0) {
    echo "Menunggu Bukti Pembayaran";
} elseif ($key->verifikasibukti == 1) {
    echo "Bukti Pembayaran Valid";
} elseif ($key->verifikasibukti == 2) {
    echo "Bukti Pembayaran Tidak Valid";
}
    ?>
                                                </td>
                                                <td>
                                                    <?php
if ($key->statustransaksi == 0) {
        echo "Belum diproses";
    } elseif ($key->statustransaksi == 1) {
        echo "Sedang diproses";
    } elseif ($key->statustransaksi == 2) {
        echo "Selesai diproses";
    } elseif ($key->statustransaksi == 3) {
        echo "Cancel. Kadaluarsa";
    }
    ?>
                                                </td>
                                                <td>
                                                    <!--<?=date('Y-m-d H:i:s', strtotime('+1 day', strtotime($key->date)));?>-->
                                                    <?php if (date('Y-m-d H:i:s') > date('Y-m-d H:i:s', strtotime('+1 day', strtotime($key->date)))) {
        if ($key->statusbukti == 0) {
            if ($key->verifikasibukti == 0 || $key->verifikasibukti == 2) {
                if ($key->statustransaksi != 3) {
                    ?>
                                                    <a type="button" class="btn btn-danger btn-sm"
                                                        href="<?=base_url()?>order/cancel_restore/<?=$key->id_transaksi?>">Cancel
                                                        &amp; Restore</a>
                                                    <?php }}}}?>
                                                    <a type="button" class="btn btn-secondary btn-sm"
                                                        href="<?=base_url()?>order/detail/<?=$key->id_transaksi?>"><i
                                                            class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                            <?php $no++;}?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function hapus(id) {

    var konfirmasi = confirm('Anda yakin akan menghapus?');
    if (konfirmasi == true) {
        window.location.href = '<?=base_url()?>produk/hapus/' + id;
    }
}
</script>
<?php $this->load->view('template/footer');?>
