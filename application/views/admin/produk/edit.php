<?php $this->load->view('template/header');?>
<style type="text/css">
.gagal {
    color: red;
}
</style>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header row">
                        <div class="col-sm-12 col-md-6">
                            <h4 class="card-title">Tambah produk</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <?=form_open_multipart('produk/update/' . $produk[0]->id_produk)?>
                        <!-- <form method="post" action="<?=base_url()?>produk/update/<?=$produk[0]->id_produk?>" enctype="multipart/form-data"> -->
                        <div class="form-group row">
                            <label for="inp-Nama" class="col-sm-2 col-form-label">Nama produk</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="Nama" id="inp-Nama"
                                    value="<?=$produk[0]->produk;?>" placeholder="Tema Retro">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10 gagal">
                                <?php echo form_error('Nama'); ?>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inp-kategori" class="col-sm-2 col-form-label">kategori</label>
                            <div class="col-sm-10">
                                <select class="form-control" required name="kategori" id="inp-kategori"
                                    value="<?=$produk[0]->kategori;?>">
                                    <option></option>
                                    <option value="Tiket" <?php if ($produk[0]->kategori == 'Tiket'): ?> selected
                                        <?php endif?>>Tiket</option>
                                    <option value="Produk" <?php if ($produk[0]->kategori == 'Produk'): ?> selected
                                        <?php endif?>>Produk</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10 gagal">
                                <?php echo form_error('kategori'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inp-harga" class="col-sm-2 col-form-label">Harga</label>
                            <div class="col-sm-10">
                                <input type="number" step=any class="form-control" name="harga" id="inp-harga"
                                    value="<?=$produk[0]->harga;?>" placeholder="1400000">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10 gagal">
                                <?php echo form_error('harga'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inp-berat" class="col-sm-2 col-form-label">Berat</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">KG</div>
                                    </div>
                                    <input type="number" step="any" min="0" class="form-control" name="berat"
                                        id="inp-berat" value="<?=$produk[0]->berat;?>" placeholder="1">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10 gagal">
                                <?php echo form_error('berat'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inp-stok" class="col-sm-2 col-form-label">Stok</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="stok" id="inp-stok"
                                    value="<?=$produk[0]->stok;?>" placeholder="100">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10 gagal">
                                <?php echo form_error('stok'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inp-gambar" class="col-sm-2 col-form-label">Gambar</label>
                            <div class="col-sm-10">
                                <img src="<?=base_url()?>picture/<?=$produk[0]->gambar;?>" height="200" width="200"
                                    alt="" class="img-thumbnail">
                                <input type="file" class="form-control" name="gambar" id="inp-gambar">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10 gagal">
                                <?php echo form_error('gambar'); ?>
                            </div>
                        </div>
                        <?php if ($produk[0]->kategori == "Tiket"): ?>
                        <div class="form-group row">
                            <label for="inp-harga" class="col-sm-2 col-form-label">Kegiatan</label>
                            <div class="col-sm-10">
                                <a class="btn btn-info text-white" data-toggle="modal"
                                    data-target="#exampleModal">Kelola Kegiatan Tiket</a>
                            </div>
                        </div>
                        <?php endif?>

                        <div class="form-group row">
                            <label for="inp-deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="keterangan"
                                    id="inp-deskripsi"><?=$produk[0]->keterangan;?></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10 gagal">
                                <?php echo form_error('deskripsi'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inp-submit" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-10">
                                <input type="submit" name="submit" class="btn btn-primary btn-block" value="Edit">
                            </div>
                        </div>
                        <?=form_close();?>
                        <!-- </form> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <?=form_open_multipart('produk/tambah_kegiatan/')?>
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Kelola Kegiatan Pada Tiket Ini</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Perhatian!</strong> Kamu cukup pilih salah satu jenis kegiatan yang ingin ditambahkan.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <input type="hidden" name="id_produk" value="<?=$this->uri->segment(3)?>">


                <div class="form-group">
                    <label for="inp-stok" class="col-sm-2 col-form-label">Kegiatan</label>
                    <div class="col-sm-12">
                        <select type="text" class="form-control" name="kegiatan" id="inp-kegiatan">

                            <?php foreach ($kegiatan as $k): ?>
                            <option value="<?=$k->id_kegiatan?>"><?=$k->Nama_kegiatan?></option>
                            <?php endforeach;?>

                        </select>
                    </div>
                </div>

                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Kegiatan</th>
                            <th scope="col">Aksi</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;?>
                        <?php foreach ($det_kegiatan as $item): ?>
                        <tr>
                            <th scope="row"><?=$no++?></th>
                            <td><?=$item->Nama_kegiatan?></td>
                            <td><a href="<?=base_url('produk/hapus_kegiatan_tiket/' . $item->id_kegiatan . '/' . $this->uri->segment(3))?>"
                                    class="btn btn-danger text-white btn-sm"><i class="fas fa-trash"></i>
                                    Hapus</a>
                            </td>

                        </tr>
                        <?php endforeach?>

                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="tambah_kategori">Tambah</button>
            </div>
        </div>
        <?=form_close();?>
    </div>
</div>

<script src="<?php echo base_url('asset/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    CKEDITOR.replace('inp-deskripsi');
});
// $(function () {
//         CKEDITOR.replace('ckeditor',{
//             filebrowserImageBrowseUrl : '<?php echo base_url('assets/kcfinder/browse.php'); ?>',
//             height: '400px'
//         });
//     });
</script>
<?php $this->load->view('template/footer');?>
