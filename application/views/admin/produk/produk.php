<?php $this->load->view('template/header');?>
<?php
function rupiah($angka)
{

    $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
    return $hasil_rupiah;

}
?>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <h4 class="card-title">Tabel produk</h4>
                            </div>
                            <div class="col-sm-12 col-md-6"><a type="button" class="btn btn-primary btn-sm float-right"
                                    href="<?=base_url()?>produk/tambah"><i class="fa fa-plus"></i> produk</a></div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-sm-12 col-md-6"></div>
                                <div class="col-sm-12 col-md-6"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example2"
                                        class="example2 table table-bordered table-hover dataTable dtr-inline"
                                        role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr role="row">
                                                <th>No</th>
                                                <th>Nama produk</th>
                                                <th>Kategori</th>
                                                <th>Harga</th>
                                                <th>Satuan Barang</th>
                                                <th>Stok</th>
                                                <!-- <th width="350">Gambar</th> -->
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 1;foreach ($produk as $key) {?>
                                            <tr>
                                                <td><?=$no;?></td>
                                                <td><?=$key->produk;?></td>
                                                <td><?=$key->kategori;?></td>
                                                <td><?=rupiah($key->harga);?></td>
                                                <td><?=$key->berat;?> <?=($key->kategori == "Tiket" ? "Waktu" : "KG")?>
                                                </td>
                                                <td><?=$key->stok;?></td>

                                                <td>
                                                    <a type="button" class="btn btn-secondary btn-sm"
                                                        href="<?=base_url()?>produk/detail/<?=$key->id_produk?>"><i
                                                            class="fa fa-eye"></i></a>
                                                    <a type="button" style="color: white" class="btn btn-warning btn-sm"
                                                        href="<?=base_url()?>produk/edit/<?=$key->id_produk?>"><i
                                                            class="fa fa-pen"></i></a>
                                                    <a type="button" style="color: white" class="btn btn-danger btn-sm"
                                                        onclick="hapus(<?=$key->id_produk?>)"><i
                                                            class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <?php $no++;}?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function hapus(id) {
    var konfirmasi = confirm('Anda yakin akan menghapus?');
    if (konfirmasi == true) {
        window.location.href = '<?=base_url()?>produk/hapus/' + id;
    }
}
</script>
<?php $this->load->view('template/footer');?>
