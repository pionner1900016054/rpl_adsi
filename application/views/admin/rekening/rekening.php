<?php $this->load->view('template/header'); ?>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
      	<div class="row">
      		<div class="col-lg-12">
      			<div class="card">
      				<div class="card-header">
		                <div class="row">
      					<div class="col-sm-12 col-md-6">
      						<h4 class="card-title">Tabel Rekening Bank</h4>
      					</div>
		                <div class="col-sm-12 col-md-6"><a type="button" class="btn btn-primary btn-sm float-right" href="<?=base_url()?>rekening/tambah"><i class="fa fa-plus"></i> rekening</a></div></div>
      				</div>
      				<div class="card-body">
      					<div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
		                  	<div class="row">
		                    	<div class="col-sm-12 col-md-6"></div>
		                    	<div class="col-sm-12 col-md-6"></div>
		                  	</div>
			                <div class="row">
			                  	<div class="col-sm-12">
			                  		<table id="example2" class="example2 table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
			                  			<thead>
						                  <tr role="row">
						                    <th>No</th>
						                    <th>Bank</th>
						                    <th>Nomor Rekening</th>
						                    <th>Nama Rekening</th>
						                    <th>Aksi</th>
						                  </tr>
			                  			</thead>
			                  			<tbody>
			                  				<?php $no=1; foreach ($rekening as $key) { ?>
			                  				<tr>
			                  					<td><?=$no;?></td>
			                  					<td><?=$key->bank;?></td>
			                  					<td><?=$key->no_rekening;?></td>
			                  					<td><?=$key->Nama_rekening;?></td>
			                  					<td>
			                  						<!-- <a type="button" class="btn btn-secondary btn-sm" href="<?=base_url()?>rekening/detail/<?=$key->id_rekening?>"><i class="fa fa-eye"></i></a> -->
			                  						<a type="button" class="btn btn-warning btn-sm text-light" href="<?=base_url()?>rekening/edit/<?=$key->id_rekening?>"><i class="fa fa-pen"></i></a>
			                  						<a type="button" class="btn btn-danger btn-sm text-light" onclick="hapus(<?=$key->id_rekening?>)"><i class="fa fa-trash"></i></a>
			                  					</td>
			                  				</tr>
			                  				<?php $no++; } ?>
			                  			</tbody>
			                		</table>
			                	</div>
			                </div>
		            	</div>
      				</div>
      			</div>
      		</div>
      	</div>
    </div>
</div>
<script type="text/javascript">
	function hapus(id) {
		var konfirmasi = confirm('Anda yakin akan menghapus?');
		if (konfirmasi == true) {
			window.location.href = '<?=base_url()?>rekening/hapus/'+id;
		}
	}
</script>
<?php $this->load->view('template/footer'); ?>