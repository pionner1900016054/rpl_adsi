<?php $this->load->view('template/header'); ?>
<style type="text/css">
	.gagal{
		color: red;
	}
</style>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
      	<div class="row">
      		<div class="col-lg-12">
      			<div class="card">
      				<div class="card-header row">
      					<div class="col-sm-12 col-md-6">
      						<h4 class="card-title">Tambah rekening</h4>
      					</div>
      				</div>
      				<div class="card-body">
      					<?=form_open_multipart('rekening/save/')?>
      					<!-- <form method="post" action="<?=base_url()?>rekening/save/" enctype="multipart/form-data"> -->
						  <div class="form-group row">
						    <label for="inp-bank" class="col-sm-2 col-form-label">Nama Bank</label>
						    <div class="col-sm-10">
						      <input type="text" class="form-control" name="bank" id="inp-bank" value="<?=set_value('bank');?>"  placeholder="ex BCA MANDIRI BRI BNI BPD JATIM">
						    </div>
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('bank'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-no" class="col-sm-2 col-form-label">Nomor Rekening</label>
						    <div class="col-sm-10">
						      <input type="text" class="form-control" name="no" id="inp-no" value="<?=set_value('no');?>"  placeholder="13989123219">
						    </div>
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('no'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-Nama" class="col-sm-2 col-form-label">Nama rekening</label>
						    <div class="col-sm-10">
						      <input type="text" class="form-control" name="Nama" id="inp-Nama" value="<?=set_value('Nama');?>"  placeholder="Desa Wisata Default">
						    </div>
						    <div class="row">
							    <div class="col-sm-2"></div>
							    <div class="col-sm-10 gagal">
							    	<?php echo form_error('Nama'); ?>
							    </div>
						  	</div>
						  </div>
						  <div class="form-group row">
						    <label for="inp-submit" class="col-sm-2 col-form-label"></label>
						    <div class="col-sm-10">
						    	<input type="submit" name="submit" class="btn btn-primary btn-block" value="Tambah">
						    </div>
						  </div>
						<!-- </form> -->
						<?=form_close()?>
      				</div>
      			</div>
      		</div>
      	</div>
    </div>
</div>
<?php $this->load->view('template/footer'); ?>