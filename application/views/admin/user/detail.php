<?php $this->load->view('template/header'); ?>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
      	<div class="row">
      		<div class="col-lg-12">
      			<div class="card">
                              <div class="card-header ">
                                    <div class="row">
            					<div class="col-sm-10">
            						<h4 class="card-title">Data User </h4>
            					</div>
                                          <div class="col-sm">
                                                <?php  if ($user[0]->status == 1) {?>
                                                <a type="button" class="btn btn-danger btn-sm text-light" onclick="hapus(<?=$user[0]->id_user?>,3)"><i class="fa fa-lock"></i> blokir</a>
                                                <?php }elseif ($user[0]->status == 3 ) {?>
                                                <a type="button" class="btn btn-success btn-sm text-light" onclick="hapus(<?=$user[0]->id_user?>,1)"><i class="fa fa-unlock"></i> buka blokir</a>
                                                <?php } ?>
                                          </div>
                                    </div>
      				<div class="card-body" style="height: 600px">
                                    <hr>
                                    <img src="<?=base_url()?>avatar/<?=$user[0]->avatar?>" class="rounded-circle" width="100" height="100" alt="logo">
                                    <hr>
                                    <div class="row">
                                          <table width="80%">
                                                <tr>
                                                      <td>Nama</td>
                                                      <td><?=$user[0]->member?></td>
                                                </tr>
                                                <tr>
                                                      <td>Tanggal Lahir</td>
                                                      <td><?=$user[0]->tanggal_lahir?></td>
                                                </tr>
                                                <tr>
                                                      <td>Email</td>
                                                      <td><?=$user[0]->email?></td>
                                                </tr>
                                                <tr>
                                                      <td>No Telepon</td>
                                                      <td><?=$user[0]->no_telpon?></td>
                                                </tr>
                                                <tr>
                                                      <td>Alamat</td>
                                                      <td><?=$user[0]->alamat?></td>
                                                </tr>
                                          </table>
                                    </div>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
</div>
<script type="text/javascript">
      function hapus(id,status) {
            var konfirmasi = confirm('Anda yakin akan menghapus?');
            if (konfirmasi == true) {
                  window.location.href = '<?=base_url()?>user/blokir/'+id+'/'+status;
                  // window.location.href = '<?=base_url()?>user/blokir/'+id;
            }
      }
</script>
<?php $this->load->view('template/footer'); ?>