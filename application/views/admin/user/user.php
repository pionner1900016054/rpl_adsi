<?php $this->load->view('template/header');?>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header ">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <h4 class="card-title">Tabel Pengguna </h4>
                            </div>
                            <!-- <div class="col-sm-12 col-md-6"><a type="button" class="btn btn-primary btn-sm float-right" href="<?=base_url()?>user/tambah"><i class="fa fa-plus"></i> user Admin</a></div></div> -->
                        </div>
                        <div class="card-body">
                            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6"></div>
                                    <div class="col-sm-12 col-md-6"></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example2"
                                            class="example2 table table-bordered table-hover dataTable dtr-inline"
                                            role="grid" aria-describedby="example2_info">
                                            <thead>
                                                <tr role="row">
                                                    <th>No</th>
                                                    <th>Email</th>
                                                    <th>Nama</th>
                                                    <th>Role</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no = 1;foreach ($user as $key) {?>
                                                <tr>
                                                    <td><?=$no;?></td>
                                                    <td><?=$key->email;?></td>
                                                    <td><?php if ($key->role == 2) {
    echo $key->Admin;
} elseif ($key->role == 3) {
    echo $key->member;
}
    ?>
                                                    </td>
                                                    <td>
                                                        <?php
if ($key->role == 2) {
        echo "Admin";
    } elseif ($key->role == 3) {
        echo "Member";
        // echo $key->member;
    }?>
                                                    </td>
                                                    <td>
                                                        <?php
if ($key->status == 1) {
        echo "Aktif";
    } elseif ($key->status == 2) {
        echo "Non-Aktif";
    } elseif ($key->status == 3) {
        echo "Banned";
    }
    ?>
                                                    </td>
                                                    <td>
                                                        <!-- <a type="button" class="btn btn-secondary btn-sm" href="<?=base_url()?>user/detail/<?=$key->id_user?>"><i class="fa fa-eye"></i></a> -->
                                                        <a type="button" class="btn btn-secondary btn-sm text-light"
                                                            href="<?=base_url()?>user/detail/<?=$key->id_user?>"><i
                                                                class="fa fa-eye"></i></a>
                                                        <?php
if ($key->status == 1) {?>
                                                        <a type="button" class="btn btn-danger btn-sm text-light"
                                                            onclick="hapus(<?=$key->id_user?>,3)"><i
                                                                class="fa fa-lock"></i> blokir</a>
                                                        <?php } elseif ($key->status == 3) {?>
                                                        <a type="button" class="btn btn-success btn-sm text-light"
                                                            onclick="hapus(<?=$key->id_user?>,1)"><i
                                                                class="fa fa-unlock"></i> buka blokir</a>
                                                        <?php }
    ?>

                                                    </td>
                                                </tr>
                                                <?php $no++;}?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    function hapus(id, status) {
        var konfirmasi = confirm('Anda yakin akan menghapus?');
        if (konfirmasi == true) {
            window.location.href = '<?=base_url()?>user/blokir/' + id + '/' + status;
        }
    }
    </script>
    <?php $this->load->view('template/footer');?>