
<!DOCTYPE html>
<html lang="en">
<head>
<title>Login V1</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- <link rel="icon" type="image/png" href="images/icons/favicon.ico" /> -->


<link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/login/animate.css">

<link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/login/hamburger.min.css">

<link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/login/select2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


<link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/bootstrap/css/bootstrap.css">

<link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/login/style_login1.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/login/style_login2.css">

</head>
<body>
	<style type="text/css">
		.button-custom{
  background-color: #BAD369 !important;
  color: white !important;
}
	</style>
<div class="limiter">
	<div class="container-login100" style="background-image: url('<?=base_url()?>asset/IMG_9476.jpg');background-repeat: no-repeat;background-size: cover;">
	<div class="container">
			<div class="row bg-white rounded" style="padding-top: 5%;padding-bottom: 5%;padding-right: 5%">
			<div class="col-sm-6 text-center">
					<img src="<?=$this->session->logo_app?>" alt="IMG" style="max-height: 400px">
			</div>
			<div class="col-sm-6">
				<span class="login100-form-title">
					LUPA KATA SANDI
				</span>
				<?=$this->session->flashdata('error_log');?>
				<?=form_open('login/forgot/');?>
        <div class="input-group mb-3">
          <input type="email" name="email" class="form-control" placeholder="Masukkan Email Anda" required>
        </div>
        <div class="input-group mb-3">
          <input type="text" name="no_telpon" class="form-control" placeholder="Masukkan Nomor Telepon" required>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <button type="submit" class="btn button-custom btn-block">Kirim Permintaan Kata Sandi Baru</button>
          </div>
        </div>
      <?=form_close();?>
				<!--<div class="wrap-input100" >-->
				<!--	<div class="text-right" >-->
				<!--		<a href="<?=base_url()?>login/lupa_password/">Lupa Kata Sandi?</a>-->
				<!--	</div>-->
				<!--</div>-->
				<!--<p class="text-center">-->
				<!--	Belum Punya Akun? <a href="<?=base_url()?>login/register" class="txt2">Daftar Disini</a>-->
				<!--</p>-->
			</div>
</div>

</div></div>
</div>
</div>
<script src="<?=base_url()?>asset/AdminLTE/plugins/jquery/jquery.min.js"></script>

<!-- <script src="vendor/jquery/jquery-3.2.1.min.js"></script> -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="<?=base_url()?>asset/login/select2.min.js"></script>

<script src="<?=base_url()?>asset/login/tilt.jquery.min.js"></script>
<script>
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

<script src="<?=base_url()?>asset/login/main.js"></script>
</body>
</html>
