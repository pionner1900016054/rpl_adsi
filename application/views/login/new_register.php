
<!DOCTYPE html>
<html lang="en">
<head>
<title>Login V1</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- <link rel="icon" type="image/png" href="images/icons/favicon.ico" /> -->


<link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/login/animate.css">

<link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/login/hamburger.min.css">

<link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/login/select2.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/bootstrap/css/bootstrap.css">

<!-- <link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/login/style_login1.css"> -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/login/style_login2.css">

</head>
<body>

	<style type="text/css">
		.button-custom{
  background-color: #BAD369 !important;
  color: white !important;
}
	</style>
<div class="limiter">
	<div class="container-login100" style="background-image: url('<?=base_url()?>asset/IMG_9476.jpg');background-repeat: no-repeat;background-size: cover;">
	<div class="container">
			<div class="row bg-white rounded" style="padding-top: 5%;padding-bottom: 5%">
			<div class="col-sm-6 text-center">
					<img src="<?=$this->session->logo_app?>" alt="IMG" style="max-height: 400px">
			</div>
			<div class="col-sm-6">
				<span class="login100-form-title">
					DAFTAR
				</span>
				<?=form_open('login/register_proses');?>
    <!-- <form action="../../index.html" method="post"> -->
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Masukkan Nama Lengkap" name="Nama" value="<?=set_value('Nama');?>">
        <span class="glyphicon glyphicon-user form-control-feedback text-danger"><?php echo form_error('Nama'); ?></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Masukkan Email" name="email" value="<?=set_value('email');?>">
        <span class="glyphicon glyphicon-envelope form-control-feedback text-danger"><?php echo form_error('email'); echo $this->session->flashdata('error_email_used'); ?></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Masukkan Kata Sandi" name="password" value="<?=set_value('password');?>" id="pass">
        <span class="glyphicon glyphicon-lock form-control-feedback text-danger"><?php echo form_error('password'); ?></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Masukkan Ulang Kata Sandi" name="password_again" id="id_password_again">
        <div id="CheckPasswordMatch"></div>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" onkeypress="return onlyNumberKey(event)" placeholder="Masukkan No Telpon" name="notelpon" value="<?=set_value('notelpon');?>">
        <span class="glyphicon glyphicon-lock form-control-feedback text-danger"><?php echo form_error('notelpon'); ?></span>
      </div>
      <!-- <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Retype password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div> -->
      <div class="row">
        <div class="col-sm-12">
          <button type="submit" disabled class="btn button-custom btn-block" id="btn_submit">Register</button>
        </div>
        <!-- /.col -->
      </div>
      <?=form_close();?>
				<p class="text-center">
					Sudah Punya Akun? <a href="<?=base_url()?>login/" >Masuk Disini</a>
				</p>
			</div>
</div>

</div></div>
</div>
</div>
<script src="<?=base_url()?>asset/AdminLTE/plugins/jquery/jquery.min.js"></script>

<!-- <script src="vendor/jquery/jquery-3.2.1.min.js"></script> -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="<?=base_url()?>asset/login/select2.min.js"></script>

<script src="<?=base_url()?>asset/login/tilt.jquery.min.js"></script>
<script>
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
  function onlyNumberKey(evt) { 
          
        // Only ASCII charactar in that range allowed 
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode 
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57)) 
            return false; 
        return true; 
    } 
  function checkPasswordMatch() {
    var password = $("#pass").val();
    var confirmPassword = $("#id_password_again").val();
    if (password != confirmPassword){
      $("#CheckPasswordMatch").html("<font class='text-danger'>Passwords does not match!<font>");
    }else{
      $("#CheckPasswordMatch").html("<font class='text-success'>Passwords match.<font>");
      $("#btn_submit").prop("disabled", false);
    }
  }
  $(document).ready(function() {
    $("#id_password_again").keyup(checkPasswordMatch);
  })
</script>

<script src="<?=base_url()?>asset/login/main.js"></script>
</body>
</html>
