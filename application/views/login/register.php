<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>asset/AdminLTE/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?=base_url()?>asset/AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>asset/AdminLTE/dist/css/Adminlte.min.css">
  <!-- iCheck -->
  <!-- <link rel="stylesheet" href="<?=base_url()?>asset/AdminLTE/plugins/iCheck/square/blue.css"> -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <img src="<?=$this->session->logo_app?>" height="90">
  </div>

  <div class="card">
    <div class="card-body login-card-body">
    <p class="login-box-msg">Daftar Akun Baru</p>
      <?=form_open('login/register_proses');?>
    <!-- <form action="../../index.html" method="post"> -->
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Masukkan Nama Lengkap" name="Nama" value="<?=set_value('Nama');?>">
        <span class="glyphicon glyphicon-user form-control-feedback text-danger"><?php echo form_error('Nama'); ?></span>
      </div>
      <div class="form-group has-feedback">
        <input type="date" class="form-control" placeholder="Masukkan Tanggal Lahir" name="tanggallahir" value="<?=set_value('tanggallahir');?>">
        <span class="glyphicon glyphicon-envelope form-control-feedback text-danger"><?php echo form_error('tanggallahir');?></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Masukkan Email" name="email" value="<?=set_value('email');?>">
        <span class="glyphicon glyphicon-envelope form-control-feedback text-danger"><?php echo form_error('email'); echo $this->session->flashdata('error_email_used'); ?></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Masukkan Password" name="password" value="<?=set_value('password');?>">
        <span class="glyphicon glyphicon-lock form-control-feedback text-danger"><?php echo form_error('password'); ?></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" onkeypress="return onlyNumberKey(event)" placeholder="Masukkan No Telpon" name="notelpon" value="<?=set_value('notelpon');?>">
        <span class="glyphicon glyphicon-lock form-control-feedback text-danger"><?php echo form_error('notelpon'); ?></span>
      </div>
      <div class="form-group has-feedback">
        <textarea name="alamat" class="form-control" placeholder="Masukkan Alamat"><?=set_value('alamat');?></textarea>
        <!-- <input type="password" class="form-control" placeholder="Password" name="password"> -->
        <span class="glyphicon glyphicon-lock form-control-feedback text-danger"><?php echo form_error('alamat'); ?></span>
      </div>
      <div class="form-group has-feedback">
        <select class="form-control" name="provinsi" id="provinsi" placeholder="Pilih Provinsi" value="<?=set_value('provinsi');?>">
          <!-- <option> -- Piih Provinsi -- </option> -->
        </select>
        <span class="glyphicon glyphicon-lock form-control-feedback text-danger"><?php echo form_error('provinsi'); ?></span>
      </div>
      <div class="form-group has-feedback">
        <select class="form-control" name="kabupaten" id="kabupaten" placeholder="Pilih Kabupaten" value="<?=set_value('kabupaten');?>">
          <!-- <option> -- Piih Kabupaten -- </option> -->
        </select>
        <span class="glyphicon glyphicon-lock form-control-feedback text-danger"><?php echo form_error('kabupaten'); ?></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" onkeypress="return onlyNumberKey(event)" placeholder="Masukkan Kode Pos" name="kodepos" value="<?=set_value('kodepos');?>">
        <span class="glyphicon glyphicon-lock form-control-feedback text-danger"><?php echo form_error('kodepos'); ?></span>
      </div>
      <!-- <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Retype password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div> -->
      <div class="row">
        <div class="col-8">
          <div class="checkbox icheck">
            <label>
              <!-- <input type="checkbox"> I agree to the <a href="#">terms</a> -->
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-4">
          <button type="submit" class="btn btn-primary btn-block">Register</button>
        </div>
        <!-- /.col -->
      </div>
      <?=form_close();?>
    <!-- </form> -->

    <!-- <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
        Google+</a>
    </div> -->
    <br>
    <center>
      Sudah Punya Akun? <a href="<?=base_url();?>login" class="text-center">Login Disini</a>
    </center>
  </div>
</div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="<?=base_url()?>asset/AdminLTE//plugins/jquery/jquery.min.js"></script>
<!-- <script src="../../bower_components/jquery/dist/jquery.min.js"></script> -->
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>asset/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script> -->
<!-- iCheck -->
<!-- <script src="../../plugins/iCheck/icheck.min.js"></script> -->
<!-- <script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script> -->
<script type="text/javascript">
  function onlyNumberKey(evt) { 
          
        // Only ASCII charactar in that range allowed 
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode 
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57)) 
            return false; 
        return true; 
    } 
  function ajax_prov() {
    $.ajax({
        type  : 'ajax',
        url   : '<?php echo base_url();?>login/select_prov/',
        async : false,
        dataType : 'json',
        success : function(data){
          var html = '<option value="">- Pilih Provinsi -</option>';
          var i;
          for(i=0; i<data.length; i++){
            html += '<option value="'+data[i].id_prov+'">'+data[i].Nama_prov+'</option>';
          }
            $('#provinsi').html(html);
          }
        // $('#provinsi').html(url);
        // return false;
    });
  }
  function ajax_kab() {
    var id_prov = $('#provinsi').val();
    console.log(id_prov);
    $.ajax({
        type  : 'ajax',
        url   : '<?php echo base_url();?>login/select_kab/' + id_prov,
        async : false,
        dataType : 'json',
        success : function(data){
          var html = '<option value="">- Pilih Kabupaten/Kota -</option>';
          var i;
          for(i=0; i<data.length; i++){
            html += '<option value="'+data[i].id_kab+'">'+data[i].Nama_kab+'</option>';
          }
            $('#kabupaten').html(html);
          }
        // $('#provinsi').html(url);
        // return false;
    });
  }
  $(document).ready(() => {
    ajax_prov();

    $('#provinsi').change(function() {
          ajax_kab();
    });
  });
</script>
</body>
</html>
