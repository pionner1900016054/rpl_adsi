<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/ico" href="<?=$this->session->logo_app?>"> 

  <title>Kampung Sinau Ticket</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<style type="text/css">
  p{
    font-size: 8pt
  }
</style>
<body>
  <div class="container">
  <table>
    <tr>
      <td width="600" style="background-color: black;color: #BAD369;">
      <p style="font-size: 12pt"> Tiket Masuk</p>
      <p> <?=$etiket[0]->produk?></p>
      <p style="font-size: 20pt;font-weight: bold;"> DESA WISATA KAMPUNG SINAU</p>
      <p> DESA BEDOHO, KECAMATAN JIWAN, KABUPATEN MADIUN</p>
      <p> Untuk 1 Orang</p>
      <p><b> Tanggal Booking/Berlaku pada</b> <?=date('d M Y',strtotime($etiket[0]->booking))?></p>
      <br>
      <br>
      </td>
      <td style="background-color: #BAD369;width: 140px;text-align: center;">
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <p><b>Kode Unik Tiket Anda</b></p><p><?=$etiket[0]->kodetiket?></p>
      </td>
    </tr>
  </table>
  </div>
<footer>
<p style="font-size:8px;text-align: center;">PDF Created at <?=date('d M Y H:i:s')?></p> 
<p style="text-align: center;">Copyright &copy; <?=date('Y')?> <?=$this->session->Nama_app;?>.</p>

</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>