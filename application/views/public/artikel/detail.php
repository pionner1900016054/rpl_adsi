<?php $this->load->view('tmp_pub/new_header');?>
<style type="text/css">
	.pad5{
		margin-right: 5px;
		margin-left: 0px;
		padding: 0px;
		border-radius: 0px;
	}
	.mari5{
		margin-left: 5px;
		margin-right: 0px;
		padding: 0px;
		border-radius: 0px;
	}
	.hrevent{
		width: 90%;
	}
	.hrcek{
		height: 5px;width: 30%;margin: auto;background-color: orange;border:0px;border-radius: 5px;
	}
	.col-md-3{
	  display: inline-block;
	  margin-left:-4px;
	}
	.col-md-3 img{
	  width:100%;
	}
	body .carousel-indicators li{
	  background-color:white;
	  border:1px solid black;
	  box-shadow: 5px
	}
	body .carousel-indicators{
	  bottom: 0;
	}
	body .carousel-control-prev-icon,
	body .carousel-control-next-icon{
	  background-color:black;
	  box-shadow: 5px
	}
	body .no-padding{
	  padding-left: 0;
	  padding-right: 0;
	   }
	#demo{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.bgroundgray{
		/*background-color: #f4f4f4;*/
		border:0px;
	}
</style>
<div class="container-fluid" style="padding-top: 240px;background-image: url('<?=base_url()?>asset/header_web/IMG_0613.jpg');background-repeat: no-repeat;background-position: bottom;background-size: cover;height: 500px">
		<center><h1 style="color: white" class="font-weight-bold text-center text-uppercase">Berita</h1></center>
</div>
<div class="container-fluid">
    	<h3 class="font-weight-bold text-center text-uppercase" style="margin-top: 40px;margin-bottom: 0px;color: #BAD369"><?=$artikel[0]->judul?></h3>
	<h5 class="text-center text-muted text-uppercase" style="font-family: 'Noto Sans', Sans-serif;padding: 20px;color: white">DAPATKAN DETAIL INFORMASI</h5>
	<hr style="width: 300px;max-height: 10px;border: 3px solid #BAD369;background-color: #BAD369">
</div>
<div class="container-fluid" style="padding-left: 10%;padding-right: 10%;margin-top: 30px">
    <!-- <div class="card shadow p-3 mb-5 bg-white rounded"> -->
    <div class="card" style="padding: 40px;border: 0px">
    <?php foreach ($artikel as $key) {?>
      <div class="col-sm" style="padding: 1px !important;">
    	<p class="text-muted" style="font-size: 10pt !important"><i class="fa fa-clock-o"></i> &nbsp<?=date('d M Y',strtotime($key->created_at));?> &nbsp<i class="fa fa-user"></i> &nbsp<?=$key->penulis;?></p>
<?php if($key->kategori != "Berita"){?>
        <img class="img-thumbnail rounded mx-auto d-block" src="<?=base_url()?>picture/<?=$key->gambar?>" alt="" style="max-width:30%;">
<?php }elseif($key->kategori == "Berita"){?>
        <img class="img-thumbnail rounded mx-auto d-block" src="<?=base_url()?>picture/<?=$key->gambar?>" alt="" style="max-width:50%;"> 
<?php }?>
	          		<p class="text-justify"><?=$key->isi?></p>
	          		<br>
	          		<p class="text-muted"><i class="fa fa-tags"></i> &nbsp<?=$key->kategori?></p>
          </div>
      <!-- </div> -->
    <?php }?>
        </div>
  </div>
<?php $this->load->view('tmp_pub/footer');?>