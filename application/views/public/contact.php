<?php $this->load->view('tmp_pub/new_header');?>
<style type="text/css">
	.pad5{
		margin-right: 5px;
		margin-left: 0px;
		padding: 0px;
		border-radius: 0px;
	}
	.mari5{
		margin-left: 5px;
		margin-right: 0px;
		padding: 0px;
		border-radius: 0px;
	}
	.hrevent{
		width: 90%;
	}
	.hrcek{
		height: 5px;width: 30%;margin: auto;background-color: orange;border:0px;border-radius: 5px;
	}
	.col-md-3{
	  display: inline-block;
	  margin-left:-4px;
	}
	.col-md-3 img{
	  width:100%;
	}
	body .carousel-indicators li{
	  background-color:white;
	  border:1px solid black;
	  box-shadow: 5px
	}
	body .carousel-indicators{
	  bottom: 0;
	}
	body .carousel-control-prev-icon,
	body .carousel-control-next-icon{
	  background-color:black;
	  box-shadow: 5px
	}
	body .no-padding{
	  padding-left: 0;
	  padding-right: 0;
	   }
	#demo{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.bgroundgray{
		/*background-color: #f4f4f4;*/
		border:0px;
	}
</style>
<div class="container-fluid" style="background-color: #BAD369;height: 250px">
		<center><h1 style="padding-top: 100px;color: white" class="font-weight-bold text-center text-uppercase">Kontak Kami</h1></center>
</div>
<div class="container-fluid">
    	<h2 class="text-center text-uppercase" style="margin-top: 40px;margin-bottom: 0px">Dimana anda dapat menghubungi kami</h2>
	<h5 class="text-center text-muted text-uppercase" style="font-family: 'Noto Sans', Sans-serif;padding: 20px;color: white">Hubungi kami 24 Jam</h5>
</div>
<div class="container">
    <div class="row" >
		<div class="col-md-3 col-sm-4" ></div>
		<div class="col-md-3 col-sm-4" >
		<div class="card wrimagecard wrimagecard-topimage" style="border: 0px !important">
	          <div class="card-title" style="padding-top: 40px;">
	            <center><i class="fa fa-phone" style="color:#BAD369 !important;font-size: 50pt"></i></center>
	          </div>
	          <div class="card-body text-center">
	          	<h4 class="font-weight-bold">No Telepon</h4>
	            <p class="text-dark text-muted"><?=$contact[0]->kontak_satu;?></p>
	            <p class="text-dark text-muted"><?=$contact[0]->kontak_dua;?></p>
	            <p class="text-dark text-muted"><?=$contact[0]->kontak_tiga;?></p>
	          </div>
	      </div>
	    </div>
	    <div class="col-md-3 col-sm-4">
		<div class="card wrimagecard wrimagecard-topimage" style="border: 0px !important">
	          <div class="card-title" style="padding-top: 40px">
	            <center><i class="fa fa-envelope" style="color:#BAD369 !important;font-size: 50pt"></i></center>
	          </div>
	          <div class="card-body text-center">
	          	<h4 class="font-weight-bold">No Telepon</h4>
	            <p class="text-dark text-muted"><?=$contact[0]->email;?></p>
	          </div>
	      </div>
	    </div>
		<div class="col-md-3 col-sm-4" ></div>
	</div>
	<div class="embed-responsive embed-responsive-16by9">
        <?=$contact[0]->embed_maps;?>
	</div>
</div>
<br>
<div class="container">
    <div class="row ">
    <?php foreach ($artikel as $key) {?>
        <div class="card shadow p-3 mb-5 bg-white rounded" style="border: 0px !important">
          <div class="card-title">
		    	<h4><?=$key->judul?></h4>
		    	<p class="text-muted" style="font-size: 10pt !important"><i class="fa fa-clock-o"></i>&nbsp<?=$key->created_at;?> &nbsp<i class="fa fa-tags"></i> &nbsp<?=$key->kategori?></p>
          </div>
          <div class="card-body">
          	<div class="row">
          		<div class="col-4" style=""><img class="img-fluid" src="<?=base_url()?>picture/<?=$key->gambar?>" alt=""></div>
          		<div class="col-8" style=""><?=$key->isi?></div>
			          		<p class="text-muted" style="font-weight: bold;"><i class="fa fa-user"></i> &nbsp<?=$key->penulis;?></p>
          	</div>
      </div>
      </div>
    <?php }?>
    </div>

  </div>
<?php $this->load->view('tmp_pub/footer');?>