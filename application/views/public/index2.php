<?php $this->load->view('tmp_pub/new_header');?>
<?php
function rupiah($angka)
{

    $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
    return $hasil_rupiah;

}
?>
<style type="text/css">
h2 {
    text-shadow: 2px 2px 4px lightgray !important;
}

#slide_bg {
    padding-top: 180px;
    background-repeat: no-repeat;
    background-size: 100% 700px;
    background-position: top;
    backface-visibility: hidden;
    animation: slideBg 10s linear infinite 0s;
    animation-timing-function: ease-in-out;
    background-image: url('<?=base_url()?>asset/IMG_9476.jpg');
}

@keyframes slideBg {
    0% {
        background-image: url('<?=base_url()?>galeri/<?=$galeri[0]->gambar?>');
    }

    25% {
        background-image: url('<?=base_url()?>galeri/<?=$galeri[1]->gambar?>');
    }

    50% {
        background-image: url('<?=base_url()?>galeri/<?=$galeri[2]->gambar?>');
    }

    75% {
        background-image: url('<?=base_url()?>galeri/<?=$galeri[3]->gambar?>');
    }

    100% {
        background-image: url('<?=base_url()?>galeri/<?=$galeri[4]->gambar?>');
    }
}
</style>
<div class="container-fluid" id="slide_bg">
    <!-- <div style=""> -->
    <div class="container" style="height: 400px;display: table;">
        <div class="row" style="padding-left: 10%">
            <div style="padding-top: 15%;">
                <h1 style="color: white;font-weight: bold;text-shadow: 1px 1px #afcb50;">KAMPUNG SINAU<br>TOURISM
                    VILLAGE</h1>
                <h5 style="color: white;text-shadow: 1px 1px #afcb50;">Jelajahi Destinasi Indah di Seluruh Desa</h5>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm">
                <a href="<?=base_url()?>desa/tiket" style="text-decoration:none;color:black">
                    <div class="card" style="border-radius: 30px !important;box-shadow: 0px 4px #afcb50;height:270px">
                        <div style="padding-top: 30px;padding-left: 35%;padding-right: 35%">
                            <img src="<?=base_url()?>logo/travel-07.png" class="card-img-top" alt="...">
                        </div>
                        <div class="card-body text-center">
                            <h5 class="card-title text-center font-weight-bold">Tiket</h5>
                            <p class="card-text">Pesan Tiket Paket Wisatamu Di sini!</p>
                            <!-- <a href="<?=base_url()?>desa/tiket" class="btn btn-dark">Go somewhere</a> -->
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm">
                <a href="<?=base_url()?>desa/produk" style="text-decoration:none;color:black">
                    <div class="card" style="border-radius: 30px !important;box-shadow: 0px 4px #afcb50;height:270px">
                        <div style="padding-top: 30px;padding-left: 35%;padding-right: 35%">
                            <img src="<?=base_url()?>logo/Full-basket-SH.png" class="card-img-top" alt="...">
                        </div>
                        <div class="card-body text-center">
                            <h5 class="card-title text-center font-weight-bold">Produk</h5>
                            <p class="card-text">Dapatkan Informasi Produk UMKM Unggulan Di sini!</p>
                            <!-- <a href="<?=base_url()?>desa/tiket" class="btn btn-primary">Go somewhere</a> -->
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm">
                <a href="<?=base_url()?>desa/event" style="text-decoration:none;color:black">
                    <div class="card" style="border-radius: 30px !important;box-shadow: 0px 4px #afcb50;height:270px">
                        <div style="padding-top: 30px;padding-left: 35%;padding-right: 35%">
                            <img src="<?=base_url()?>logo/travel-08.png" class="card-img-top" alt="...">
                        </div>
                        <div class="card-body text-center">
                            <h5 class="card-title text-center font-weight-bold">Event</h5>
                            <p class="card-text">Lihat dan Ikuti Event Seru Desa Wisata Di sini!</p>
                            <!-- <a href="<?=base_url()?>desa/tiket" class="btn btn-primary">Go somewhere</a> -->
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm">
                <a href="<?=base_url()?>desa/artikel" style="text-decoration:none;color:black">
                    <div class="card" style="border-radius: 30px !important;box-shadow: 0px 4px #afcb50;height:270px">
                        <div style="padding-top: 30px;padding-left: 35%;padding-right: 35%">
                            <img src="<?=base_url()?>logo/icon-70-512.webp" class="card-img-top" alt="...">
                        </div>
                        <div class="card-body text-center">
                            <h5 class="card-title text-center font-weight-bold">Berita</h5>
                            <p class="card-text">Ketahui Berita Terkini Di sini!</p>
                            <!-- <a href="<?=base_url()?>desa/tiket" class="btn btn-primary">Go somewhere</a> -->
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <!-- </div> -->
    </div>
</div>
<div class="container-fluid">
    <h2 class="text-center font-weight-bold text-uppercase" style="color: #BAD369;margin-top: 40px"><a
            style="color: #BAD369;text-decoration: none;" href="<?=base_url()?>desa/galeri/">Galeri</a></h2>
    <div class="row" style="padding-left: 10%;padding-right: 10%;margin-top: 30px">
        <?php $no = 1;foreach ($galeri as $key) {?>
        <div class="col-sm-4" style="padding: 10px">
            <div class="card" style="border: 0px">
                <div>

                    <img src="<?=base_url()?>galeri/<?=$key->gambar?>" class="img-fluid" alt="..."
                        style="border-radius: 20px; width: 500px;height: 250px;">
                </div>
            </div>
        </div>
        <?php if ($no == 3): ?>
        <div class="w-100"></div>
        <?php endif?>
        <?php $no++;}?>
    </div>
    <p class="text-center" style="margin-top: 30px"><a class="btn button-custom-1" style="color: #BAD369;"
            href="<?=base_url()?>desa/galeri/">Lihat Lebih Banyak Galeri</a></p>
    <br>
    <br>
</div>
<div class="container-fluid shadow"
    style="background:linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('<?=base_url()?>asset/farm-1737182_1280-1024x678.jpg');background-repeat: no-repeat;background-size: cover;background-position: top;">
    <h1 class="text-center font-weight-bold text-uppercase"
        style="padding-top: 70px;color: white;margin-bottom: 0px;color: #afcb50;text-shadow: 2px 2px 2px 2px white">
        Pesan Tiket</h1>
    <h5 class="text-center" style="padding-top: 10px;padding-bottom: 10px;color: white">
        Pesan Tiket Paket Wisatamu Di sini!
    </h5>





    <?php foreach ($tiket as $key) {?>
    <div class="row" style="padding-left: 10%;padding-right: 10%;padding-bottom: 30px">
        <div class="card col-lg shadow" style="margin-bottom: 20px;border-radius: 20px">
            <div class="row">
                <div class="col-sm-3" style="padding-left: 0px;padding-right: 0px">
                    <img style="max-width: 100%;max-height: 200px;max-width: 100%;border-radius: 20px 0 0 20px"
                        id="img_tiket" src="<?=base_url()?>picture/<?=$key->gambar?>" class="img" alt="">
                </div>
                <div class="col-sm-6 d-flex align-content-center flex-wrap">
                    <div class="p-4 bd-highlight">
                        <h3 class="font-weight-bold"><?=$key->produk?>
                            <?=$key->produk == "Paket EduWisata Souvenir" ? "<a class='badge badge-info'>Mantap</a>" : ""?>
                        </h3>

                        <?php if ($key->produk == "Paket Pilihan"): ?>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Hallo sobat!</strong> Paket ini bisa milih kegiatan yang kamu ingin, cukup checklist
                            kegiatan apa saja yang ingin kamu ikuti.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php endif?>

                        <?php if ($key->produk == "Paket Pilihan"): ?>
                        <?php foreach ($kegiatan as $a): ?>

                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chk" id="<?=$a->id_kegiatan?>"
                                value="<?=$a->harga_kegiatan?>">
                            <label style="color: gray;" class="custom-control-label"
                                for="<?=$a->id_kegiatan?>"><?=$a->Nama_kegiatan?> (<a href="" class="detail-kegiatan"
                                    data-toggle="modal" data-target="#exampleModal" data-Nama="<?=$a->Nama_kegiatan?>"
                                    data-durasi="<?=$a->durasi_kegiatan?>" data-harga="<?=$a->harga_kegiatan?>"
                                    data-deskripsi="<?=$a->deskripsi_kegiatan?>" id="detail-kegiatan">Detail
                                    kegiatan</a>)
                            </label>
                        </div>


                        <?php endforeach?>

                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chk" id="voucher" value="20000">
                            <label style="color: gray;" class="custom-control-label" for="voucher">Souvenir produk UMKM
                                menarikk
                            </label>
                        </div>

                        <?php else: ?>
                        <?php $acara = $this->M_template->get_kegiatan_tiket($key->id_produk)?>
                        <?php foreach ($acara as $a): ?>
                        <p style="color: gray;"><?=$a->Nama_kegiatan?> (<a href="" class="detail-kegiatan"
                                data-toggle="modal" data-target="#exampleModal" data-Nama="<?=$a->Nama_kegiatan?>"
                                data-durasi="<?=$a->durasi_kegiatan?>" data-harga="<?=$a->harga_kegiatan?>"
                                data-deskripsi="<?=$a->deskripsi_kegiatan?>" id="detail-kegiatan">Detail
                                kegiatan</a>)
                        </p>

                        <?php endforeach;?>
                        <?php endif;?>

                        <font style="color: gray"><?=$key->keterangan?> </font>

                    </div>

                    <!-- <a href="<?=base_url()?>desa/tiket" class="btn btn-dark">Go somewhere</a> -->
                </div>
                <div class="col-sm-2 d-flex align-content-center flex-wrap" style="border-left: 1px solid lightgray">
                    <div class="p-4 bd-highlight text-center" style="padding-left: 8%">
                        <?php if ($key->produk == "Paket Pilihan"): ?>
                        <h5 class="font-weight-bold" id="harga-pilihan"><i></i>Rp 0</h5>
                        <?php else: ?>
                        <h5 class="font-weight-bold"><i></i><?=rupiah($key->harga)?></h5>

                        <?php endif?>

                        <?php if ($key->produk == "Paket Pilihan"): ?>

                        <a class="btn btn-block" style="color: white;background-color: #afcb50; display: none;"
                            data-produk="<?=$key->id_produk?>" id="paketpilihan">Pesan Tiket</a>

                        <?php else: ?>
                        <a href="<?=base_url()?>transaksi/addCart/<?=$key->id_produk . '/' . $key->time;?>"
                            class="btn btn-block" style="color: white;background-color: #afcb50">Pesan Tiket </a>
                        <?php endif?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php }?>

    <?php foreach ($tiket_pilihan as $key) {?>
    <div class="row" style="padding-left: 10%;padding-right: 10%;padding-bottom: 30px">
        <div class="card col-lg shadow" style="margin-bottom: 20px;border-radius: 20px">
            <div class="row">
                <div class="col-sm-3" style="padding-left: 0px;padding-right: 0px">
                    <img style="max-width: 100%;max-height: 200px;max-width: 100%;border-radius: 20px 0 0 20px"
                        id="img_tiket" src="<?=base_url()?>picture/<?=$key->gambar?>" class="img" alt="">
                </div>
                <div class="col-sm-6 d-flex align-content-center flex-wrap">
                    <div class="p-4 bd-highlight">
                        <h3 class="font-weight-bold"><?=$key->produk?>
                            <?=$key->produk == "Paket EduWisata Souvenir" ? "<a class='badge badge-info'>Mantap</a>" : ""?>
                        </h3>

                        <?php if ($key->produk == "Paket Pilihan"): ?>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Hallo sobat!</strong> Paket ini bisa milih kegiatan yang kamu ingin, cukup checklist
                            kegiatan apa saja yang ingin kamu ikuti.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php endif?>

                        <?php if ($key->produk == "Paket Pilihan"): ?>
                        <?php foreach ($kegiatan as $a): ?>

                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chk" id="<?=$a->id_kegiatan?>"
                                value="<?=$a->harga_kegiatan?>" data-time="<?=$a->durasi_kegiatan?>">
                            <label style="color: gray;" class="custom-control-label"
                                for="<?=$a->id_kegiatan?>"><?=$a->Nama_kegiatan?> (<a href="" class="detail-kegiatan"
                                    data-toggle="modal" data-target="#exampleModal" data-Nama="<?=$a->Nama_kegiatan?>"
                                    data-durasi="<?=$a->durasi_kegiatan?>" data-harga="<?=$a->harga_kegiatan?>"
                                    data-deskripsi="<?=$a->deskripsi_kegiatan?>" id="detail-kegiatan">Detail
                                    kegiatan</a>)
                            </label>
                        </div>


                        <?php endforeach?>

                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chk" id="voucher" value="20000">
                            <label style="color: gray;" class="custom-control-label" for="voucher">Souvenir produk UMKM
                                menarikk
                            </label>
                        </div>

                        <?php else: ?>
                        <?php $acara = $this->M_template->get_kegiatan_tiket($key->id_produk)?>
                        <?php foreach ($acara as $a): ?>
                        <p style="color: gray;"><?=$a->Nama_kegiatan?> (<a href="" class="detail-kegiatan"
                                data-toggle="modal" data-target="#exampleModal" data-Nama="<?=$a->Nama_kegiatan?>"
                                data-durasi="<?=$a->durasi_kegiatan?>" data-harga="<?=$a->harga_kegiatan?>"
                                data-deskripsi="<?=$a->deskripsi_kegiatan?>" id="detail-kegiatan">Detail
                                kegiatan</a>)
                        </p>

                        <?php endforeach;?>
                        <?php endif;?>

                        <font style="color: gray"><?=$key->keterangan?> </font>

                    </div>

                    <!-- <a href="<?=base_url()?>desa/tiket" class="btn btn-dark">Go somewhere</a> -->
                </div>
                <div class="col-sm-2 d-flex align-content-center flex-wrap" style="border-left: 1px solid lightgray">
                    <div class="p-4 bd-highlight text-center" style="padding-left: 8%">
                        <?php if ($key->produk == "Paket Pilihan"): ?>
                        <h5 class="font-weight-bold" id="harga-pilihan"><i></i>Rp 0</h5>
                        <?php else: ?>
                        <h5 class="font-weight-bold"><i></i><?=rupiah($key->harga)?></h5>

                        <?php endif?>

                        <?php if ($key->produk == "Paket Pilihan"): ?>

                        <input type="hidden" id="waktu">
                        <a class="btn btn-block" style="color: white;background-color: #afcb50; display: none;"
                            data-produk="<?=$key->id_produk?>" id="paketpilihan">Pesan Tiket</a>

                        <?php else: ?>
                        <a href="<?=base_url()?>transaksi/addCart/<?=$key->id_produk;?>" class="btn btn-block"
                            style="color: white;background-color: #afcb50">Pesan Tiket </a>
                        <?php endif?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php }?>


</div>


<br>
<div class="container-fluid" style="margin-top: 0px">
    <h2 class="text-center font-weight-bold text-uppercase" style="color: #BAD369;margin-top: 40px"><a
            style="color: #BAD369;text-decoration: none;" href="<?=base_url()?>desa/produk/">Produk</a></h2>
    <h5 class="text-center text-muted" style="padding-top: 10px;padding-bottom: 10px;">Dapatkan Informasi Produk UMKM
        Unggulan Di sini!</h5>
    <div class="row" style="padding-left: 10%;padding-right: 10%;margin-top: 30px">
        <?php foreach ($produk as $key) {?>
        <div class="col-sm-6" style="margin-bottom: 15px;">
            <div class="card shadow">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm">
                            <img src="<?=base_url()?>picture/<?=$key->gambar?>" class="img-fluid" alt="..."
                                style="width: 250px;height: 250px;">
                        </div>
                        <div class="col-sm">
                            <p class="font-weight-bold" style="font-size: 16pt;margin-bottom: 10px"><a class="text-dark"
                                    href="<?=base_url()?>desa/produk_detail/<?=$key->id_produk?>"><?=$key->produk?></a>
                            </p>
                            <p style="font-size: 10pt;margin-bottom: 5px">Stok : <?php if ($key->stok > 1): ?>
                                tersedia
                                <?php endif?></p>
                            <font style="font-size: 10pt"><?=substr($key->keterangan, 0, 100)?> ... </font>
                            <p class="text-center font-weight-bold" style="font-size: 14pt;margin-bottom: 5px">
                                <?=rupiah($key->harga)?></p>
                            <!--<a href="<?=base_url()?>transaksi/addCart/<?=$key->id_produk;?>" class="btn btn-block button-custom">Tambahkan ke keranjang</a>-->
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-sm" style="padding-bottom:3px;">
                            <a href="<?=base_url()?>desa/produk_detail/<?=$key->id_produk;?>"
                                class="btn btn-block button-custom">Detail Produk</a>
                        </div>
                        <div class="col-sm">
                            <a href="<?=base_url()?>transaksi/addCart/<?=$key->id_produk;?>"
                                class="btn btn-block button-custom">Tambah ke keranjang</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php }?>
    </div>
    <p class="text-center" style="margin-top: 30px"><a class="btn button-custom-1" style="color: #BAD369;"
            href="<?=base_url()?>desa/produk/">Lihat Lebih Banyak Produk UMKM</a></p>
    <br>
    <br>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Kegiatan</th>
                            <th scope="col">Harga Kegiatan</th>
                            <th scope="col">Durasi Kegiatan</th>
                            <th scope="col">Deskripsi Kegiatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td id="Nama"></td>
                            <td id="harga"></td>
                            <td id="durasi"></td>
                            <td id="deskripsi" style="width: 500px;text-align: justify;"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<script>
const formatRupiah = (money) => {
    return new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',
        minimumFractionDigits: 0
    }).format(money);
}
</script>

<script>
$('.detail-kegiatan').on('click', function() {

    var Nama_kegiatan = $(this).data('Nama');
    var durasi_kegiatan = $(this).data('durasi');
    var harga_kegiatan = $(this).data('harga');
    var deskripsi_kegiatan = $(this).data('deskripsi');

    $(".modal-title").html(Nama_kegiatan);
    $(".modal-body #Nama").html(Nama_kegiatan);
    $(".modal-body #harga").html(formatRupiah(harga_kegiatan));
    $(".modal-body #durasi").html(durasi_kegiatan);
    $(".modal-body #deskripsi").html(deskripsi_kegiatan);

    // alert(deskripsi_kegiatan)

})
</script>

<script>
$('.chk').on('click', function() {

    var val = [];
    var total = 0
    var total_waktu = 0;

    $(':checkbox:checked').each(function(i) {
        var waktu = $(this).data('time').replace(['Menit', 'Jam'], '');
        waktu = Number(waktu.replace(/[^0-9\.]+/g, ""));
        total = total + Number($(this).val());
        total_waktu = total_waktu + waktu;
    });

    if (total > 0) {
        $('#paketpilihan').show();
    } else {
        $('#paketpilihan').hide();
    }

    $('#harga-pilihan').html(formatRupiah(total) + ",00");
    $('#waktu').val(total_waktu);

    console.log(total_waktu)

})
</script>

<script>
$('#paketpilihan').on('click', function() {

    var val = [];
    var total = 0
    $(':checkbox:checked').each(function(i) {
        total = total + Number($(this).val());
    });

    var idproduk = $(this).data('produk');
    var waktu = $('#waktu').val();

    // alert(waktu)

    window.location.href = "<?=base_url()?>transaksi/addCartPaketPilihan/" + idproduk + "/" + total + "/" +
        waktu;

})
</script>






<?php $this->load->view('tmp_pub/footer');?>
