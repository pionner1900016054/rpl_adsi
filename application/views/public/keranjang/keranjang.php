<?php $this->load->view('tmp_pub/new_header');?>
<link rel="stylesheet" href="<?=base_url()?>asset/AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<style type="text/css">
.pad5 {
    margin-right: 5px;
    margin-left: 0px;
    padding: 0px;
    border-radius: 0px;
}

.mari5 {
    margin-left: 5px;
    margin-right: 0px;
    padding: 0px;
    border-radius: 0px;
}

.hrevent {
    width: 90%;
}

.hrcek {
    height: 5px;
    width: 30%;
    margin: auto;
    background-color: orange;
    border: 0px;
    border-radius: 5px;
}

.col-md-3 {
    display: inline-block;
    margin-left: -4px;
}

.col-md-3 img {
    width: 100%;
}

body .carousel-indicators li {
    background-color: white;
    border: 1px solid black;
    box-shadow: 5px
}

body .carousel-indicators {
    bottom: 0;
}

body .carousel-control-prev-icon,
body .carousel-control-next-icon {
    background-color: black;
    box-shadow: 5px
}

body .no-padding {
    padding-left: 0;
    padding-right: 0;
}

#demo {
    margin-top: 10px;
    margin-bottom: 10px;
}

.bgroundgray {
    /*background-color: #f4f4f4;*/
    border: 0px;
}
</style>
<div class="container-fluid" style="padding-top: 180px;height: 400px">
    <h3 class="font-weight-bold text-center text-uppercase" style="margin-top: 40px;margin-bottom: 0px;color: #BAD369">
        Keranjangmu</h3>
    <h5 class="text-center text-muted text-uppercase"
        style="font-family: 'Noto Sans', Sans-serif;padding: 20px;color: white">ayo selesaikan pembelianmu</h5>
    <hr style="width: 300px;max-height: 10px;border: 3px solid #BAD369;background-color: #BAD369">
</div>
<div class="container-fluid">
    <div class="row" style="padding-left: 10%;padding-right: 10%;margin-top: 30px;margin-bottom: 100px">
        <div class="col shadow p-3 mb-5 bg-white rounded" style="margin: 10px">
            <div class="card" style="border: 0px !important">
                <div class="card-header" style="background-color: white !important;">
                    <!-- <div class="col-sm-12 col-md-6"> -->
                    <h4 class="card-title">Daftar Keranjangmu</h4>
                    <!-- </div> -->
                </div>
                <div class="card-body">
                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12 col-md-6"></div>
                            <div class="col-sm-12 col-md-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2"
                                    class="example2 table table-bordered table-hover dataTable dtr-inline" role="grid"
                                    aria-describedby="example2_info">
                                    <thead>
                                        <tr role="row">
                                            <th></th>
                                            <th>Produk</th>
                                            <th>Kategori</th>
                                            <th>Harga</th>
                                            <th>Jumlah</th>
                                            <th>Satuan Barang</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="show_keranjang">
                                    </tbody>
                                </table>
                                <br>
                                <?=form_open('transaksi/checkout');?>
                                <input id="value_keranjang" type="hidden" name="transaksi">
                                <button type="submit" disabled id="btn-checkout"
                                    class="btn button-custom float-right">Bayar Keranjang</button>
                                <?=form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?=base_url()?>asset/AdminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>asset/AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>asset/AdminLTE/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=base_url()?>asset/AdminLTE/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script type="text/javascript">
const formatRupiah = (money) => {
    return new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',
        minimumFractionDigits: 0
    }).format(money);
}

function delete_keranjang(id) {
    var conf = confirm('Yakin Menghapus Produk?');
    if (conf == true) {
        window.location = "<?=base_url('transaksi/hapusKeranjang/')?>" + id;
    }
}

function kerangjangs() {
    $.ajax({
        type: 'ajax',
        url: '<?php echo base_url() ?>transaksi/getKeranjang/',
        async: false,
        dataType: 'json',
        success: function(data) {
            var html = '';
            var i;
            var no;
            for (i = 0; i < data.length; i++) {
                no = i + 1;
                var satuan = "";
                var berat = 0;
                if (data[i].kategori == "Tiket") {
                    berat = data[i].waktu;
                    satuan = "Menit";
                } else {
                    berat = data[i].jumlah * data[i].berat;
                    satuan = "KG";
                }

                if (data[i].produk == "Paket Pilihan") {
                    document.cookie = "harga_pilihan=" + formatRupiah(data[i].harga_satuan);
                }

                html += `<tr>
				          	<td><input type="checkbox" id="cb_keranjang" name="selected_keranjang[]" class="form-control" value="${data[i].id_keranjang}"></td>
				          	<td>${data[i].produk}</td>
				          	<td>${data[i].kategori}</td>
				          	<td>${formatRupiah(data[i].harga_satuan)}</td>
				          	<td>
				          	<a type="button" class="btn btn-outline-secondary btn-sm" onclick="modify_jumlah('min',${data[i].id_keranjang})">-</a>
				                 &nbsp${data[i].jumlah}&nbsp
				                 <a type="button" class="btn btn-outline-secondary btn-sm" onclick="modify_jumlah('plus',${data[i].id_keranjang})">+</a>
				          	</td>
				          	<td>${berat} ${satuan}</td>
				          	<td><a onclick="delete_keranjang(${data[i].id_keranjang})" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
				        </tr>`;
            }
            $('#show_keranjang').html(html);
        }

    });
}

function modify_jumlah(ket, id) {
    let csrf = '<?=$this->security->get_csrf_hash();?>';
    $.ajax({
        url: "<?php echo base_url() ?>transaksi/addJumlah/" + ket,
        type: "POST",
        dataType: "json",
        async: true,
        data: {
            csrf_webdesa_name: csrf,
            id: id
        },
        success: function(data) {
            if (data.status == 'success') {
                location.reload();
            }
        }
    });
}

function insert_value_hidden() {
    var checkedValue = '';
    $('input[type=checkbox]:checked').each(function() {
        checkedValue += ($(this).val()) + ',';
    });
    $('#value_keranjang').val(checkedValue);
    // console.log(checkedValue);
}
$(document).ready(function() {
    kerangjangs();
    $('.example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
    $('input[type=checkbox]').change(function() {
        $("#btn-checkout").removeAttr('disabled');
        insert_value_hidden();
    });
});
</script>
<?php $this->load->view('tmp_pub/footer');?>