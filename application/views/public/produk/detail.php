<?php $this->load->view('tmp_pub/new_header');?>
<style type="text/css">
	.pad5{
		margin-right: 5px;
		margin-left: 0px;
		padding: 0px;
		border-radius: 0px;
	}
	.mari5{
		margin-left: 5px;
		margin-right: 0px;
		padding: 0px;
		border-radius: 0px;
	}
	.hrevent{
		width: 90%;
	}
	.hrcek{
		height: 5px;width: 30%;margin: auto;background-color: orange;border:0px;border-radius: 5px;
	}
	.col-md-3{
	  display: inline-block;
	  margin-left:-4px;
	}
	.col-md-3 img{
	  width:100%;
	}
	body .carousel-indicators li{
	  background-color:white;
	  border:1px solid black;
	  box-shadow: 5px
	}
	body .carousel-indicators{
	  bottom: 0;
	}
	body .carousel-control-prev-icon,
	body .carousel-control-next-icon{
	  background-color:black;
	  box-shadow: 5px
	}
	body .no-padding{
	  padding-left: 0;
	  padding-right: 0;
	   }
	#demo{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.bgroundgray{
		/*background-color: #f4f4f4;*/
		border:0px;
	}
</style>
<div class="container-fluid" style="padding-top: 240px;background-image: url('<?=base_url()?>asset/header_web/IMG_0617.jpg');background-repeat: no-repeat;background-position: top;background-size: cover;height: 500px">
		<center><h1 style="color: white" class="font-weight-bold text-center text-uppercase">Produk</h1></center>
</div>
<div class="container-fluid">
    	<h3 class="font-weight-bold text-center text-uppercase" style="margin-top: 40px;margin-bottom: 0px;color: #BAD369">Detail Produk</h3>
	<h5 class="text-center text-muted text-uppercase" style="font-family: 'Noto Sans', Sans-serif;padding: 20px;color: white">dapatkan sebelum kehabisan</h5>
	<hr style="width: 300px;max-height: 10px;border: 3px solid #BAD369;background-color: #BAD369">
</div>
<div class="container-fluid">
    <div class="row" style="padding-left: 10%;padding-right: 10%;margin-top: 30px">
    <!-- <?php foreach ($produk as $key) {?> -->
      <div class="col" style="margin: 10px">
      			<div class="card" style="border: 0px !important">
      				<div class="card-header" style="background-color: white !important;">
      					<!-- <div class="col-sm-12 col-md-6"> -->
      						<h4 class="text-center"><?=$produk[0]->produk;?></h4>
      					<!-- </div> -->
      				</div>
      				<div class="card-body">
      					<div class="row">
						  <!-- <div class="form-group row"> -->
						  	<div class="col-6">
						  		<!-- <center> -->
							  		<img src="<?=base_url()?>picture/<?=$produk[0]->gambar;?>" width="auto" style="max-height: 350px;width: auto;"  alt="" class="img-fluid img-thumbnail">
						  		<!-- </center> -->
						  	</div>
						  	<div class="col">
						  		<br>
							  <div class="form-group row">
							    <div class="col-sm-4 col-form-div" style="font-size: 14pt;font-weight: bold;">Kategori</div>
							    <div class="col-sm-6">
							    	<?=$produk[0]->kategori?>
							    </div>
							  </div>
							  <div class="form-group row">
							    <div  class="col-sm-4 col-form-div" style="font-size: 14pt;font-weight: bold;">Harga</div>
							    <div class="col-sm-6">
							    	<?=$produk[0]->harga;?>
							    </div>
							  </div>
							  <div class="form-group row">
							    <div class="col-sm-4 col-form-div" style="font-size: 14pt;font-weight: bold;">Berat</div>
							    <div class="col-sm-6">
							    	<?=$produk[0]->berat;?> KG
							    </div>
							  </div>
						  		<div class="form-group row">
							    <div  class="col-sm-4 col-form-div" style="font-size: 14pt;font-weight: bold;">Stok</div>
							    <div class="col-sm-6">
							    	<?=$produk[0]->stok;?>
							    </div>
							  </div>
							  <div class="form-group row">
							    <div class="col-sm-4 col-form-div" style="font-size: 14pt;font-weight: bold;">Keterangan</div>
							    <div class="col-sm-6">
							    		<?=$produk[0]->keterangan;?>
							  </div>
						  	</div>
						  	
						  </div>
      				<!-- </div> -->
      			</div>
      			<br>
      			<div class="row">
      				<div class="col-2"></div>
						  		<div class="col-sm-4 col-form-div" style="font-size: 14pt;font-weight: bold;"></div>
							    <div class="col-sm-6">
						  		<p  style="font-size: 14pt;font-weight: bold;">Cek Ongkir</p>
							    	<div class="container p-5">

									  <div class="input-group">
									   <div class="input-group-append">
									      <span class="input-group-text">Berat</span>
									    </div>
									    <?php if ($produk[0]->berat > 1) { ?>
									    	<input type="number" min="1" class="form-control" id="berat" name="berat" value="<?=$produk[0]->berat?>">
									    <?php }else{ ?>
									    	<input type="number" value="1" min="1" class="form-control" id="berat" name="berat">
									    <?php } ?>
									    <div class="input-group-append">
									      <span class="input-group-text">Kg</span>
									    </div>
									  </div>

									<div class="form-group">  
									  
									</div>

									<p>Lokasi Asal :</p>
									<div class="form-group">  
									  <select class="form-control" id="sel1" disabled>
									    <option value=""> Pilih Provinsi</option>            
									  </select>
									</div>

									<div class="form-group">  
									  <select class="form-control" id="sel2" disabled>
									    <option value=""> Pilih Kota</option>            
									  </select>
									</div>

									<p>Lokasi Tujuan :</p>


									<div class="form-group">  
									  <select class="form-control" id="sel11">
									    <option value=""> Pilih Provinsi</option>            
									  </select>
									</div>

									<div class="form-group">  
									  <select class="form-control" id="sel22" disabled>
									    <option value=""> Pilih Kota</option>            
									  </select>
									</div>

									<div class="form-group">  
									  <select class="form-control" id="kurir" disabled>
									    <option value=""> Pilih Kurir</option>
									    <option value="jne">JNE</option>
									    <!-- <option value="jnt">J&T</option> -->
									    <option value="tiki">TIKI</option>
									    <option value="pos">POS Indonesia</option>
									  </select>
									</div>

									<div id="hasil"></div>

									</div>
							  </div>
						  	</div>
						  	<div class="row">
							  <div class="col">
							  	<a href="<?=base_url()?>transaksi/addCart/<?=$produk[0]->id_produk;?>" class="btn button-custom btn-block">Tambahkan ke Keranjang</a>
							  </div>
      			</div>
      		</div>
      		</div>
      	</div>
    <!-- <?php }?> -->


      <!-- <div class="col-sm-4">
        <div class="card">
          <div class="card-body">
	          <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
	          <p class="text-center text-muted">Catering</p>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-body">
	          <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
	          <p class="text-center text-muted">Catering</p>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-body">
	          <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
	          <p class="text-center text-muted">Catering</p>
          </div>
        </div>
      </div> -->
    <!-- </div>
    <div class="row text-center">
      <center> -->
        <!--Tampilkan pagination-->
        <!-- <?php echo $pagination; ?></center>
    </div> -->
    <!-- /.row -->
</div>
  </div>
  <script type="text/javascript">
  
function getLokasi() {
  var $op = $("#sel1"), $op1 = $("#sel11");
  
  $.getJSON("<?=base_url()?>ongkir/provinsi", function(data){  
    $.each(data, function(i,field){  
    
       $op.append('<option value="'+field.province_id+'">'+field.province+'</option>'); 
       $op1.append('<option value="'+field.province_id+'">'+field.province+'</option>'); 

    });
	  $op.val("11");
  });
 
}
function getOrigin(origin,des,qty,cour) {
  var $op = $("#hasil"); 
  var i, j, x = "";
  
  $.getJSON("<?=base_url()?>ongkir/tarif/"+origin+"/"+des+"/"+qty+"/"+cour, function(data){     
    $.each(data, function(i,field){  

      for(i in field.costs)
      {

      	// set value input
          x += "<p class='mb-0'><b>" + field.costs[i].service + "</b> : "+field.costs[i].description+"</p>";

           for (j in field.costs[i].cost) {
                x += "Rp "+field.costs[i].cost[j].value +"<br>"+field.costs[i].cost[j].etd+"<br>"+field.costs[i].cost[j].note;
            }
         
      }

      $op.html(x);

    });
  });
 
}


function getKota1(idpro) {
  var $op = $("#sel22"); 
  
  $.getJSON("<?=base_url()?>ongkir/kota/"+idpro, function(data){      
    $.each(data, function(i,field){  
    

       $op.append('<option value="'+field.city_id+'">'+field.type+' '+field.city_name+'</option>'); 

    });
    
  });
 
}
  
function getKota(idpro) {
  var $op = $("#sel2"); 
  
  $.getJSON("<?=base_url()?>ongkir/kota/"+idpro, function(data){      
    $.each(data, function(i,field){  
    

       $op.append('<option value="'+field.city_id+'">'+field.type+' '+field.city_name+'</option>'); 

    });
    
	$("#sel2").val("248");
  });
 
}
$(document).ready(function() {
	getLokasi();
	getKota("11");

	$("#sel11").on("change", function(e){
	  e.preventDefault();
	  var option = $('option:selected', this).val();    
	  $('#sel22 option:gt(0)').remove();
	  $('#kurir').val('');

	  if(option==='')
	  {
	    alert('null');    
	    $("#sel22").prop("disabled", true);
	    $("#kurir").prop("disabled", true);
	  }
	  else
	  {        
	    $("#sel22").prop("disabled", false);
	    getKota1(option);
	  }
	});

	$("#sel22").on("change", function(e){
	  e.preventDefault();
	  var option = $('option:selected', this).val();    
	  $('#kurir').val('');

	  if(option==='')
	  {
	    alert('null');    
	    $("#kurir").prop("disabled", true);
	  }
	  else
	  {        
	    $("#kurir").prop("disabled", false);    
	  }
	});


	$("#kurir").on("change", function(e){
	  e.preventDefault();
	  var option = $('option:selected', this).val();    
	  var origin = $("#sel2").val();
	  var des = $("#sel22").val();
	  var qty = $("#berat").val();

	  if(qty==='0' || qty==='')
	  {
	    alert('null');
	  }
	  else if(option==='')
	  {
	    alert('null');        
	  }
	  else
	  {                
	    getOrigin(origin,des,qty,option);
	  }
	});

})



// $("#sel1").on("change", function(e){
//   e.preventDefault();
//   var option = $('option:selected', this).val();    
//   $('#sel2 option:gt(0)').remove();
//   $('#kurir').val('');

//   if(option==='')
//   {
//     alert('null');    
//     $("#sel2").prop("disabled", true);
//     $("#kurir").prop("disabled", true);
//   }
//   else
//   {        
//     $("#sel2").prop("disabled", false);
//     getKota(option);
//   }
// });


</script>
<?php $this->load->view('tmp_pub/footer');?>