<?php $this->load->view('tmp_pub/new_header');?>
<?php
function rupiah($angka)
{

    $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
    return $hasil_rupiah;

}
?>
<style type="text/css">
.pad5 {
    margin-right: 5px;
    margin-left: 0px;
    padding: 0px;
    border-radius: 0px;
}

.mari5 {
    margin-left: 5px;
    margin-right: 0px;
    padding: 0px;
    border-radius: 0px;
}

.hrevent {
    width: 90%;
}

.hrcek {
    height: 5px;
    width: 30%;
    margin: auto;
    background-color: orange;
    border: 0px;
    border-radius: 5px;
}

.col-md-3 {
    display: inline-block;
    margin-left: -4px;
}

.col-md-3 img {
    width: 100%;
}

body .carousel-indicators li {
    background-color: white;
    border: 1px solid black;
    box-shadow: 5px
}

body .carousel-indicators {
    bottom: 0;
}

body .carousel-control-prev-icon,
body .carousel-control-next-icon {
    background-color: black;
    box-shadow: 5px
}

body .no-padding {
    padding-left: 0;
    padding-right: 0;
}

#demo {
    margin-top: 10px;
    margin-bottom: 10px;
}

.bgroundgray {
    /*background-color: #f4f4f4;*/
    border: 0px;
}
</style>
<div class="container-fluid"
    style="padding-top: 240px;background-image: url('<?=base_url()?>asset/header_web/IMG_0617.jpg');background-repeat: no-repeat;background-position: top;background-size: cover;height: 500px">
    <center>
        <h1 style="color: white" class="font-weight-bold text-center text-uppercase">Produk</h1>
    </center>
</div>
<div class="container-fluid">
    <h3 class="font-weight-bold text-center text-uppercase" style="margin-top: 40px;margin-bottom: 0px;color: #BAD369">
        Kualitas dan Mutu Kami Terjaga</h3>
    <h5 class="text-center text-muted text-uppercase"
        style="font-family: 'Noto Sans', Sans-serif;padding: 20px;color: white">Dapatkan Informasi Produk UMKM Unggulan
        Di sini!</h5>
    <hr style="width: 300px;max-height: 10px;border: 3px solid #BAD369;background-color: #BAD369">
</div>
<div class="container-fluid">
    <div class="row" style="padding-left: 10%;padding-right: 10%;margin-top: 30px">
        <?php foreach ($produk as $key) {?>
        <div class="col-sm-6" style="margin-bottom: 15px;">
            <div class="card shadow">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm">
                            <img src="<?=base_url()?>picture/<?=$key->gambar?>" class="img-fluid" alt="..."
                                style="width: 400px;height: 400px;">
                        </div>
                        <div class="col-sm">
                            <p class="font-weight-bold" style="font-size: 16pt;margin-bottom: 10px">
                                <a class="text-dark"
                                    href="<?=base_url()?>desa/produk_detail/<?=$key->id_produk?>"><?=$key->produk?></a>
                            </p>
                            <p style="font-size: 10pt;margin-bottom: 5px">Stok : <?php if ($key->stok > 1): ?>
                                tersedia
                                <?php endif?></p>
                            <font style="font-size: 10pt"><?=substr($key->keterangan, 0, 100)?> ... </font>
                            <p class="text-center font-weight-bold" style="font-size: 14pt;margin-bottom: 5px">
                                <?=rupiah($key->harga)?></p>

                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-sm" style="padding-bottom:3px;">
                            <a href="<?=base_url()?>desa/produk_detail/<?=$key->id_produk;?>"
                                class="btn btn-block button-custom">Detail Produk</a>
                        </div>
                        <div class="col-sm">
                            <a href="<?=base_url()?>transaksi/addCart/<?=$key->id_produk;?>"
                                class="btn btn-block button-custom">Tambah ke keranjang</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php }?>
    </div>
</div>
<div class="container">
    <center>
        <!--Tampilkan pagination-->
        <?php echo $pagination; ?>
    </center>
</div>
<?php $this->load->view('tmp_pub/footer');?>