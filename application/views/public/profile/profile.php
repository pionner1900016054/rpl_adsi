<?php $this->load->view('tmp_pub/new_header');?>
<style type="text/css">
	.pad5{
		margin-right: 5px;
		margin-left: 0px;
		padding: 0px;
		border-radius: 0px;
	}
	.mari5{
		margin-left: 5px;
		margin-right: 0px;
		padding: 0px;
		border-radius: 0px;
	}
	.hrevent{
		width: 90%;
	}
	.hrcek{
		height: 5px;width: 30%;margin: auto;background-color: orange;border:0px;border-radius: 5px;
	}
	.col-md-3{
	  display: inline-block;
	  margin-left:-4px;
	}
	.col-md-3 img{
	  width:100%;
	}
	body .carousel-indicators li{
	  background-color:white;
	  border:1px solid black;
	  box-shadow: 5px
	}
	body .carousel-indicators{
	  bottom: 0;
	}
	body .carousel-control-prev-icon,
	body .carousel-control-next-icon{
	  background-color:black;
	  box-shadow: 5px
	}
	body .no-padding{
	  padding-left: 0;
	  padding-right: 0;
	   }
	#demo{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.bgroundgray{
		/*background-color: #f4f4f4;*/
		border:0px;
	}
</style>
<div class="container-fluid" style="padding-top: 100px;height: 250px">
    	<h3 class="font-weight-bold text-center text-uppercase" style="margin-top: 40px;margin-bottom: 0px;color: #BAD369">PROFIL ANDA</h3>
	<h5 class="text-center text-muted text-uppercase" style="font-family: 'Noto Sans', Sans-serif;padding: 20px;color: white">LENGKAPI PROFIL ANDA</h5>
	<hr style="width: 300px;max-height: 10px;border: 3px solid #BAD369;background-color: #BAD369">
</div>
<div class="container-fluid" style="padding-left: 10%;padding-right: 10%;margin-top: 30px">
    <!-- <div class="card shadow p-3 mb-5 bg-white rounded"> -->
    <div class="card" style="padding: 40px;border: 0px">
    	<div class="text-center">
    		<img src="<?=base_url()?>avatar/<?=$this->session->avatar?>" class="rounded-circle" width="100" height="100" alt="logo"><br>
    		<button class="btn button-custom" data-toggle="modal" data-target="#modalFoto">Ubah Foto Profil</button><br><br>
    		<button class="btn button-custom" data-toggle="modal" data-target="#modalPassword">Ubah Kata Sandi</button>
    	</div>
    	<br>
    	<!-- <form> -->
    	<?=form_open('profile/ubah_data/')?>
		  <div class="form-group row">
		    <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
		    <div class="col-sm-10">
		      <input type="text" disabled readonly class="form-control-plaintext" id="staticEmail" value="<?=$profile[0]->email?>">
		    </div>
		  </div>
		  <div class="form-group row">
		    <label  class="col-sm-2 col-form-label">Nama Lengkap</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="inputNama" name="Nama" value="<?=$profile[0]->member?>">
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="inputTanggal" class="col-sm-2 col-form-label">Tanggal Lahir</label>
		    <div class="col-sm-10">
		      <input type="date" class="form-control" id="inputTanggal" name="tanggal" value="<?=$profile[0]->tanggal_lahir?>">
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="inputNoTelpon" class="col-sm-2 col-form-label">No Telepon</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="inputNoTelpon" name="no_telpon" value="<?=$profile[0]->no_telpon?>">
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="inputPassword" class="col-sm-2 col-form-label">Alamat Lengkap</label>
		    <div class="col-sm-10">
		    	<textarea class="form-control" name="alamat"><?=$profile[0]->alamat?></textarea>
		    </div>
		  </div>
		  <div class="form-group row">
		  	<div class="col-sm-2"></div>
		  	<div class="col-sm-10"><button type="submit" class="btn button-custom btn-block">Simpan</button></div>
		    </div>
		  </div>
		<!-- </form> -->
		<?=form_close()?>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalFoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ubah Foto Profil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="text-center">
    		<img src="<?=base_url()?>avatar/<?=$this->session->avatar?>" class="rounded-circle" width="100" height="100" alt="logo"><br>
    	</div>
    	<div class="row" style="padding-left: 10%">
    		<p >Foto Profil Baru</p>
    		<?=form_open_multipart('profile/ubah_foto')?>
    		<div class="col-sm"><input type="file" name="gambar" required class="form-control"><br></div>
    		<div class="col-sm"><input type="submit" name="submit" value="Simpan" class="btn button-custom btn-block"></div>
    		<?=form_close()?>
    	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ubah Kata Sandi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
    		<?=form_open_multipart('profile/ganti_password')?>
    		<div class="col-sm"><input type="password" name="password_lama" required class="form-control" placeholder="Masukkan Kata Sandi Lama"><br></div>
    		<div class="col-sm"><input type="password" name="password_baru" required class="form-control" placeholder="Masukkan Kata Sandi Baru"><br></div>
    		<div class="col-sm"><input type="password" name="confirm_password" required class="form-control" placeholder="Masukkan Ulang Kata Sandi Baru"><br></div>
    		<div class="col-sm"><input type="submit" name="submit" value="Simpan" class="btn button-custom btn-block"></div>
    		<?=form_close()?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('tmp_pub/footer');?>