<?php $this->load->view('tmp_pub/new_header');?>
<?php
function rupiah($angka)
{

    $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
    return $hasil_rupiah;

}
?>
<style type="text/css">
.pad5 {
    margin-right: 5px;
    margin-left: 0px;
    padding: 0px;
    border-radius: 0px;
}

.mari5 {
    margin-left: 5px;
    margin-right: 0px;
    padding: 0px;
    border-radius: 0px;
}

.hrevent {
    width: 90%;
}

.hrcek {
    height: 5px;
    width: 30%;
    margin: auto;
    background-color: orange;
    border: 0px;
    border-radius: 5px;
}

.col-md-3 {
    display: inline-block;
    margin-left: -4px;
}

.col-md-3 img {
    width: 100%;
}

body .carousel-indicators li {
    background-color: white;
    border: 1px solid black;
    box-shadow: 5px
}

body .carousel-indicators {
    bottom: 0;
}

body .carousel-control-prev-icon,
body .carousel-control-next-icon {
    background-color: black;
    box-shadow: 5px
}

body .no-padding {
    padding-left: 0;
    padding-right: 0;
}

#demo {
    margin-top: 10px;
    margin-bottom: 10px;
}

.bgroundgray {
    /*background-color: #f4f4f4;*/
    border: 0px;
}
</style>
<div class="container-fluid"
    style="padding-top: 240px;background-image: url('<?=base_url()?>asset/header_web/IMG_0613.jpg');background-repeat: no-repeat;background-position: center;background-size: cover;height: 500px">
    <center>
        <h1 style="color: white" class="font-weight-bold text-center text-uppercase">Tiket</h1>
    </center>
</div>
<div class="container-fluid">
    <h2 class="text-center font-weight-bold text-uppercase" style="padding-top: 40px;margin-bottom: 0px">
        Pesan Tiket</h2>
    <h5 class="text-center text-muted" style="padding-top: 40px;">Pesan Tiket Paket Wisatamu Di sini!</h5>
    <?php foreach ($tiket as $key) {?>
    <div class="row" style="padding-left: 10%;padding-right: 10%;padding-bottom: 30px">
        <div class="card col-lg shadow" style="margin-bottom: 20px;border-radius: 20px">
            <div class="row">
                <div class="col-sm-3" style="padding-left: 0px;padding-right: 0px">
                    <img style="max-width: 100%;max-height: 200px;max-width: 100%;border-radius: 20px 0 0 20px"
                        id="img_tiket" src="<?=base_url()?>picture/<?=$key->gambar?>" class="img" alt="">
                </div>
                <div class="col-sm-6 d-flex align-content-center flex-wrap">
                    <div class="p-4 bd-highlight">
                        <h3 class="font-weight-bold"><?=$key->produk?>
                            <?=$key->produk == "Paket EduWisata Souvenir" ? "<a class='badge badge-info'>Mantap</a>" : ""?>
                        </h3>

                        <?php if ($key->produk == "Paket Pilihan"): ?>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Hallo sobat!</strong> Paket ini bisa milih kegiatan yang kamu ingin, cukup checklist
                            kegiatan apa saja yang ingin kamu ikuti.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php endif?>

                        <?php if ($key->produk == "Paket Pilihan"): ?>
                        <?php foreach ($kegiatan as $a): ?>

                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chk" id="<?=$a->id_kegiatan?>"
                                value="<?=$a->harga_kegiatan?>" data-time="<?=$a->durasi_kegiatan?>">
                            <label style="color: gray;" class="custom-control-label"
                                for="<?=$a->id_kegiatan?>"><?=$a->Nama_kegiatan?> (<a href="" class="detail-kegiatan"
                                    data-toggle="modal" data-target="#exampleModal" data-Nama="<?=$a->Nama_kegiatan?>"
                                    data-durasi="<?=$a->durasi_kegiatan?>" data-harga="<?=$a->harga_kegiatan?>"
                                    data-deskripsi="<?=$a->deskripsi_kegiatan?>" id="detail-kegiatan">Detail
                                    kegiatan</a>)
                            </label>
                        </div>


                        <?php endforeach?>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chk" id="voucher" value="20000">
                            <label style="color: gray;" class="custom-control-label" for="voucher">Souvenir produk UMKM
                                menarikk
                            </label>
                        </div>

                        <?php else: ?>
                        <?php $acara = $this->M_template->get_kegiatan_tiket($key->id_produk)?>
                        <?php foreach ($acara as $a): ?>
                        <p style="color: gray;"><?=$a->Nama_kegiatan?> (<a href="" class="detail-kegiatan"
                                data-toggle="modal" data-target="#exampleModal" data-Nama="<?=$a->Nama_kegiatan?>"
                                data-durasi="<?=$a->durasi_kegiatan?>" data-harga="<?=$a->harga_kegiatan?>"
                                data-deskripsi="<?=$a->deskripsi_kegiatan?>" id="detail-kegiatan">Detail
                                kegiatan</a>)
                        </p>

                        <?php endforeach;?>
                        <?php endif;?>

                        <font style="color: gray"><?=$key->keterangan?> </font>

                    </div>

                    <!-- <a href="<?=base_url()?>desa/tiket" class="btn btn-dark">Go somewhere</a> -->
                </div>
                <div class="col-sm-2 d-flex align-content-center flex-wrap" style="border-left: 1px solid lightgray">
                    <div class="p-4 bd-highlight text-center" style="padding-left: 8%">
                        <?php if ($key->produk == "Paket Pilihan"): ?>
                        <h5 class="font-weight-bold" id="harga-pilihan"><i></i>Rp 0</h5>
                        <?php else: ?>
                        <h5 class="font-weight-bold"><i></i><?=rupiah($key->harga)?></h5>

                        <?php endif?>

                        <?php if ($key->produk == "Paket Pilihan"): ?>
                        <input type="hidden" id="waktu">

                        <a class="btn btn-block" style="color: white;background-color: #afcb50; display: none;"
                            data-produk="<?=$key->id_produk?>" id="paketpilihan">Pesan Tiket</a>

                        <?php else: ?>
                        <a href="<?=base_url()?>transaksi/addCart/<?=$key->id_produk;?>" class="btn btn-block"
                            style="color: white;background-color: #afcb50">Pesan Tiket</a>
                        <?php endif?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php }?>
</div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Kegiatan</th>
                            <th scope="col">Harga Kegiatan</th>
                            <th scope="col">Durasi Kegiatan</th>
                            <th scope="col">Deskripsi Kegiatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td id="Nama"></td>
                            <td id="harga"></td>
                            <td id="durasi"></td>
                            <td id="deskripsi" style="width: 500px;text-align: justify;"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<script>
const formatRupiah = (money) => {
    return new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',
        minimumFractionDigits: 0
    }).format(money);
}
</script>

<script>
$('.detail-kegiatan').on('click', function() {

    var Nama_kegiatan = $(this).data('Nama');
    var durasi_kegiatan = $(this).data('durasi');
    var harga_kegiatan = $(this).data('harga');
    var deskripsi_kegiatan = $(this).data('deskripsi');

    $(".modal-title").html(Nama_kegiatan);
    $(".modal-body #Nama").html(Nama_kegiatan);
    $(".modal-body #harga").html(formatRupiah(harga_kegiatan));
    $(".modal-body #durasi").html(durasi_kegiatan);
    $(".modal-body #deskripsi").html(deskripsi_kegiatan);

    // alert(deskripsi_kegiatan)

})
</script>

<script>
$('.chk').on('click', function() {

    var val = [];
    var total = 0
    var total_waktu = 0;

    $(':checkbox:checked').each(function(i) {
        var waktu = $(this).data('time').replace(['Menit', 'Jam'], '');
        waktu = Number(waktu.replace(/[^0-9\.]+/g, ""));
        total = total + Number($(this).val());
        total_waktu = total_waktu + waktu;
    });

    if (total > 0) {
        $('#paketpilihan').show();
    } else {
        $('#paketpilihan').hide();
    }

    $('#harga-pilihan').html(formatRupiah(total) + ",00");
    $('#waktu').val(total_waktu);

    // console.log(total_waktu)

})
</script>

<script>
$('#paketpilihan').on('click', function() {

    var val = [];
    var total = 0
    $(':checkbox:checked').each(function(i) {
        total = total + Number($(this).val());
    });

    var idproduk = $(this).data('produk');
    var waktu = $('#waktu').val();

    // alert(waktu)

    window.location.href = "<?=base_url()?>transaksi/addCartPaketPilihan/" + idproduk + "/" + total + "/" +
        waktu;

})
</script>

<!-- </div> -->
<?php $this->load->view('tmp_pub/footer');?>
