<?php $this->load->view('tmp_pub/new_header');?>
<?php
function rupiah($angka)
{

    $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
    return $hasil_rupiah;

}
?>
<link rel="stylesheet" href="<?=base_url()?>asset/AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<style type="text/css">
.pad5 {
    margin-right: 5px;
    margin-left: 0px;
    padding: 0px;
    border-radius: 0px;
}

.mari5 {
    margin-left: 5px;
    margin-right: 0px;
    padding: 0px;
    border-radius: 0px;
}

.hrevent {
    width: 90%;
}

.hrcek {
    height: 5px;
    width: 30%;
    margin: auto;
    background-color: orange;
    border: 0px;
    border-radius: 5px;
}

.col-md-3 {
    display: inline-block;
    margin-left: -4px;
}

.col-md-3 img {
    width: 100%;
}

body .carousel-indicators li {
    background-color: white;
    border: 1px solid black;
    box-shadow: 5px
}

body .carousel-indicators {
    bottom: 0;
}

body .carousel-control-prev-icon,
body .carousel-control-next-icon {
    background-color: black;
    box-shadow: 5px
}

body .no-padding {
    padding-left: 0;
    padding-right: 0;
}

#demo {
    margin-top: 10px;
    margin-bottom: 10px;
}

.bgroundgray {
    /*background-color: #f4f4f4;*/
    border: 0px;
}
</style>
<div class="container-fluid" style="padding-top: 180px;height: 400px">
    <h3 class="font-weight-bold text-center text-uppercase" style="margin-top: 40px;margin-bottom: 0px;color: #BAD369">
        Checkout Keranjang</h3>
    <h5 class="text-center text-muted text-uppercase"
        style="font-family: 'Noto Sans', Sans-serif;padding: 20px;color: white">Tinggal sedikit lagi pembelianmu selesai
    </h5>
    <hr style="width: 300px;max-height: 10px;border: 3px solid #BAD369;background-color: #BAD369">
</div>
<div class="container-fluid">
    <div class="row" style="padding-left: 10%;padding-right: 10%;margin-top: 30px">
        <div class="col shadow p-3 mb-5 bg-white rounded" style="margin: 10px">
            <div class="card" style="border: 0px !important">
                <div class="card-header" style="background-color: white !important;">
                    <!-- <div class="col-sm-12 col-md-6"> -->
                    <h4 class="card-title">Daftar Produk Pesanan</h4>
                    <!-- </div> -->
                </div>
                <div class="card-body">
                    <?php
foreach ($distict_checkout as $kategori):
    if ($kategori->kategori == 'Produk') {
        ?>
                    <h5>Produk UMKM</h5>
                    <?php foreach ($keranjang as $key) {?>
                    <?php if ($key->kategori == "Produk"): ?>
                    <div class="row">
                        <div class="col-2"></div>
                        <div class="col-4"><img src="<?=base_url()?>picture/<?=$key->gambar;?>" class="img" height="100"
                                width="auto"></div>
                        <div class="col-4">
                            <table>
                                <tr>
                                    <td colspan="2"><?=$key->produk?></td>
                                    <!-- <td></td> -->
                                </tr>
                                <tr>
                                    <td>Harga</td>
                                    <td><?=rupiah($key->harga)?></td>
                                </tr>
                                <tr>
                                    <td>Jumlah</td>
                                    <td><?=$key->jumlah?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?php endif?>
                    <?php }?>
                    <?php }
endforeach?>
                    <br><br>

                    <?php
foreach ($distict_checkout as $kategori):
    if ($kategori->kategori == 'Tiket') {
        ?>
                    <h5>Tiket Paket Kampung Sinau</h5>
                    <?php $jml = 0?>
                    <?php foreach ($keranjang as $key) {?>
                    <?php if ($key->kategori == "Tiket"): ?>
                    <div class="row">
                        <div class="col-2"></div>
                        <div class="col-4"><img src="<?=base_url()?>picture/<?=$key->gambar;?>" class="img" height="100"
                                width="auto"></div>
                        <div class="col-4">
                            <table>
                                <tr>
                                    <td colspan="2"><?=$key->produk?></td>
                                    <!-- <td></td> -->
                                </tr>
                                <tr>
                                    <td>Harga</td>


                                    <td>
                                        <?php if ($key->produk == "Paket Pilihan"): ?>
                                        <?php $h1 = str_replace(["Rp", "."], "", $_COOKIE['harga_pilihan'])?>
                                        <?php $h = substr($h1, 0, 2);?>
                                        <?php $h = str_replace($h, "", $h1)?>

                                        <?=$_COOKIE['harga_pilihan'] . ",00"?>
                                        <?php $harga = $h * $key->jumlah;?>
                                        <?php else: ?>
                                        <?=rupiah($key->harga)?>
                                        <?php $harga = $key->harga * $key->jumlah;?>
                                        <?php endif?>

                                        <?php $jml = $jml + $harga?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Jumlah</td>
                                    <td><?=$key->jumlah?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?php endif?>
                    <?php }?>
                    <?php }
endforeach?>


                    <hr>
                    <div class="row">
                        <div class="col-sm-2 col-form-div"></div>
                        <div class="col-sm-8">
                            <div class="container p-5">
                                <?=form_open('transaksi/proses_transaksi/' . $istiketonly);?>
                                <input type="hidden" name="keranjang_id" value="<?=$keranjang_id?>">
                                <?php
if ($istiket == true) {
    ?>
                                <p>Isi Tanggal Tiket Paket Kujungan <?=$this->session->Nama_app;?></p>
                                <div class="form-group">
                                    <input type="date" class="form-control" name="tanggal" id="tanggal" required>
                                </div>
                                <?php }
if ($istiketonly == 0) {
    ?>
                                <p>Penerima</p>
                                <div class="form-group">
                                    <input type="text" onkeypress="return onlyNumberKey(event)" class="form-control"
                                        name="penerima" id="penerima" value="<?=$this->session->member?>" required>
                                </div>
                                <p>No Telpon Penerima</p>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="notelponpenerima"
                                        id="notelponpenerima" value="<?=$this->session->notelpon?>" required>
                                </div>
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Total Berat</span>
                                    </div>
                                    <?php if ($totalberat > 1) {?>
                                    <input type="number" min="1" class="form-control" readonly id="berat" name="berat"
                                        value="<?=$totalberat?>">
                                    <?php } else {?>
                                    <input type="number" value="1" min="1" readonly class="form-control" id="berat"
                                        name="berat">
                                    <?php }?>
                                    <div class="input-group-append">
                                        <span class="input-group-text">Kg</span>
                                    </div>
                                </div>

                                <!-- <p>Lokasi Asal :</p> -->
                                <div class="form-group">
                                    <input type="hidden" name="sel1" id="sel1" value="11">
                                    <!-- <select class="form-control" id="sel1">
									    <option value=""> Pilih Provinsi</option>
									  </select> -->
                                </div>

                                <div class="form-group">
                                    <input type="hidden" name="sel2" id="sel2" value="247">
                                    <!-- <select class="form-control" id="sel2" disabled>
									    <option value=""> Pilih Kota</option>
									  </select> -->
                                </div>

                                <p>Alamat Pengiriman :</p>
                                <div class="form-group">
                                    <textarea class="form-control" name="alamat_kirim" required
                                        placeholder="Perum Bintang Mandiri, Desa Linglih, Kecamatan Longloh"></textarea>
                                </div>

                                <p>Provinsi Pengiriman :</p>
                                <div class="form-group">
                                    <select class="form-control" id="sel11" name="sel11">
                                        <option value=""> Pilih Provinsi</option>
                                    </select>
                                </div>
                                <input type="hidden" name="getprovinsi" id="getprovinsi">

                                <p>Kota/Kabupaten Pengiriman:</p>
                                <div class="form-group">
                                    <select class="form-control" id="sel22" name="sel22">
                                        <option value=""> Pilih Kota</option>
                                    </select>
                                </div>
                                <input type="hidden" name="getkabupaten" id="getkabupaten">

                                <p>Pilih Kurir :</p>
                                <div class="form-group">
                                    <select class="form-control" id="kurir" name="kurir">
                                        <option value=""> Pilih Kurir</option>
                                        <option value="jne">JNE</option>
                                        <!-- <option value="jnt">J&T</option> -->
                                        <option value="tiki">TIKI</option>
                                        <option value="pos">POS Indonesia</option>
                                    </select>
                                </div>
                                <div id="hasil"></div>
                                <p>Pilih Paket Kurir :</p>
                                <div class="form-group">
                                    <select class="form-control" id="pilih-jenis" name="paket_kurir">
                                        <option value=""> Pilih Jenis Pengiriman</option>
                                    </select>
                                </div>
                                <p>Kode Pos :</p>
                                <div class="form-group">
                                    <input type="text" onkeypress="return onlyNumberKey(event)" required name="kodepos"
                                        class="form-control" required placeholder="77126">
                                </div>
                                <input type="hidden" name="service" id="service">
                                <?php }?>
                                <p>Pilih Rekening Tujuan Transfer :</p>
                                <div class="form-group">
                                    <select class="form-control" id="pilih-rekening" name="rekening">
                                        <option value=""> Pilih Rekening Pembayaran</option>
                                        <?php foreach ($rekening as $key) {?>
                                        <option value="<?=$key->id_rekening?>"><?=$key->bank?> | <?=$key->no_rekening?>|
                                            Atas Nama <?=$key->Nama_rekening?> </option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div id="hasiltotalpembayaran"></div>
                                <input type="hidden" name="totalall" id="totalall">
                                <input type="hidden" name="totalpesanan" id="totalpesanan"
                                    value="<?=$totalbayarproduk?>">
                                <div class="form-group">
                                    <input type="submit" disabled id="btn-sub" class="btn btn-block button-custom"
                                        value="Selesaikan Pembayaran">
                                </div>
                                <?=form_close()?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function getLokasi() {
    $op1 = $("#sel11");

    $.getJSON("<?=base_url()?>ongkir/provinsi", function(data) {
        $.each(data, function(i, field) {

            // $op.append('<option value="'+field.province_id+'">'+field.province+'</option>');
            $op1.append('<option value="' + field.province_id + '">' + field.province + '</option>');

        });

    });

}

function getOrigin(origin, des, qty, cour) {
    var $op = $("#hasil");
    var i, j, x = "";

    $.getJSON("<?=base_url()?>ongkir/tarif/" + origin + "/" + des + "/" + qty + "/" + cour, function(data) {
        $.each(data, function(i, field) {

            for (i in field.costs) {

                // set value input
                x += "<p class='mb-0'><b>" + field.costs[i].service + "</b> : " + field.costs[i]
                    .description + "</p>";

                for (j in field.costs[i].cost) {
                    x += "Rp " + field.costs[i].cost[j].value + "<br>" + field.costs[i].cost[j].etd +
                        " hari <br>" + field.costs[i].cost[j].note;
                    console.log(field.costs[i].cost[j].value);
                    console.log(field.costs[i].cost[j].etd);
                    console.log(field.costs[i].cost[j]);
                    $('#pilih-jenis').append('<option value="' + cour + '--' + field.costs[i].service +
                        '--' + field.costs[i].cost[j].value + '"><b>' + field.costs[i].service +
                        '</b> | Rp ' + field.costs[i].cost[j].value + ' | ' + field.costs[i].cost[j]
                        .etd + ' hari </option>');
                }

            }

            $op.html(x);

        });
    });

}


function getKota1(idpro) {
    var $op = $("#sel22");

    $.getJSON("<?=base_url()?>ongkir/kota/" + idpro, function(data) {
        $.each(data, function(i, field) {


            $op.append('<option value="' + field.city_id + '">' + field.type + ' ' + field.city_name +
                '</option>');

        });

    });

}

function onlyNumberKey(evt) {

    // Only ASCII charactar in that range allowed
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
    return true;
}
$(document).ready(function() {
    getLokasi();

    $('#pilih-rekening').on("change", function() {
        var istiketonly = '<?=$istiketonly?>';
        if (istiketonly == 1) {
            var piljenis = $('#pilih-jenis').val();
            var harga_total = <?=$totalbayarproduk;?>;
            var kode_unik = <?=$rand_code;?>;
            // var harga_kirim = piljenis.split("--");
            // if (istiketonly == 1) {
            var hargatotalpesanan = parseInt(harga_total) + parseInt(kode_unik);
            // }else{
            //  var hargatotalpesanan = parseInt(harga_total)+parseInt(harga_kirim[2])+parseInt(kode_unik);
            // }

            // console.log(harga_kirim[2]);
            console.log(kode_unik);
            var hasiltotal =
                `<p>Subtotal Produk : Rp ${<?=$jml?>} </p><p>Kode Unik : <b>${kode_unik}<b/></p><p>Total Pesanan : <b>Rp ${hargatotalpesanan}</b></p>`;
            $('#hasiltotalpembayaran').html(hasiltotal);
            $('#totalall').val(hargatotalpesanan);
            $('#btn-sub').removeAttr('disabled');
        } else {
            var piljenis = $('#pilih-jenis').val();
            var harga_total = <?=$totalbayarproduk;?>;
            var kode_unik = <?=$rand_code;?>;
            var harga_kirim = piljenis.split("--");
            // var istiketonly=<?=$istiketonly?>;
            // if (istiketonly == true) {
            //  var hargatotalpesanan = parseInt(harga_total)+parseInt(kode_unik);
            // }else{
            var hargatotalpesanan = parseInt(harga_total) + parseInt(harga_kirim[2]) + parseInt(
                kode_unik);
            // }

            console.log(harga_kirim[2]);
            console.log(kode_unik);
            var hasiltotal =
                `<p>Subtotal Produk : Rp ${harga_total} </p><p>Pengiriman : Rp ${harga_kirim[2]}</p><p>Kode Unik : <b>${kode_unik}<b/></p><p>Total Pesanan : <b>Rp ${hargatotalpesanan}</b></p>`;
            $('#hasiltotalpembayaran').html(hasiltotal);
            $('#totalall').val(hargatotalpesanan);
            $('#btn-sub').removeAttr('disabled');
        }
    });
    // $('#pilih-rekening').on("change",function() {
    // 	var piljenis= $('#pilih-jenis').val();
    //     var harga_total= <?=$totalbayarproduk;?>;
    //     var harga_kirim = piljenis.split("--");
    //     var istiketonly=<?=$istiketonly?>;
    //     if (istiketonly == true) {
    // 	    var hargatotalpesanan = parseInt(harga_total);
    //     }else{
    // 	    var hargatotalpesanan = parseInt(harga_total)+parseInt(harga_kirim[2]);
    //     }

    //     console.log(harga_kirim[2]);
    //     var hasiltotal=`<p>Subtotal Produk : Rp ${harga_total} </p><p>Pengiriman : Rp ${harga_kirim[2]}</p><p>Total Pesanan : <b>Rp ${hargatotalpesanan}</b></p>`;
    //     $('#hasiltotalpembayaran').html(hasiltotal);
    //     $('#totalall').val(hargatotalpesanan);
    //     $('#btn-sub').removeAttr('disabled');
    // });

    $("#pilih-jenis").on("change", function(e) {
        // $('#btn-sub').removeAttr('disabled');
        e.preventDefault();
        var option = $('#kurir').val();
        var origin = $("#sel2").val();
        var des = $("#sel22").val();
        var qty = $("#berat").val();

        if (qty === '0' || qty === '') {
            alert('null');
        } else if (option === '') {
            alert('null');
        } else {
            var hasil_pilih_jenis = $('#pilih-jenis').val();
            $('#service').val(hasil_pilih_jenis);
            // getHiddenOrigin(origin,des,qty,option);

        }
    });

    $("#kurir").on("change", function(e) {
        e.preventDefault();
        var option = $('option:selected', this).val();
        var origin = $("#sel2").val();
        var des = $("#sel22").val();
        var qty = $("#berat").val();

        if (qty === '0' || qty === '') {
            alert('null');
        } else if (option === '') {
            alert('null');
        } else {
            getOrigin(origin, des, qty, option);
        }
    });

    $("#sel11").on("change", function(e) {
        e.preventDefault();
        var option = $('option:selected', this).val();
        $('#sel22 option:gt(0)').remove();
        $('#kurir').val('');

        if (option === '') {
            alert('null');
            $("#sel22").prop("disabled", true);
            $("#kurir").prop("disabled", true);
        } else {
            $("#sel22").prop("disabled", false);
            getKota1(option);
        }
    });


    $("#sel22").on("change", function(e) {
        e.preventDefault();
        var option = $('option:selected', this).val();
        $('#kurir').val('');

        if (option === '') {
            alert('null');
            $("#kurir").prop("disabled", true);
        } else {
            $("#kurir").prop("disabled", false);
        }
    });

});
</script>
<?php $this->load->view('tmp_pub/footer');?>