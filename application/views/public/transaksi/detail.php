<?php $this->load->view('tmp_pub/new_header'); ?>
<style type="text/css">
	.gagal{
		color: red;
	}
</style>
<div style="height: 100px"></div>
<!-- Main content -->
<div class="content">
    <div class="container-fluid" style="padding-left: 10%;padding-right: 10%;margin-top: 30px">
      <div class="row"><br>
          <?=$this->session->flashdata('invoice');?></div>
          <br><br>
      	<div class="row">
      		<div class="col-lg-12">
      			<div class="card">
      				<div class="card-header">
      					<div class="col-sm-12 col-md-6">
      						<h4 class="card-title">Detail Order</h4>
      					</div>
      				</div>
      				<div class="card-body">
						  <div class="form-group">
							  <!-- <div class="form-group row">
						      	<img src="<?=base_url()?>picture/<?=$transaksi[0]->gambar;?>" width="auto"  alt="" class="img-fluid img-thumbnail" style="max-width: 50%">
							  </div> -->
							</div>
							<div class="form-group row">
							    <div for="inp-tanggal_selesai" class="col-sm-4 col-form-div">Tanggal Order</div>
							    <div class="col-sm-6">
							    	<?=$transaksi[0]->date?>
							    </div>
							  </div>
							  <div class="form-group row">
							    <div for="inp-tanggal_selesai" class="col-sm-4 col-form-div">Diorder oleh member</div>
							    <div class="col-sm-6">
							    	<?=$transaksi[0]->member?>
							    </div>
							  </div>
							  <div class="form-group row">
							    <div for="inp-tanggal_mulai" class="col-sm-4 col-form-div">Pembayaran Transfer Via Bank</div>
							    <div class="col-sm-6">
							    	<?=$transaksi[0]->bank;?><br>
							    	<?=$transaksi[0]->no_rekening;?><br>
							    	<?=$transaksi[0]->Nama_rekening;?><br>
							    </div>
							  </div>
							  <div class="form-group row">
							    <div for="inp-kuota" class="col-sm-4 col-form-div">Penerima</div>
							    <div class="col-sm-6">
							    	<?=$transaksi[0]->penerima;?>
							    </div>
							  </div>
							  <div class="form-group row">
							    <div for="inp-kuota" class="col-sm-4 col-form-div">No Telpon Penerima</div>
							    <div class="col-sm-6">
							    	<?=$transaksi[0]->notelponpenerima;?>
							    </div>
							  </div>
							  <div class="form-group row">
							    <div for="inp-kuota" class="col-sm-4 col-form-div">Alamat Penerima</div>
							    <div class="col-sm-6">
							    	<?=$transaksi[0]->alamat_penerima;?>,<?=$transaksi[0]->kabupaten_penerima;?>,<?=$transaksi[0]->provinsi_penerima;?>,<?=$transaksi[0]->kodepos_penerima;?>
							    </div>
							  </div>
							  <div class="form-group row">
							    <div for="inp-kuota" class="col-sm-4 col-form-div">Kurir Pengiriman</div>
							    <div class="col-sm-6">
							    	<?=$transaksi[0]->kurir;?>
							    </div>
							  </div>
							  <div class="form-group row">
							    <div for="inp-kuota" class="col-sm-4 col-form-div">Jenis Paket Kurir Pengiriman</div>
							    <div class="col-sm-6">
							    	<?=$transaksi[0]->paket_kurir;?>
							    </div>
							  </div>
							  <div class="form-group row">
							    <div class="col-sm-4 col-form-div">Total Berat</div>
							    <div class="col-sm-6">
							    	<?=$transaksi[0]->berattotal;?> KG
							    </div>
							  </div>
							  <div class="form-group row">
							    <div for="inp-kuota" class="col-sm-4 col-form-div">Total Biaya Order</div>
							    <div class="col-sm-6">
							    	Ongkos Kirim : Rp <?=$transaksi[0]->totalkirim;?><br>
							    	Subtotal Pesanan :  Rp <?=$transaksi[0]->totalpesanan;?><br><hr>
							    	Total Pembayaran Pesanan :  Rp <?=$transaksi[0]->totalbayar;?>
							    </div>
							  </div>
                <div class="form-group row">
                  <div class="col-sm-4 col-form-div">Status Transaksi</div>
                  <div class="col-sm-6 alert alert-info">
                    <?php
                      if ($transaksi[0]->statustransaksi==0) {
                        echo "Belum diproses";
                      }elseif ($transaksi[0]->statustransaksi==1) { 
                        echo "Sedang diproses";
                      }elseif ($transaksi[0]->statustransaksi==2) { 
                        echo "Selesai diproses";
                      }elseif ($transaksi[0]->statustransaksi==3) { 
                        echo "Cancel. Kadaluarsa";
                      }
                    ?>
                  </div>
                </div>
							  <!-- <div class="form-group row">
							    <div for="inp-keterangan" class="col-sm-4 col-form-div">Keterangan</div>
							    <div class="col-sm-6">
							    		<?=$transaksi[0]->keterangan;?>
							  </div>
						  	</div> -->
						  <!-- </div> -->
                <div class="row">
                    <div class="col-6 text-center">
                      <p style="font-weight: bold" class="text-center">Status Bukti Pembayaran</p>
                      <?=$this->session->flashdata('error');?>
                      <?php if ($transaksi[0]->statusbukti == 1 && $transaksi[0]->verifikasibukti == 1){?>
                             <a href="<?=base_url()?>pembayaran/<?=$transaksi[0]->buktibayar?>" target="_blank">Lihat Bukti bayar</a>
                      <?php }else{?>
                        <?=form_open_multipart('transaksi/uploadbuktibayar/'.$transaksi[0]->id_transaksi);?>
                        <?php if ($transaksi[0]->statusbukti == 0){ ?>  
                         <p>Silahkan Upload Bukti Bayar<?php if ($transaksi[0]->verifikasibukti == 2 ){?> Ulang<?php }?></p>
                        <div class="row">
                          <div class="col-8"><input type="file" class="form-control" name="file" required></div>
                          <div class="col"><input type="submit" name="submit" value="Submit" class="btn btn-sm button-custom"></div>
                        </div>
                        <?=form_close();?>
                        <?php }else{echo "Menunggu Verifikasi Pembayaran";} ?>
                        <?php }?>
                      </div>
                      <div class="col-6 text-center">
                                                <p style="font-weight: bold" class="text-center">Status Verifikasi Bukti Pembayaran</p>
                                              <?php
                                      if ($transaksi[0]->verifikasibukti==0) {
                                        echo "Menunggu Verifikasi Bukti Pembayaran";
                                      }elseif ($transaksi[0]->verifikasibukti==1) { 
                                        echo "Bukti Pembayaran Valid";
                                      }elseif ($transaksi[0]->verifikasibukti==2) { 
                                        echo "Bukti Pembayaran Tidak Valid Silahkan Upload Lagi!";
                                      }
                                    ?>
                                            </div>
                                      </div>
      				</div>
      			</div>
      			<br>
      			<div class="card">
      			<div class="card-header">
      					<div class="col-sm-12 col-md-6">
      						<h4 class="card-title">Detail Produk Order</h4>
      					</div>
      				</div>
      				<div class="card-body">
      					<table class="table">
      						<tr>
      							<th>Gambar</th>
      							<th>Produk</th>
      							<th>Jumlah</th>
      						</tr>
      						<?php foreach ($produk_order as $key): ?>
      						<tr>
      							<td><img src="<?=base_url()?>picture/<?=$key->gambar;?>" class="img" height="100" width="auto"></td>
      							<td><?=$key->produk;?></td>
      							<td><?=$key->jumlah;?></td>
      						</tr>	
      						<?php endforeach ?>
      					</table>
      				</div></div>
      			<br>
            <?php if ($transaksi[0]->verifikasibukti == 1) { ?>
      			<div class="card">
      			<div class="card-header">
      					<div class="col-sm-12 col-md-6">
      						<h4 class="card-title">Data E-tiket</h4>
      					</div>
      				</div>
      				<div class="card-body">
      					<table class="table">
      						<tr>
      							<th>No</th>
      							<th>Tiket</th>
      							<th>Nama Member</th>
      							<th>Kode Tiket</th>
      							<th>E-Tiket</th>
      							<th>Tanggal Booking</th>
                                                <th>Status Expired</th>
      						</tr>
      						<?php $no=1; foreach ($etiket as $key): ?>
      						<tr>
                                                <td><?=$no;?></td>
      							<td><?=$key->produk?></td>
      							<td><?=$key->Nama_etiket?></td>
      							<td><?=$key->kodetiket;?></td>
      							<td><a href="<?=base_url()?>etiket/test_pdf/<?=$key->id_etiket;?>" target="_blank"><?=$key->etiket?>.pdf</a></td>
      							<td><?=$key->booking?><td>
      								<?php if ($key->expired == 0){
      									echo "Masih Berlaku";
      								}else{
                        echo "Sudah Tidak Berlaku";
                      }?>
   								</td>
      						</tr>	
      						<?php $no++; endforeach ?>
      					</table>
      				</div>
      			</div>
          <?php } ?>
                        <br>
                        <div class="card">
                        <div class="card-header">
                                    <div class="col-sm-12 col-md-6">
                                          <h4 class="card-title">Alur Order</h4>
                                    </div>
                              </div>
                              <div class="card-body">
                                    <table class="table">
                                          <tr>
                                                <th>No Resi</th>
                                                <th>Dikemas</th>
                                                <th>Tanggal Dikemas</th>
                                                <th>Dikirim</th>
                                                <th>Tanggal Dikirim</th>
                                                <th>Diterima</th>
                                                <th>Tanggal Diterima</th>
                                          </tr>
                                          <?php foreach ($alur_transaksi as $key): ?>
                                          <tr>
                                                <!-- <td><<?=$noresi?></td> -->
                                                <td><?=$key->noresi?></td>
                                                <td>
                                                      <?php if ($key->dikemas == 0){
                                                            echo "Belum dikemas";
                                                      }?>
                                                </td>
                                                <td>
                                                      <?=$key->tanggal_dikemas;?></td>
                                                <td>
                                                      <?php if ($key->dikirim == 0){
                                                            echo "Belum dikirim";
                                                      }?>
                                                </td>
                                                <td>
                                                      <?php if ($key->dikirim == 0){?>
                                                            
                                                      <?php }else{ ?>
                                                            <?=$key->tanggal_dikirim;?></td>
                                                      <?php }?>
                                                <td>
                                                      <?php if ($key->diterima == 0){
                                                            echo "Belum diterima";
                                                      }?>
                                                </td>
                                                <td><?php if ($key->diterima == 0){?>
                                                  <?php if ($key->dikirim == 1): ?>
                                                            <a href="<?=base_url()?>transaksi/ubahalurtransaksi/<?=$key->id_transaksi?>" >Sudah Diterima</a>
                                                  <?php endif ?>
                                                      <?php }else{ ?>
                                                            <?=$key->tanggal_diterima;?>
                                                      <?php }?></td>
                                          </tr> 
                                          <?php endforeach ?>
                                    </table>
                              </div>
                        </div>

      		</div>
      	</div>
    </div>
</div>
<br>
<?php $this->load->view('tmp_pub/footer'); ?>