<?php $this->load->view('tmp_pub/new_header'); ?>

<div class="container-fluid" style="padding-top: 180px;height: 400px">
    	<h3 class="font-weight-bold text-center text-uppercase" style="margin-bottom: 0px;color: #BAD369">Riwayat Transaksimu</h3>
	<h5 class="text-center text-muted text-uppercase" style="font-family: 'Noto Sans', Sans-serif;padding: 20px;color: white">Ayo belanja lagi</h5>
	<hr style="width: 300px;max-height: 10px;border: 3px solid #BAD369;background-color: #BAD369">
</div>

<div class="container-fluid">
    <div class="row" style="padding-left: 10%;padding-right: 10%;">
      		<div class="col-lg-12">
      			<br>
      			<div class="card">
      				<!-- <div class="card-header">
      					<div class="col-sm-12 col-md-6">
      					</div>
      				</div> -->
      				<div class="card-body">
      					<div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
		                  	<div class="row">
		                    	<div class="col-sm-12 col-md-6"></div>
		                    	<div class="col-sm-12 col-md-6"></div>
		                  	</div>
			                <div class="row">
			                  	<div class="col-sm-12">
			                  		<table id="example2" class="example2 table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
			                  			<thead>
						                  <tr role="row">
						                    <!-- <th>No</th> -->
						                    <th>Tanggal order</th>
						                    <th>Produk</th>
						                    <th>Status Bukti Bayar</th>
						                    <th>Verifikasi Bukti Bayar</th>
						                    <th>Status Transaksi Order</th>
						                    <th>Aksi</th>
						                  </tr>
			                  			</thead>
			                  			<tbody style="font-size: 8pt">
			                  				<?php $no=1; foreach ($transaksi as $key) { ?>
			                  				<tr>
			                  					<td><?=$key->date;?></td>
			                  					<td>
			                  						<?php foreach ($produk as $kuy): ?>
			                  							<?php if ($kuy->id_transaksi == $key->id_transaksi): ?>
			                  								<p><b><?=$kuy->produk?></b><br> Jumlah <?=$kuy->jumlah?></p>
			                  							<?php endif ?>
			                  						<?php endforeach ?>
			                  					</td>
			                  					<td>
			                  						<?php
			                  						if($key->statustransaksi != 3){
				                  						if ($key->statusbukti==0) {?>
				                  							Silahkan Upload Bukti Bayar<br>
				                  							<?=form_open_multipart('transaksi/uploadbuktibayar/'.$key->id_transaksi);?>
				                  							<div class="row" style="border-top: 0.5px solid lightgray">
				                  								<div class="col-8"><input type="file" class="form-control" name="file" required></div>
				                  								<div class="col"><input type="submit" name="submit" value="Submit" class="btn btn-sm button-custom"></div>
				                  							</div>
				                  							<?=form_close();?>
				                  						<?php }elseif ($key->statusbukti==1) { ?>
				                  							<a href="<?=base_url()?>pembayaran/<?=$key->buktibayar?>" target="_blank">Lihat Bukti Bayar</a>
				                  					<?php }}elseif($key->statustransaksi == 3){ echo "Cancel. Kadaluarsa";}?>
				                  				</td>
				                  				<td>
			                  						<?php
			                  						if($key->statustransaksi != 3){
				                  						if ($key->verifikasibukti==0) {
				                  							echo "Menunggu Bukti Pembayaran";
				                  						}elseif ($key->verifikasibukti==1) { 
				                  							echo "Bukti Pembayaran Valid";
				                  						}elseif ($key->verifikasibukti==2) { 
				                  							echo "Bukti Pembayaran Tidak Valid";
				                  						}
			                  				    }elseif($key->statustransaksi == 3){ echo "Cancel. Kadaluarsa";}
				                  					?>
				                  				</td>
				                  				<td>
			                  						<?php
				                  						if ($key->statustransaksi==0) {
				                  							echo "Belum diproses";
				                  						}elseif ($key->statustransaksi==1) { 
				                  							echo "Sedang diproses";
				                  						}elseif ($key->statustransaksi==2) { 
				                  							echo "Selesai diproses";
				                  						}elseif ($key->statustransaksi==3) { 
				                  							echo "Cancel. Kadaluarsa";
				                  						}
				                  					?>
				                  				</td>
			                  					<td>
			                  						<a type="button" class="btn btn-secondary btn-sm" href="<?=base_url()?>transaksi/detail/<?=$key->id_transaksi?>"><i class="fa fa-eye"></i></a>
			                  					</td>
			                  				</tr>
			                  				<?php $no++; } ?>
			                  			</tbody>
			                		</table>
			                	</div>
			                </div>
		            	</div>
      				</div>
      			</div>
      		</div>
      	</div>
    </div>
 <script src="<?=base_url()?>asset/AdminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>asset/AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>asset/AdminLTE/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=base_url()?>asset/AdminLTE/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script>
  $(document).ready(function() {
    $('.example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
<?php $this->load->view('tmp_pub/footer'); ?>