<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-light bg-light elevation-4">
    <!-- Brand Logo -->
    <a href="<?=base_url()?>Admin" class="brand-link text-white text-center">
        <img src="<?=$this->session->logo_app;?>" alt="AdminLTE Logo" class="img-fluid"
            style="max-height: 150px;max-width: 100%"><br>
        <span class="brand-text font-weight-light">
            <!-- <?=$this->session->Nama_app;?>  Admin-->
        </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?=base_url()?>avatar/<?=$this->session->avatar;?>" class="img-circle elevation-2"
                    alt="User Image" style="height: 35px;width: 35px;">
            </div>
            <div class="info">
                <a href="#" class="d-block" style="font-weight: bold;"><?=$this->session->Admin;?></a>
                <span style="color: #c2c7d0;font-size: 10pt"><?php if ($this->session->role == 1) {
    echo "Super";
} else {
    echo "Admin";
}?></span>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2 text-white">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="<?=base_url()?>Admin/"
                        class="nav-link <?php if ($this->session->func == 'dash') {?> active<?php }?>">
                        <i class="nav-icon">
                            <img src="<?=$this->session->logo_app;?>" alt="AdminLTE Logo"
                                style="max-height:40px;max-width: 100%">
                        </i>
                        <p>Dashborad</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=base_url()?>Admin/order/"
                        class="nav-link <?php if ($this->session->func == 'orders') {?> active<?php }?>">
                        <i class="fas fa-receipt nav-icon"></i>
                        <p>Pesanan</p>
                    </a>
                </li>
                <!-- <li class="nav-item">
            <a href="<?=base_url()?>Admin/paket" class="nav-link <?php if ($this->session->func == 'paket') {?> active<?php }?>">
              <i class="fa fa-object-group nav-icon"></i>
              <p>Paket Wedding</p>
            </a>
          </li> -->
                <li class="nav-item">
                    <a href="<?=base_url()?>Admin/produk"
                        class="nav-link <?php if ($this->session->func == 'produk') {?> active<?php }?>">
                        <i class="fa fa-clipboard-list nav-icon"></i>
                        <p>Produk</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=base_url()?>Admin/kegiatan"
                        class="nav-link <?php if ($this->session->func == 'kegiatan') {?> active<?php }?>">
                        <i class="fa fa-hiking nav-icon"></i>
                        <p>Kegiatan</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=base_url()?>Admin/event"
                        class="nav-link <?php if ($this->session->func == 'event') {?> active<?php }?>">
                        <i class="fa fa-calendar nav-icon"></i>
                        <p>Event</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=base_url()?>Admin/artikel"
                        class="nav-link <?php if ($this->session->func == 'artikel') {?> active<?php }?>">
                        <i class="fa fa-newspaper nav-icon"></i>
                        <p>Artikel</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=base_url()?>Admin/user"
                        class="nav-link <?php if ($this->session->func == 'user') {?> active<?php }?>">
                        <i class="fa fa-users-cog nav-icon" aria-hidden="true"></i>
                        <p>Pengguna</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=base_url()?>Admin/rekening"
                        class="nav-link <?php if ($this->session->func == 'rekening') {?> active<?php }?>">
                        <i class="fa fa-credit-card nav-icon"></i>
                        <p>Rekening</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=base_url()?>Admin/galeri"
                        class="nav-link <?php if ($this->session->func == 'galeri') {?> active<?php }?>">
                        <i class="fa fa-images nav-icon" aria-hidden="true"></i>
                        <p>Galeri</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=base_url()?>Admin/konfigurasi"
                        class="nav-link <?php if ($this->session->func == 'konfigurasi') {?> active<?php }?>">
                        <i class="fa fa-wrench nav-icon" aria-hidden="true"></i>
                        <p>Konfigurasi</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

<div class="container-fluid">