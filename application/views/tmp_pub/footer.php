<div class="container-fluid" style="background-color: #F5F5F5;font-family: 'Montserrat', Sans-serif">
    <div class="row" style="padding-left: 10%;padding-right: 10%;padding-top: 20px">
    	<div class="col">
    		<h4 style="color: #afcb50" class="font-weight-bold">Tentang Kami</h4>
	            <p class="text-justify" style="color: #808080"></p>
    	</div>
    	<div class="col text-center">
    		<h4 style="color: #afcb50" class="font-weight-bold">Hubungi Kami</h4><br>
    		<div class="row">
    			<div class="col">
    				<h5 class="font-weight-bold"><i class="fa fa-phone"></i></h5>
		            <p style="color: #808080"><?=$this->session->kontak_satu;?><br>
		            <?=$this->session->kontak_dua;?><br>
		            <?=$this->session->kontak_tiga;?></p>
    			</div>
    			<div class="col">
    				<h5 class="font-weight-bold"><i class="fa fa-envelope"></i></h5>
	            	<p style="color: #808080"><?=$this->session->email;?></p>
    			</div>
    		</div>
    	</div>
    	<div class="col text-center">
    		<h4 class="font-weight-bold" style="color: #afcb50">Lokasi Kami</h4>
	            <div class="embed-responsive embed-responsive-16by9 rounded" style="height: 200px">
			        <?=$this->session->embed_maps;?>
				</div>
    	</div>
	</div>
	<!-- <div class="contai"></div> -->
	
	<br>
</div>

<footer style="background-color: #BAD369 !important;height: 80px;padding-top: 10px">

<p style="text-align: center;color: white;font-weight: bold;">Copyright &copy; 2021 <?=$this->session->Nama_app;?>.</p>

</footer>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script> -->
<!-- <script src="<?=base_url()?>asset/jquery.mask.min.js"></script> -->
<script type="text/javascript">
    // $( '.uang' ).mask('000.000.000.000.0000', {reverse: true});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<!-- <script type="text/javascript" src="<?=base_url()?>asset/bootstrap/js/"></script> -->
</body>
</html>