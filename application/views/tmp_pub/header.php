<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/ico" href="<?=$this->session->logo_app?>"> 

  <title><?=$this->session->name_app?></title>
  <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/styleee.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/bootstrap/css/bootstrap.css">
<style type="text/css">
	.card-title{margin-bottom:.75rem}
	.card-title{float:left;font-size:1.1rem;font-weight:400;margin:0}
	.card-text{clear:both}
	.card-title{font-size:1rem}
	#password-strength-status {
        padding: 5px 10px;
        color: #FFFFFF;
        border-radius: 4px;
        margin-top: 5px;
    }

    .medium-password {
        background-color: #b7d60a;
        border: #BBB418 1px solid;
    }
    .weak-password {
         background-color: #ce1d14;
        border: #AA4502 1px solid;
    }
    .strong-password {
               background-color: #12CC1A;
        border: #0FA015 1px solid;
    }
    .button-custom{
  background-color: #BAD369 !important;
  color: white !important;
}
.button-custom:hover{
  background-color: white!important;
  color: #BAD369 !important;
  border:1px solid #BAD369 !important;
}
</style>
</head>
<body>
<?php $this->load->view('tmp_pub/nav');?>
<script src="<?=base_url()?>asset/AdminLTE/plugins/jquery/jquery.min.js"></script>