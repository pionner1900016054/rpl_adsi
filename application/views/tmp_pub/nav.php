<div class="top-container" style="background-color: #f4f4f4">
  <div class="row">
    <div class="col">
      <img src="<?=$this->session->logo_app?>" alt="logo" width="auto" height="100">
    </div>
    <div class="col" id="colcontactus">
      <a href="<?=base_url()?>desa/read_profile/Contact-Us" id="acontactus" style="color:#BAD369 !important;"><i class="fa fa-phone" style="font-size:13pt;"></i> Contact Us</a>
    </div>    
  </div>
  <!-- <h1>Scroll Down</h1>
  <p>Scroll down to see the sticky effect.</p> -->
</div>

<div class="header" id="myHeader" style="background-color: #BAD369 !important;">
  <nav class="navbar navbar-expand-sm navbar-dark">
    <a class="navbar-brand" href="<?=base_url()?>desa"><?=$this->session->Nama_app?></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Navbar links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <div class="dropdown">
            <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Profile Desa
            </a>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a class="dropdown-item text-body" style="color: #BAD369 !important" href="<?=base_url()?>desa/read_profile/Profil-Desa">Profile Desa </a>
              <a class="dropdown-item text-body" style="color: #BAD369 !important" href="<?=base_url()?>desa/read_profile/Sejarah-Desa">Sejarah Desa</a>
              <a class="dropdown-item text-body" style="color: #BAD369 !important" href="<?=base_url()?>desa/read_profile/Visi-Misi">Visi & Misi Desa</a>
            </div>
          </div>
          <!-- <a class="nav-link" href="#">Profil Desa</a> -->
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?=base_url()?>desa/artikel/">Artikel</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?=base_url()?>desa/event/">Event</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?=base_url()?>desa/galeri/">Galeri</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?=base_url()?>desa/produk/">Produk UMKM</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?=base_url()?>desa/tiket">Pesan Tiket</a>
        </li>
        <!-- <li class="nav-item">
          <a class="nav-link" href="#">Kontak</a>
        </li> -->
      </ul>
      <ul class="navbar-nav">
      <?php if ($this->session->isLogin == false) { ?>
          <li><a href="<?=base_url()?>login" class="nav-link">
            Login
          </a></li>&nbsp
          <li><a href="<?=base_url()?>login/register" class="nav-link">
            Register
          </a></li>
      <?php }else{ ?>
        <li><a href="<?=base_url()?>transaksi/keranjang" class="nav-link">
            <i class="fa fa-shopping-cart"></i> Keranjang
        </a></li>&nbsp
        <div class="dropdown">
          <a type="button" class="dropdown-toggle" data-toggle="dropdown" style="color: white;text-decoration: none;">
            <img src="<?=base_url()?>asset/user.jpg" class="rounded-circle" width="35" height="35" alt="logo">
            <?=$this->session->username;?>
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="<?=base_url()?>transaksi/">Transaksi Pesanan</a>
            <a class="dropdown-item" href="<?=base_url()?>profile/myprofile/<?=$this->session->id_member?>">Profile</a>
            <!-- <a class="dropdown-item" href="#">Link 3</a> -->
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?=base_url()?>login/logout/">Logout</a>
          </div>
        </div>
      <?php } ?>
        <!-- <li class="nav-item">
          <a class="nav-link" href="#">
            <img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/logo_white.png" width="30" height="30" alt="logo">
          </a>
        </li> -->
      </ul>
    </div>
  </nav>
</div>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}</script>
