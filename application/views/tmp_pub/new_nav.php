<!-- Just an image -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top "
    style="background-color: white;border-bottom: 1px solid #BAD369;padding-left: 7%;padding-right: 7%">
    <div style="margin: 10px">
        <a class="navbar-brand" href="<?=base_url()?>">
            <img src="<?=$this->session->logo_app?>" width="50" height="auto" alt="">
        </a>
    </div>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        </ul>
        <ul class="navbar-nav my-2 my-lg-0">

            <li class="nav-item">
                <div class="dropdown">
                    <a class="nav-link dropdown-toggle lead <?php if ($this->session->flashdata('active') == 'profil'): ?>
              active
            <?php endif?>" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        Profil Desa
                    </a>

                    <div class="dropdown-menu" style="z-index: 3;border:2px solid #BAD369"
                        aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item nav-link" href="<?=base_url()?>desa/read_profile/Profil-Desa">Profil
                            Desa </a>
                        <a class="dropdown-item nav-link" href="<?=base_url()?>desa/read_profile/Visi-Misi">Visi & Misi
                            Desa</a>
                    </div>
                </div>
                <!-- <a class="nav-link" href="#">Profil Desa</a> -->
            </li>
            <li class="nav-item">
                <div class="dropdown">
                    <a class="nav-link dropdown-toggle lead <?php if ($this->session->flashdata('active') == 'berita'): ?>
              active
            <?php endif?>" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        Berita
                    </a>
                    <div class="dropdown-menu" style="z-index: 3;border:2px solid #BAD369"
                        aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item nav-link" href="<?=base_url()?>desa/artikel/">Berita</a>
                        <a class="dropdown-item nav-link" href="<?=base_url()?>desa/event/">Event</a>
                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link lead <?php if ($this->session->flashdata('active') == 'galeri'): ?>
              active
            <?php endif?>" href="<?=base_url()?>desa/galeri/">Galeri</a>
            </li>
            <li class="nav-item">
                <div class="dropdown">
                    <a class="nav-link dropdown-toggle lead <?php if ($this->session->flashdata('active') == 'produk'): ?>
              active
            <?php endif?>" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        Paket Wisata
                    </a>
                    <div class="dropdown-menu" style="z-index: 3;border:2px solid #BAD369"
                        aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item nav-link" href="<?=base_url()?>desa/tiket">Pesan Tiket</a>
                        <a class="dropdown-item nav-link" href="<?=base_url()?>desa/produk/">Produk UMKM</a>
                    </div>
                </div>
            </li>
            <?php if ($this->session->isLogin == false) {?>
            <li><a href="<?=base_url()?>login" class="btn button-custom nav-link">
                    <i class="fa fa-sign-in"></i>&nbsp Masuk
                </a></li>
            <?php } else {?>
            <li><a href="<?=base_url()?>transaksi/keranjang" class="nav-link lead <?php if ($this->session->flashdata('active') == 'keranjang'): ?>
              active
            <?php endif?>">
                    <i class="fa fa-shopping-cart"></i> Keranjang
                </a></li>&nbsp
            <div class="dropdown">
                <a type="button" class="dropdown-toggle" data-toggle="dropdown"
                    style="color: white;text-decoration: none;">
                    <img src="<?=base_url()?>avatar/<?=$this->session->avatar?>" class="rounded-circle" width="35"
                        height="35" alt="logo">
                    <?=$this->session->username;?>
                </a>
                <div class="dropdown-menu dropdown-menu-right" style="border:2px solid #BAD369">
                    <a class="dropdown-item nav-link" href="<?=base_url()?>transaksi/">Transaksi Pesanan</a>
                    <a class="dropdown-item nav-link" href="<?=base_url()?>profile/">Profile</a>
                    <!-- <a class="dropdown-item nav-link" href="#">Link 3</a> -->
                    <a class="dropdown-item nav-link" href="<?=base_url()?>login/logout/">Logout</a>
                </div>
            </div>
            <?php }?>
        </ul>
    </div>
</nav>