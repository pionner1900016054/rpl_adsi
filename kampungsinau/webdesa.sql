-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyAdmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 19, 2021 at 10:56 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webdesa`
--

-- --------------------------------------------------------

--
-- Table structure for table `acara`
--

CREATE TABLE `acara` (
  `id_acara` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `acara`
--

INSERT INTO `acara` (`id_acara`, `id_produk`, `id_kegiatan`) VALUES
(10, 2, 1),
(11, 2, 3),
(16, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Admins`
--

CREATE TABLE `Admins` (
  `id_Admin` int(11) NOT NULL,
  `Admin` varchar(255) NOT NULL,
  `no_telpon` varchar(15) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Admins`
--

INSERT INTO `Admins` (`id_Admin`, `Admin`, `no_telpon`, `alamat`) VALUES
(1, 'Admin Desa Bedoho', '082282710200', 'Desa Bedoho, Kabupaten Madiun, Jawa Timur'),
(2, '404 Not Found Team', '0812345678', 'Telkom University');

-- --------------------------------------------------------

--
-- Table structure for table `alur_transaksies`
--

CREATE TABLE `alur_transaksies` (
  `id_alur_transaksi` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `noresi` varchar(255) DEFAULT NULL,
  `dikemas` int(11) NOT NULL DEFAULT 0,
  `tanggal_dikemas` timestamp NULL DEFAULT NULL,
  `dikirim` int(11) NOT NULL DEFAULT 0,
  `tanggal_dikirim` timestamp NULL DEFAULT NULL,
  `diterima` int(11) NOT NULL DEFAULT 0,
  `tanggal_diterima` timestamp NULL DEFAULT NULL,
  `date_alur` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_alur_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `alur_transaksies`
--

INSERT INTO `alur_transaksies` (`id_alur_transaksi`, `id_transaksi`, `noresi`, `dikemas`, `tanggal_dikemas`, `dikirim`, `tanggal_dikirim`, `diterima`, `tanggal_diterima`, `date_alur`, `updated_alur_at`) VALUES
(1, 1, NULL, 0, NULL, 0, NULL, 0, NULL, '2020-11-06 22:18:10', '2020-11-06 22:18:10'),
(4, 3, NULL, 1, '2020-11-29 15:26:58', 0, NULL, 0, NULL, '2020-11-07 08:20:27', '2020-11-07 08:20:27'),
(5, 4, NULL, 1, '2020-11-14 03:13:57', 0, NULL, 0, NULL, '2020-11-07 08:20:54', '2020-11-07 08:20:54'),
(6, 5, NULL, 0, NULL, 0, NULL, 0, NULL, '2020-11-07 08:21:58', '2020-11-07 08:21:58'),
(7, 6, NULL, 1, '2020-11-14 03:26:35', 0, NULL, 0, NULL, '2020-11-07 19:21:14', '2020-11-07 19:21:14'),
(8, 7, '12319238123', 1, '2020-11-08 06:55:35', 1, '2020-11-08 07:18:48', 1, '2020-11-08 07:45:29', '2020-11-08 06:31:42', '2020-11-08 06:31:42'),
(9, 8, '1283712312', 1, '2020-11-09 15:02:37', 1, '2020-11-09 15:03:47', 1, '2020-11-09 14:58:55', '2020-11-09 14:58:15', '2020-11-09 14:58:15'),
(10, 9, '312312312', 1, '2020-11-09 15:08:33', 1, '2020-11-09 15:17:16', 1, '2020-11-09 15:17:51', '2020-11-09 15:01:06', '2020-11-09 15:01:06'),
(11, 10, NULL, 0, NULL, 0, NULL, 0, NULL, '2020-11-27 19:10:46', '2020-11-27 19:10:46'),
(12, 11, '-', 1, '2020-11-28 19:38:19', 1, '2020-11-28 19:42:16', 0, NULL, '2020-11-28 15:14:11', '2020-11-28 15:14:11'),
(13, 12, NULL, 1, '2020-11-28 16:59:19', 0, NULL, 0, NULL, '2020-11-28 15:20:17', '2020-11-28 15:20:17'),
(14, 13, NULL, 1, '2020-11-28 18:07:20', 0, NULL, 0, NULL, '2020-11-28 17:59:49', '2020-11-28 17:59:49'),
(15, 14, NULL, 1, '2020-11-28 19:58:10', 0, NULL, 0, NULL, '2020-11-28 18:05:06', '2020-11-28 18:05:06'),
(16, 15, NULL, 1, '2020-11-28 19:38:01', 0, NULL, 0, NULL, '2020-11-28 18:07:10', '2020-11-28 18:07:10'),
(17, 16, NULL, 0, NULL, 0, NULL, 0, NULL, '2020-11-28 18:11:07', '2020-11-28 18:11:07'),
(18, 17, NULL, 1, '2020-11-28 19:57:29', 0, NULL, 0, NULL, '2020-11-28 19:56:43', '2020-11-28 19:56:43'),
(19, 18, NULL, 0, NULL, 0, NULL, 0, NULL, '2020-11-28 20:02:23', '2020-11-28 20:02:23'),
(20, 19, NULL, 0, NULL, 0, NULL, 0, NULL, '2020-11-29 15:36:29', '2020-11-29 15:36:29'),
(21, 20, 'GEKA%20GANTENG', 1, '2020-11-29 16:09:20', 1, '2020-11-29 16:09:35', 0, NULL, '2020-11-29 16:02:11', '2020-11-29 16:02:11'),
(22, 21, '87878', 1, '2020-11-29 16:14:19', 1, '2020-11-29 17:28:47', 0, NULL, '2020-11-29 16:11:22', '2020-11-29 16:11:22'),
(23, 22, NULL, 0, NULL, 0, NULL, 0, NULL, '2020-11-29 17:26:39', '2020-11-29 17:26:39'),
(24, 23, NULL, 0, NULL, 0, NULL, 0, NULL, '2020-11-30 14:16:54', '2020-11-30 14:16:54'),
(25, 24, NULL, 0, NULL, 0, NULL, 0, NULL, '2020-12-02 03:40:16', '2020-12-02 03:40:16'),
(26, 25, '11111', 1, '2020-12-02 10:41:51', 1, '2020-12-02 10:42:24', 1, '2021-07-04 14:19:12', '2020-12-02 10:39:50', '2020-12-02 10:39:50'),
(27, 26, NULL, 0, NULL, 0, NULL, 0, NULL, '2020-12-13 14:58:13', '2020-12-13 14:58:13'),
(28, 27, NULL, 1, '2021-04-07 09:18:48', 0, NULL, 0, NULL, '2021-04-07 09:17:52', '2021-04-07 09:17:52'),
(29, 28, NULL, 0, NULL, 0, NULL, 0, NULL, '2021-04-07 09:34:42', '2021-04-07 09:34:42'),
(30, 29, NULL, 0, NULL, 0, NULL, 0, NULL, '2021-04-07 09:46:35', '2021-04-07 09:46:35'),
(31, 30, NULL, 1, '2021-04-07 13:24:13', 0, NULL, 0, NULL, '2021-04-07 13:23:11', '2021-04-07 13:23:11'),
(32, 31, NULL, 1, '2021-04-12 13:49:32', 0, NULL, 0, NULL, '2021-04-12 11:52:47', '2021-04-12 11:52:47'),
(33, 32, NULL, 0, NULL, 0, NULL, 0, NULL, '2021-04-13 04:18:33', '2021-04-13 04:18:33'),
(34, 33, 'null', 1, '2021-06-22 13:10:40', 1, '2021-04-20 05:59:52', 0, NULL, '2021-04-13 17:02:12', '2021-04-13 17:02:12'),
(35, 34, NULL, 0, NULL, 0, NULL, 0, NULL, '2021-07-05 08:39:59', '2021-07-05 08:39:59'),
(36, 35, NULL, 0, NULL, 0, NULL, 0, NULL, '2021-07-05 08:41:14', '2021-07-05 08:41:14'),
(37, 36, NULL, 0, NULL, 0, NULL, 0, NULL, '2021-07-05 08:41:14', '2021-07-05 08:41:14'),
(38, 37, NULL, 0, NULL, 0, NULL, 0, NULL, '2021-07-06 07:53:33', '2021-07-06 07:53:33'),
(39, 38, NULL, 0, NULL, 0, NULL, 0, NULL, '2021-07-06 07:54:43', '2021-07-06 07:54:43'),
(40, 39, NULL, 0, NULL, 0, NULL, 0, NULL, '2021-07-06 07:55:28', '2021-07-06 07:55:28'),
(41, 40, NULL, 0, NULL, 0, NULL, 0, NULL, '2021-07-06 14:00:38', '2021-07-06 14:00:38'),
(42, 41, 'JP2187743730', 1, '2021-07-07 00:51:49', 1, '2021-07-07 00:53:42', 0, NULL, '2021-07-07 00:47:32', '2021-07-07 00:47:32'),
(43, 42, NULL, 0, NULL, 0, NULL, 0, NULL, '2021-07-07 01:06:15', '2021-07-07 01:06:15'),
(44, 43, NULL, 1, '2021-07-07 01:38:12', 0, NULL, 0, NULL, '2021-07-07 01:34:56', '2021-07-07 01:34:56'),
(45, 44, NULL, 0, NULL, 0, NULL, 0, NULL, '2021-07-07 01:41:20', '2021-07-07 01:41:20'),
(46, 45, 'JP21346098', 1, '2021-07-07 01:44:33', 1, '2021-07-07 01:45:23', 0, NULL, '2021-07-07 01:42:11', '2021-07-07 01:42:11'),
(47, 46, NULL, 0, NULL, 0, NULL, 0, NULL, '2021-07-07 02:02:11', '2021-07-07 02:02:11'),
(48, 47, NULL, 0, NULL, 0, NULL, 0, NULL, '2021-07-07 02:04:26', '2021-07-07 02:04:26'),
(49, 48, NULL, 0, NULL, 0, NULL, 0, NULL, '2021-07-07 02:14:21', '2021-07-07 02:14:21'),
(50, 49, NULL, 0, NULL, 0, NULL, 0, NULL, '2021-07-14 11:53:35', '2021-07-14 11:53:35');

-- --------------------------------------------------------

--
-- Table structure for table `artikels`
--

CREATE TABLE `artikels` (
  `id_artikel` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `penulis` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `artikels`
--

INSERT INTO `artikels` (`id_artikel`, `judul`, `isi`, `gambar`, `kategori`, `penulis`, `status`, `created_at`) VALUES
(2, 'Sejarah Desa', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\r\n', 'Green_and_Black_Travel_Agency_Logo(4).png', 'Sejarah-Desa', 'Pemerintah Desa', 1, '2020-11-08 14:29:48'),
(3, 'Visi Misi Desa', '<p>1. Visi dan Misi</p>\r\n\r\n<p>Tantangan birokrasi Pemerintah Desa di masa depan meliputi berbagai aspek baik yang bersifat alamiah maupun Politik, Ekonomi, Sosial Budaya. Pertahanan &amp; Keamanan, Ilmu Pengetahuan dan Teknologi serta Agama.</p>\r\n\r\n<p>Seiring dengan penerapan Otonomi Daerah yang luas dan bertanggungjawab, maka diperlukan suatu Pemerintahan Desa yang berkualitas dan profesional dalam pelaksanaan tugas dan fungsinya, sehingga dapat benar-benar mewujudkan Pemerintahan yang Good Government sesuai tuntutan masyarakat. Guna memenuhi tuntutan kebutuhan masyarakat, maka Pemerintahan Desa harus memiliki visi dan misi ke depan.</p>\r\n\r\n<p>1.1 Visi</p>\r\n\r\n<p>Visi adalah gambaran kondisi masa depan yang lebih baik (ideal), dibandingkan dengan kondisi yang ada saat ini. Setiap organisasi selalu mempunyai harapan jauh kedepan, kemana dan bagaimana organisasi itu akan dibawa serta bekerja, agar tetap eksis dan konsisten. Penyusunan Visi Desa Sukolilo dilakukan dengan pendekatan partisipatif yang melibatkan pemerintahan desa, BPD, LPMD, tokoh masyarakat dengan mempertimbangkan potensi dan nilainilai budaya yang ada dan tumbuh di masyarakat.</p>\r\n\r\n<p>Untuk itulah Pemerintah Desa Bedoho dalam mencapai cita-citanya mempunyai visi :</p>\r\n\r\n<p><strong>&ldquo;TERBANGUNYA TATA KELOLA PEMERINTAHAN DESAYANG BAIK DAN BERSIH, GUNA MEWUJUDKAN DESA BEDOHO YANG LEBIH SEJAHTERA, ADIL MAKMUR DAN BERMARTABAT&rdquo;</strong></p>\r\n\r\n<p>1.2 Misi</p>\r\n\r\n<p>Untuk mewujudkan visi tersebut, maka misi yang akan dilakukan adalah sebagai berikut:</p>\r\n\r\n<ol>\r\n	<li>menyelenggarakan pemerintahan desa yang brsih, demokratis, terbebas dari korupsi, kolusi nepotisme serta bentuk penyelewengan lainya..</li>\r\n	<li>Membangun perekonomian masyarakat melalui potensi desa.</li>\r\n	<li>Meningkatkan mutu kesejahtraan masyarakat untuk mencapai taraf kehidupan dan pendidikan masyarakat</li>\r\n	<li>Meningkatkan sarana dan prasarana masyarakat.</li>\r\n	<li>Meningkatkan sarana dan prasarana pemerintahan desa dan lembaga desa</li>\r\n</ol>\r\n\r\n<p><strong>TUJUAN DAN SASARAN</strong></p>\r\n\r\n<ol>\r\n	<li><strong>Untuk mencapai misi 1</strong>, yaitu Menyelenggarakan pemerintahan desa yang brsih, demokratis, terbebas dari korupsi, kolusi nepotisme serta bentuk penyelewengan lainya yang harus dilakukan antara lain :</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul style=\"list-style-type:circle\">\r\n	<li>Menciptakan serta kenyamanan tata kelola pemerintahan desa yang transparan dan akutanbel.</li>\r\n	<li>Meningkatkan pengelolaan pemerintahan desa yang berkomitmen serta bertanggung jawab</li>\r\n	<li>Meningkatnya rasa kepercayaan masyarakat terhadap pemerintahan desa.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>Untuk mencapai misi 2</strong>, yaitu mengembangkan perekonomian masyarakat melalui potensi desa yang akan dilaksanakan adalah sebagai berikut:</li>\r\n</ul>\r\n\r\n<ul style=\"list-style-type:circle\">\r\n	<li>Meningkatkan perekonomian masyarakat melalui Pengembangan BUMDesa yang ada.</li>\r\n	<li>Meningkatkan taraf hidup masyarakat desa</li>\r\n	<li>Meningkatkan perekonomian desa melalui UMKM di desa.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Untuk mencapai misi 3, yaitu meningkatkan mutu kesejahteraan masyarakat untuk tercapainya taraf kehidupan dan berpendidikan dan yang akan dilaksanakan adalah sebagai berikut:</li>\r\n</ul>\r\n\r\n<ul style=\"list-style-type:circle\">\r\n	<li>Meningkatnya mutu pendidikan TK DESA, TPA, TPQ serta Bentuk Pendidikan lainya yang ada di desa. &nbsp;</li>\r\n	<li>Meningkatkan mutu pelayanan kesehatan yang ada di desamelalui kegiatan POSYANDU.</li>\r\n	<li>Meningkatkan kemandirian masyarakat desa dalam bidang kesehatan melalui pembinaan dengan dinas terkait.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Untuk mencapai misi 4, yaitu meningkatkan sarana dan prasarana pertanian yang akan dilakukan adalah sebagai berikut:</li>\r\n</ul>\r\n\r\n<ul style=\"list-style-type:circle\">\r\n	<li>Pembangunan dan pemliharaan sarana saluran irigasi desa&nbsp;</li>\r\n	<li>Pembangunan dan pemliharaan sarana jalan usaha tani desa</li>\r\n</ul>\r\n\r\n<p>&bull; Untuk mencapai misi 5, yaitu Peningkatan saran dan pasarana pemerintahan desa dan lembaga desa&nbsp; yang akan di laksanakan sebagai berikut:&nbsp;</p>\r\n\r\n<ul style=\"list-style-type:circle\">\r\n	<li>Pembangunan dan pemeliharaan sarana dan prasarana pemerintahan desa.</li>\r\n	<li>Pembangunan sarana dan prasarana kelembagaan yang ada di desa.</li>\r\n	<li>Mengadakan pelatihan, pembinaan dan peningkatan SDM bagi aparatur pemerintahan desa dan Lembaga desa yang ada.</li>\r\n</ul>\r\n', 'Green_and_Black_Travel_Agency_Logo(4)1.png', 'Visi-Misi', 'Pemerintah Desa', 1, '2020-11-08 14:30:20'),
(4, 'Profil Desa', '<p>Secara Administratif Desa Bedoho merupakan salah satu desa yang berada di Kecamatan Jiwan, Kabupaten Madiun, Provinsi Jawa Timur. Wilayah Desa Bedoho memiliki batas-batas dengan wilayah sebagai berikut :<br />\r\n&nbsp;</p>\r\n\r\n<table cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"vertical-align:top; width:113px\">\r\n			<p>Sebelah Utara</p>\r\n			</td>\r\n			<td style=\"vertical-align:top; width:28px\">\r\n			<p>:</p>\r\n			</td>\r\n			<td style=\"vertical-align:top; width:248px\">\r\n			<p>Desa Bibrik</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top; width:113px\">\r\n			<p>Sebelah Selatan</p>\r\n			</td>\r\n			<td style=\"vertical-align:top; width:28px\">\r\n			<p>:</p>\r\n			</td>\r\n			<td style=\"vertical-align:top; width:248px\">\r\n			<p>Desa Ngetrep</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top; width:113px\">\r\n			<p>Sebelah Timur</p>\r\n			</td>\r\n			<td style=\"vertical-align:top; width:28px\">\r\n			<p>:</p>\r\n			</td>\r\n			<td style=\"vertical-align:top; width:248px\">\r\n			<p>Desa Ngetrep</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top; width:113px\">\r\n			<p>Sebelah Barat</p>\r\n			</td>\r\n			<td style=\"vertical-align:top; width:28px\">\r\n			<p>:</p>\r\n			</td>\r\n			<td style=\"vertical-align:top; width:248px\">\r\n			<p>Desa Sumberejo</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Desa Bedoho merupakan desa terkecil di kecamatan Jiwan, yang memiliki area seluas 107.60 Ha , dimana 70% wilayah desa ini merupakan area persawahan, dengan memiliki luas tanah sawah seluas 76.60 Hektar menjadi salah&nbsp; satu peluang bagi masyarakat setempat untuk mengembangkan Sumber Daya Alam tersebut dalam mencukupi kebutuhan ekonomi dari sektor pertanian. Terdapat 2 dusun pada desa ini yaitu Dusun Bedoho dan Dusun Gunting. Jumlah penduduk Desa Bedoho per Desember 2019 sejumlah 1158 jiwa, dengan 80% masyarakatnya menggantungkan kecukupan ekonomi dari sektor pertanian</p>\r\n', 'WhatsApp_Image_2020-11-28_at_22_28_182.jpeg', 'Profil-Desa', 'Admin', 1, '2020-11-08 14:30:44'),
(6, 'Pembangunan Baligho SH Terate', '<p>Persaudaraan Setia Hati Terate (dikenal luas sebagai PSHT atau SH Terate) adalah organisasi olahraga yang diinisiasi oleh Ki Hadjar Hardjo Oetomo pada tahun 1922 dan kemudian disepakati Namanya menjadi Persaudaraan Setia Hati Terate pada kongres pertamanya di Madiun pada tahun 1948.</p>\r\n\r\n<p>PSHT merupakan organisasi pencak silat yang tergabung dan salah satu yang turut mendirikan Ikatan Pencak Silat Indonesia (IPSI) pada tanggal 18 Mei 1948.[1][2] Saat ini PSHT diikuti sekitar 7 juta anggota, memiliki cabang di 236 kabupaten/kota di Indonesia, 10 komisariat di perguruan tinggi dan 10 komisariat luar negeri di Malaysia, Belanda, Rusia (Moskwa), Timor Leste, Hongkong, Korea Selatan, Jepang, Belgia, dan Prancis.</p>\r\n\r\n<p>Pembangunan Baligho ini diharapkan Desa Bedoho dapat menjadi icon SH Terate di Madiun dan menjadi jendela Kabupaten Madiun.</p>\r\n', 'IMG_0288.jpg', 'Berita', 'Admin', 1, '2020-10-29 11:46:51'),
(7, 'Survei Data Tim Innovillage ke Desa', '<p>Tim 404NotFound Telkom University melakukan survei data secara langsung dengan menggunakan metode observasi/wawancara kepada sejumlah aparat desa Bedoho pada bulan November 2020</p>\r\n', '52c52d50-c696-4bb7-a448-fe6dfd531bf8.jpg', 'Berita', 'Admin', 1, '2020-10-29 11:46:51'),
(10, 'Tentang ', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\r\n', 'kampung_sinau.png', 'Contact-Us', 'Pemerintah Desa', 1, '2020-11-08 17:34:28'),
(11, 'Proses Pembuatan Emping Melinjo Khas Bedoho', '<p>Emping Melinjo adalah sebuah camilan atau makanan ringan Khas Indonesia yang dapat ditemui hampir di seluruh penjuru&nbsp;nusantara. Emping melinjo produksi UMKM Karibe ini&nbsp;cukup digemari masyarakat karena sangat mementingkan kualitas produk dengan tidak menggunakan bahan pengawet dalam proses&nbsp;produksi yang masih dilakukan secara tradisional agar&nbsp;mengahasilkan cita rasa yang alami</p>\r\n', 'IMG_0748.jpg', 'Berita', 'Admin', 1, '2020-12-02 06:03:47'),
(12, 'Penyerahan Tanda Terima Kasih', '<p>Telah dilakukan penyerahan plakat oleh tim 404NotFound kepada pihak Desa Bedoho sebagai ucapan terimakasih atas diijinkannya melakukan impelementasi ide project dan tempat penelitian dalam kurun waktu 5 minggu</p>\r\n', 'IMG_0632.jpg', 'Berita', 'Admin', 1, '2020-12-02 06:06:08'),
(13, 'PenaNaman Padi', '<p>Bedoho, merupakan salah satu desa yang terletak di Kecamatan Jiwan, sebelah barat Kabupaten Madiun, secara geografis Desa Bedoho memiliki luas tanah sawah seluas 76.60 Ha, dimana mayoritas penduduk Desa Bedoho menggantungkan kecukupan ekonomi dari sektor pertanian.</p>\r\n', 'IMG_0744.jpg', 'Berita', 'Admin', 1, '2020-12-02 06:14:27');

-- --------------------------------------------------------

--
-- Table structure for table `email_sents`
--

CREATE TABLE `email_sents` (
  `id_email_sent` int(11) NOT NULL,
  `id_transaksi` int(11) DEFAULT NULL,
  `email_from` varchar(255) NOT NULL,
  `email_to` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `etikets`
--

CREATE TABLE `etikets` (
  `id_etiket` int(11) NOT NULL,
  `id_produk_orders` int(11) NOT NULL,
  `Nama_etiket` varchar(255) NOT NULL,
  `kodetiket` varchar(255) NOT NULL,
  `etiket` varchar(255) NOT NULL,
  `booking` date NOT NULL,
  `expired` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `etikets`
--

INSERT INTO `etikets` (`id_etiket`, `id_produk_orders`, `Nama_etiket`, `kodetiket`, `etiket`, `booking`, `expired`, `date`) VALUES
(1, 6, 'Oliver Sykes', '4620201107172112', 'Tiket Oliver Sykes-4-6-202011071721121', '2020-11-21', 0, '2020-11-07 10:21:12'),
(2, 10, 'Oliver Sykes', '41020201107172112', 'Tiket Oliver Sykes-4-10-202011071721122', '2020-11-21', 0, '2020-11-07 10:21:12'),
(4, 12, 'Oliver Sykes', '71220201108135136', 'Tiket Oliver Sykes-7-12-202011081351361', '2020-11-25', 0, '2020-11-08 06:51:36'),
(5, 15, 'Polkana Sociollita', '91520201109220833', 'Tiket Polkana Sociollita-9-15-202011092208331', '2020-11-29', 0, '2020-11-09 15:08:33'),
(8, 11, 'Oliver Sykes', '61120201114102614', 'Tiket Oliver Sykes-6-11-202011141026141', '2020-11-25', 0, '2020-11-14 03:26:14'),
(9, 11, 'Oliver Sykes', '61120201114102614', 'Tiket Oliver Sykes-6-11-202011141026142', '2020-11-25', 0, '2020-11-14 03:26:14'),
(10, 19, 'nova hanafi', '121920201128222913', 'Tiket nova hanafi-12-19-202011282229131', '2020-11-29', 0, '2020-11-28 15:29:13'),
(11, 20, 'nova hanafi', '132020201129010720', 'Tiket nova hanafi-13-20-202011290107201', '2020-11-29', 0, '2020-11-28 18:07:20'),
(12, 22, 'Shakilla Faridhotus Aulia Ramadhan', '152220201129023801', 'Tiket Shakilla Faridhotus Aulia Ramadhan-15-22-202011290238011', '2020-11-30', 0, '2020-11-28 19:38:01'),
(13, 18, 'nova hanafi', '111820201129023819', 'Tiket nova hanafi-11-18-202011290238191', '2020-11-29', 0, '2020-11-28 19:38:19'),
(14, 24, 'nova hanafi', '172420201129025729', 'Tiket nova hanafi-17-24-202011290257291', '2020-11-30', 0, '2020-11-28 19:57:29'),
(15, 24, 'nova hanafi', '172420201129025729', 'Tiket nova hanafi-17-24-202011290257292', '2020-11-30', 0, '2020-11-28 19:57:29'),
(16, 24, 'nova hanafi', '172420201129025729', 'Tiket nova hanafi-17-24-202011290257293', '2020-11-30', 0, '2020-11-28 19:57:29'),
(17, 21, 'Jumahid Sykes', '142120201129025810', 'Tiket Jumahid Sykes-14-21-202011290258101', '2020-11-30', 0, '2020-11-28 19:58:10'),
(18, 28, 'nova hanafi', '212820201129231419', 'Tiket nova hanafi-21-28-202011292314191', '2020-12-01', 0, '2020-11-29 16:14:19'),
(19, 34, 'shakillaaaa', '273420210407161848', 'Tiket shakillaaaa-27-34-202104071618481', '2021-03-09', 0, '2021-04-07 09:18:48'),
(20, 37, 'Shakilla Faridhotus Aulia Ramadhan', '303720210407202413', 'Tiket Shakilla Faridhotus Aulia Ramadhan-30-37-202104072024131', '2021-04-14', 0, '2021-04-07 13:24:13'),
(21, 38, 'shakillaaaa', '313820210412204932', 'Tiket shakillaaaa-31-38-202104122049321', '2021-04-15', 0, '2021-04-12 13:49:32'),
(22, 40, 'Muhammad Dahana', '334020210414000514', 'Tiket Muhammad Dahana-33-40-202104140005141', '2021-04-14', 0, '2021-04-13 17:05:14'),
(23, 40, 'Muhammad Dahana', '334020210414000514', 'Tiket Muhammad Dahana-33-40-202104140005142', '2021-04-14', 0, '2021-04-13 17:05:14'),
(24, 46, 'Christine Yenny', '434620210707083812', 'Tiket Christine Yenny-43-46-202107070838121', '2021-07-06', 0, '2021-07-07 01:38:12');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id_event` int(11) NOT NULL,
  `event` varchar(255) NOT NULL,
  `tanggal_mulai` datetime NOT NULL,
  `tanggal_selesai` datetime NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `kuota` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id_event`, `event`, `tanggal_mulai`, `tanggal_selesai`, `gambar`, `keterangan`, `kuota`, `status`, `created_at`) VALUES
(7, 'Gotong Royong Bersih Desa', '2020-12-09 18:00:00', '2020-12-11 23:59:00', 'WhatsApp_Image_2020-12-02_at_10_40_43_(1).jpeg', '<p>Bersih Desa adalah acara tahunan yang selalu diselenggarakan oleh aparat desa Bedoho untuk mempererat tali silaturahmi antar warga</p>\r\n', 1000, 1, '2020-10-29 08:59:09'),
(8, 'Pagelaran Kesenian dan Wayang Kulit Virtual Memperingati Hari Pahlawan Dan Hari Wayang 2019', '2019-11-13 17:26:00', '2019-11-15 23:26:00', 'wayang.jpg', '<p>Memasuki akhir tahun seharusnya banyak event yang melibatkan para pelaku seni. &nbsp;Dan dibulan ini ada peristiwa sejarah yaitu Hari Pahlawan dan Hari Wayang Nasional.&nbsp;Menyikapi hal tersebut Aparat Desa Bedoho memfasilitasi kegiatan seni secara virtual yang akan diselenggarakan mulai tanggal 13 - 15 November 2019. Kegiatan pertama akan menyuguhkan atraksi barongsai Sambi Goak dari Banjaratma yang sarat prestasi sampai tingkat nasional. Disusul &nbsp;Tari Genjring Akrobatik Mulya Sejati dari Sigempol Randusanga yang merupakan seni tradisi lokal setempat. &nbsp;Dan ditutup dengan penampilan sendratasik Teater Kembang&nbsp;</p>\r\n', 100, 1, '2020-12-02 10:26:23');

-- --------------------------------------------------------

--
-- Table structure for table `galeries`
--

CREATE TABLE `galeries` (
  `id_galeri` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `date` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `galeries`
--

INSERT INTO `galeries` (`id_galeri`, `judul`, `gambar`, `date`) VALUES
(15, 'Proses Pembuatan Emping Melinjo Khas Bedoho', 'IMG_0748.jpg', '2020-12-02 06:09:49'),
(16, 'Penyerahan Tanda Terima Kasih', 'IMG_0632.jpg', '2020-12-02 06:10:04'),
(17, 'Tim 404NotFound bersama aparat desa Bedoho', '52c52d50-c696-4bb7-a448-fe6dfd531bf8.jpg', '2020-12-02 06:10:35'),
(18, 'Pembangunan pondok pesantren Desa Bedoho', 'IMG_9310.jpg', '2020-12-02 06:11:09'),
(19, 'Pembangunan Baligho SH Terate', 'IMG_0288.jpg', '2020-12-02 06:11:38'),
(20, 'PenaNaman Padi', 'IMG_0744.jpg', '2020-12-02 06:13:37'),
(21, 'Belajar Daring Menggunakan Smart Wifi Mitra Priyangga', 'WhatsApp_Image_2020-12-02_at_13_45_18.jpeg', '2020-12-02 06:47:34');

-- --------------------------------------------------------

--
-- Table structure for table `kabupatens`
--

CREATE TABLE `kabupatens` (
  `id_kab` varchar(4) NOT NULL,
  `provinsi_id` varchar(2) NOT NULL DEFAULT '',
  `Nama_kab` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kabupatens`
--

INSERT INTO `kabupatens` (`id_kab`, `provinsi_id`, `Nama_kab`) VALUES
('1101', '11', 'Kab. Simeulue'),
('1102', '11', 'Kab. Aceh Singkil'),
('1103', '11', 'Kab. Aceh Selatan'),
('1104', '11', 'Kab. Aceh Tenggara'),
('1105', '11', 'Kab. Aceh Timur'),
('1106', '11', 'Kab. Aceh Tengah'),
('1107', '11', 'Kab. Aceh Barat'),
('1108', '11', 'Kab. Aceh Besar'),
('1109', '11', 'Kab. Pidie'),
('1110', '11', 'Kab. Bireuen'),
('1111', '11', 'Kab. Aceh Utara'),
('1112', '11', 'Kab. Aceh Barat Daya'),
('1113', '11', 'Kab. Gayo Lues'),
('1114', '11', 'Kab. Aceh Tamiang'),
('1115', '11', 'Kab. Nagan Raya'),
('1116', '11', 'Kab. Aceh Jaya'),
('1117', '11', 'Kab. Bener Meriah'),
('1118', '11', 'Kab. Pidie Jaya'),
('1171', '11', 'Kota Banda Aceh'),
('1172', '11', 'Kota Sabang'),
('1173', '11', 'Kota Langsa'),
('1174', '11', 'Kota Lhokseumawe'),
('1175', '11', 'Kota Subulussalam'),
('1201', '12', 'Kab. Nias'),
('1202', '12', 'Kab. Mandailing Natal'),
('1203', '12', 'Kab. Tapanuli Selatan'),
('1204', '12', 'Kab. Tapanuli Tengah'),
('1205', '12', 'Kab. Tapanuli Utara'),
('1206', '12', 'Kab. Toba Samosir'),
('1207', '12', 'Kab. Labuhan Batu'),
('1208', '12', 'Kab. Asahan'),
('1209', '12', 'Kab. Simalungun'),
('1210', '12', 'Kab. Dairi'),
('1211', '12', 'Kab. Karo'),
('1212', '12', 'Kab. Deli Serdang'),
('1213', '12', 'Kab. Langkat'),
('1214', '12', 'Kab. Nias Selatan'),
('1215', '12', 'Kab. Humbang Hasundutan'),
('1216', '12', 'Kab. Pakpak Bharat'),
('1217', '12', 'Kab. Samosir'),
('1218', '12', 'Kab. Serdang Bedagai'),
('1219', '12', 'Kab. Batu Bara'),
('1220', '12', 'Kab. Padang Lawas Utara'),
('1221', '12', 'Kab. Padang Lawas'),
('1222', '12', 'Kab. Labuhan Batu Selatan'),
('1223', '12', 'Kab. Labuhan Batu Utara'),
('1224', '12', 'Kab. Nias Utara'),
('1225', '12', 'Kab. Nias Barat'),
('1271', '12', 'Kota Sibolga'),
('1272', '12', 'Kota Tanjung Balai'),
('1273', '12', 'Kota Pematang Siantar'),
('1274', '12', 'Kota Tebing Tinggi'),
('1275', '12', 'Kota Medan'),
('1276', '12', 'Kota Binjai'),
('1277', '12', 'Kota Padangsidimpuan'),
('1278', '12', 'Kota Gunungsitoli'),
('1301', '13', 'Kab. Kepulauan Mentawai'),
('1302', '13', 'Kab. Pesisir Selatan'),
('1303', '13', 'Kab. Solok'),
('1304', '13', 'Kab. Sijunjung'),
('1305', '13', 'Kab. Tanah Datar'),
('1306', '13', 'Kab. Padang Pariaman'),
('1307', '13', 'Kab. Agam'),
('1308', '13', 'Kab. Lima Puluh Kota'),
('1309', '13', 'Kab. Pasaman'),
('1310', '13', 'Kab. Solok Selatan'),
('1311', '13', 'Kab. Dharmasraya'),
('1312', '13', 'Kab. Pasaman Barat'),
('1371', '13', 'Kota Padang'),
('1372', '13', 'Kota Solok'),
('1373', '13', 'Kota Sawah Lunto'),
('1374', '13', 'Kota Padang Panjang'),
('1375', '13', 'Kota Bukittinggi'),
('1376', '13', 'Kota Payakumbuh'),
('1377', '13', 'Kota Pariaman'),
('1401', '14', 'Kab. Kuantan Singingi'),
('1402', '14', 'Kab. Indragiri Hulu'),
('1403', '14', 'Kab. Indragiri Hilir'),
('1404', '14', 'Kab. Pelalawan'),
('1405', '14', 'Kab. S I A K'),
('1406', '14', 'Kab. Kampar'),
('1407', '14', 'Kab. Rokan Hulu'),
('1408', '14', 'Kab. Bengkalis'),
('1409', '14', 'Kab. Rokan Hilir'),
('1410', '14', 'Kab. Kepulauan Meranti'),
('1471', '14', 'Kota Pekanbaru'),
('1473', '14', 'Kota D U M A I'),
('1501', '15', 'Kab. Kerinci'),
('1502', '15', 'Kab. Merangin'),
('1503', '15', 'Kab. Sarolangun'),
('1504', '15', 'Kab. Batang Hari'),
('1505', '15', 'Kab. Muaro Jambi'),
('1506', '15', 'Kab. Tanjung Jabung Timur'),
('1507', '15', 'Kab. Tanjung Jabung Barat'),
('1508', '15', 'Kab. Tebo'),
('1509', '15', 'Kab. Bungo'),
('1571', '15', 'Kota Jambi'),
('1572', '15', 'Kota Sungai Penuh'),
('1601', '16', 'Kab. Ogan Komering Ulu'),
('1602', '16', 'Kab. Ogan Komering Ilir'),
('1603', '16', 'Kab. Muara Enim'),
('1604', '16', 'Kab. Lahat'),
('1605', '16', 'Kab. Musi Rawas'),
('1606', '16', 'Kab. Musi Banyuasin'),
('1607', '16', 'Kab. Banyu Asin'),
('1608', '16', 'Kab. Ogan Komering Ulu Selatan'),
('1609', '16', 'Kab. Ogan Komering Ulu Timur'),
('1610', '16', 'Kab. Ogan Ilir'),
('1611', '16', 'Kab. Empat Lawang'),
('1671', '16', 'Kota Palembang'),
('1672', '16', 'Kota Prabumulih'),
('1673', '16', 'Kota Pagar Alam'),
('1674', '16', 'Kota Lubuklinggau'),
('1701', '17', 'Kab. Bengkulu Selatan'),
('1702', '17', 'Kab. Rejang Lebong'),
('1703', '17', 'Kab. Bengkulu Utara'),
('1704', '17', 'Kab. Kaur'),
('1705', '17', 'Kab. Seluma'),
('1706', '17', 'Kab. Mukomuko'),
('1707', '17', 'Kab. Lebong'),
('1708', '17', 'Kab. Kepahiang'),
('1709', '17', 'Kab. Bengkulu Tengah'),
('1771', '17', 'Kota Bengkulu'),
('1801', '18', 'Kab. Lampung Barat'),
('1802', '18', 'Kab. Tanggamus'),
('1803', '18', 'Kab. Lampung Selatan'),
('1804', '18', 'Kab. Lampung Timur'),
('1805', '18', 'Kab. Lampung Tengah'),
('1806', '18', 'Kab. Lampung Utara'),
('1807', '18', 'Kab. Way Kanan'),
('1808', '18', 'Kab. Tulangbawang'),
('1809', '18', 'Kab. Pesawaran'),
('1810', '18', 'Kab. Pringsewu'),
('1811', '18', 'Kab. Mesuji'),
('1812', '18', 'Kab. Tulang Bawang Barat'),
('1813', '18', 'Kab. Pesisir Barat'),
('1871', '18', 'Kota Bandar Lampung'),
('1872', '18', 'Kota Metro'),
('1901', '19', 'Kab. Bangka'),
('1902', '19', 'Kab. Belitung'),
('1903', '19', 'Kab. Bangka Barat'),
('1904', '19', 'Kab. Bangka Tengah'),
('1905', '19', 'Kab. Bangka Selatan'),
('1906', '19', 'Kab. Belitung Timur'),
('1971', '19', 'Kota Pangkal Pinang'),
('2101', '21', 'Kab. Karimun'),
('2102', '21', 'Kab. Bintan'),
('2103', '21', 'Kab. Natuna'),
('2104', '21', 'Kab. Lingga'),
('2105', '21', 'Kab. Kepulauan Anambas'),
('2171', '21', 'Kota B A T A M'),
('2172', '21', 'Kota Tanjung Pinang'),
('3101', '31', 'Kab. Kepulauan Seribu'),
('3171', '31', 'Kota Jakarta Selatan'),
('3172', '31', 'Kota Jakarta Timur'),
('3173', '31', 'Kota Jakarta Pusat'),
('3174', '31', 'Kota Jakarta Barat'),
('3175', '31', 'Kota Jakarta Utara'),
('3201', '32', 'Kab. Bogor'),
('3202', '32', 'Kab. Sukabumi'),
('3203', '32', 'Kab. Cianjur'),
('3204', '32', 'Kab. Bandung'),
('3205', '32', 'Kab. Garut'),
('3206', '32', 'Kab. Tasikmalaya'),
('3207', '32', 'Kab. Ciamis'),
('3208', '32', 'Kab. Kuningan'),
('3209', '32', 'Kab. Cirebon'),
('3210', '32', 'Kab. Majalengka'),
('3211', '32', 'Kab. Sumedang'),
('3212', '32', 'Kab. Indramayu'),
('3213', '32', 'Kab. Subang'),
('3214', '32', 'Kab. Purwakarta'),
('3215', '32', 'Kab. Karawang'),
('3216', '32', 'Kab. Bekasi'),
('3217', '32', 'Kab. Bandung Barat'),
('3218', '32', 'Kab. Pangandaran'),
('3271', '32', 'Kota Bogor'),
('3272', '32', 'Kota Sukabumi'),
('3273', '32', 'Kota Bandung'),
('3274', '32', 'Kota Cirebon'),
('3275', '32', 'Kota Bekasi'),
('3276', '32', 'Kota Depok'),
('3277', '32', 'Kota Cimahi'),
('3278', '32', 'Kota Tasikmalaya'),
('3279', '32', 'Kota Banjar'),
('3301', '33', 'Kab. Cilacap'),
('3302', '33', 'Kab. Banyumas'),
('3303', '33', 'Kab. Purbalingga'),
('3304', '33', 'Kab. Banjarnegara'),
('3305', '33', 'Kab. Kebumen'),
('3306', '33', 'Kab. Purworejo'),
('3307', '33', 'Kab. Wonosobo'),
('3308', '33', 'Kab. Magelang'),
('3309', '33', 'Kab. Boyolali'),
('3310', '33', 'Kab. Klaten'),
('3311', '33', 'Kab. Sukoharjo'),
('3312', '33', 'Kab. Wonogiri'),
('3313', '33', 'Kab. Karanganyar'),
('3314', '33', 'Kab. Sragen'),
('3315', '33', 'Kab. Grobogan'),
('3316', '33', 'Kab. Blora'),
('3317', '33', 'Kab. Rembang'),
('3318', '33', 'Kab. Pati'),
('3319', '33', 'Kab. Kudus'),
('3320', '33', 'Kab. Jepara'),
('3321', '33', 'Kab. Demak'),
('3322', '33', 'Kab. Semarang'),
('3323', '33', 'Kab. Temanggung'),
('3324', '33', 'Kab. Kendal'),
('3325', '33', 'Kab. Batang'),
('3326', '33', 'Kab. Pekalongan'),
('3327', '33', 'Kab. Pemalang'),
('3328', '33', 'Kab. Tegal'),
('3329', '33', 'Kab. Brebes'),
('3371', '33', 'Kota Magelang'),
('3372', '33', 'Kota Surakarta'),
('3373', '33', 'Kota Salatiga'),
('3374', '33', 'Kota Semarang'),
('3375', '33', 'Kota Pekalongan'),
('3376', '33', 'Kota Tegal'),
('3401', '34', 'Kab. Kulon Progo'),
('3402', '34', 'Kab. Bantul'),
('3403', '34', 'Kab. Gunung Kidul'),
('3404', '34', 'Kab. Sleman'),
('3471', '34', 'Kota Yogyakarta'),
('3501', '35', 'Kab. Pacitan'),
('3502', '35', 'Kab. Ponorogo'),
('3503', '35', 'Kab. Trenggalek'),
('3504', '35', 'Kab. Tulungagung'),
('3505', '35', 'Kab. Blitar'),
('3506', '35', 'Kab. Kediri'),
('3507', '35', 'Kab. Malang'),
('3508', '35', 'Kab. Lumajang'),
('3509', '35', 'Kab. Jember'),
('3510', '35', 'Kab. Banyuwangi'),
('3511', '35', 'Kab. Bondowoso'),
('3512', '35', 'Kab. Situbondo'),
('3513', '35', 'Kab. Probolinggo'),
('3514', '35', 'Kab. Pasuruan'),
('3515', '35', 'Kab. Sidoarjo'),
('3516', '35', 'Kab. Mojokerto'),
('3517', '35', 'Kab. Jombang'),
('3518', '35', 'Kab. Nganjuk'),
('3519', '35', 'Kab. Madiun'),
('3520', '35', 'Kab. Magetan'),
('3521', '35', 'Kab. Ngawi'),
('3522', '35', 'Kab. Bojonegoro'),
('3523', '35', 'Kab. Tuban'),
('3524', '35', 'Kab. Lamongan'),
('3525', '35', 'Kab. Gresik'),
('3526', '35', 'Kab. Bangkalan'),
('3527', '35', 'Kab. Sampang'),
('3528', '35', 'Kab. Pamekasan'),
('3529', '35', 'Kab. Sumenep'),
('3571', '35', 'Kota Kediri'),
('3572', '35', 'Kota Blitar'),
('3573', '35', 'Kota Malang'),
('3574', '35', 'Kota Probolinggo'),
('3575', '35', 'Kota Pasuruan'),
('3576', '35', 'Kota Mojokerto'),
('3577', '35', 'Kota Madiun'),
('3578', '35', 'Kota Surabaya'),
('3579', '35', 'Kota Batu'),
('3601', '36', 'Kab. Pandeglang'),
('3602', '36', 'Kab. Lebak'),
('3603', '36', 'Kab. Tangerang'),
('3604', '36', 'Kab. Serang'),
('3671', '36', 'Kota Tangerang'),
('3672', '36', 'Kota Cilegon'),
('3673', '36', 'Kota Serang'),
('3674', '36', 'Kota Tangerang Selatan'),
('5101', '51', 'Kab. Jembrana'),
('5102', '51', 'Kab. Tabanan'),
('5103', '51', 'Kab. Badung'),
('5104', '51', 'Kab. Gianyar'),
('5105', '51', 'Kab. Klungkung'),
('5106', '51', 'Kab. Bangli'),
('5107', '51', 'Kab. Karang Asem'),
('5108', '51', 'Kab. Buleleng'),
('5171', '51', 'Kota Denpasar'),
('5201', '52', 'Kab. Lombok Barat'),
('5202', '52', 'Kab. Lombok Tengah'),
('5203', '52', 'Kab. Lombok Timur'),
('5204', '52', 'Kab. Sumbawa'),
('5205', '52', 'Kab. Dompu'),
('5206', '52', 'Kab. Bima'),
('5207', '52', 'Kab. Sumbawa Barat'),
('5208', '52', 'Kab. Lombok Utara'),
('5271', '52', 'Kota Mataram'),
('5272', '52', 'Kota Bima'),
('5301', '53', 'Kab. Sumba Barat'),
('5302', '53', 'Kab. Sumba Timur'),
('5303', '53', 'Kab. Kupang'),
('5304', '53', 'Kab. Timor Tengah Selatan'),
('5305', '53', 'Kab. Timor Tengah Utara'),
('5306', '53', 'Kab. Belu'),
('5307', '53', 'Kab. Alor'),
('5308', '53', 'Kab. Lembata'),
('5309', '53', 'Kab. Flores Timur'),
('5310', '53', 'Kab. Sikka'),
('5311', '53', 'Kab. Ende'),
('5312', '53', 'Kab. Ngada'),
('5313', '53', 'Kab. Manggarai'),
('5314', '53', 'Kab. Rote Ndao'),
('5315', '53', 'Kab. Manggarai Barat'),
('5316', '53', 'Kab. Sumba Tengah'),
('5317', '53', 'Kab. Sumba Barat Daya'),
('5318', '53', 'Kab. Nagekeo'),
('5319', '53', 'Kab. Manggarai Timur'),
('5320', '53', 'Kab. Sabu Raijua'),
('5371', '53', 'Kota Kupang'),
('6101', '61', 'Kab. Sambas'),
('6102', '61', 'Kab. Bengkayang'),
('6103', '61', 'Kab. Landak'),
('6104', '61', 'Kab. Pontianak'),
('6105', '61', 'Kab. Sanggau'),
('6106', '61', 'Kab. Ketapang'),
('6107', '61', 'Kab. Sintang'),
('6108', '61', 'Kab. Kapuas Hulu'),
('6109', '61', 'Kab. Sekadau'),
('6110', '61', 'Kab. Melawi'),
('6111', '61', 'Kab. Kayong Utara'),
('6112', '61', 'Kab. Kubu Raya'),
('6171', '61', 'Kota Pontianak'),
('6172', '61', 'Kota Singkawang'),
('6201', '62', 'Kab. Kotawaringin Barat'),
('6202', '62', 'Kab. Kotawaringin Timur'),
('6203', '62', 'Kab. Kapuas'),
('6204', '62', 'Kab. Barito Selatan'),
('6205', '62', 'Kab. Barito Utara'),
('6206', '62', 'Kab. Sukamara'),
('6207', '62', 'Kab. Lamandau'),
('6208', '62', 'Kab. Seruyan'),
('6209', '62', 'Kab. Katingan'),
('6210', '62', 'Kab. Pulang Pisau'),
('6211', '62', 'Kab. Gunung Mas'),
('6212', '62', 'Kab. Barito Timur'),
('6213', '62', 'Kab. Murung Raya'),
('6271', '62', 'Kota Palangka Raya'),
('6301', '63', 'Kab. Tanah Laut'),
('6302', '63', 'Kab. Kota Baru'),
('6303', '63', 'Kab. Banjar'),
('6304', '63', 'Kab. Barito Kuala'),
('6305', '63', 'Kab. Tapin'),
('6306', '63', 'Kab. Hulu Sungai Selatan'),
('6307', '63', 'Kab. Hulu Sungai Tengah'),
('6308', '63', 'Kab. Hulu Sungai Utara'),
('6309', '63', 'Kab. Tabalong'),
('6310', '63', 'Kab. Tanah Bumbu'),
('6311', '63', 'Kab. Balangan'),
('6371', '63', 'Kota Banjarmasin'),
('6372', '63', 'Kota Banjar Baru'),
('6401', '64', 'Kab. Paser'),
('6402', '64', 'Kab. Kutai Barat'),
('6403', '64', 'Kab. Kutai Kartanegara'),
('6404', '64', 'Kab. Kutai Timur'),
('6405', '64', 'Kab. Berau'),
('6409', '64', 'Kab. Penajam Paser Utara'),
('6471', '64', 'Kota Balikpapan'),
('6472', '64', 'Kota Samarinda'),
('6474', '64', 'Kota Bontang'),
('6501', '65', 'Kab. Malinau'),
('6502', '65', 'Kab. Bulungan'),
('6503', '65', 'Kab. Tana Tidung'),
('6504', '65', 'Kab. Nunukan'),
('6571', '65', 'Kota Tarakan'),
('7101', '71', 'Kab. Bolaang Mongondow'),
('7102', '71', 'Kab. Minahasa'),
('7103', '71', 'Kab. Kepulauan Sangihe'),
('7104', '71', 'Kab. Kepulauan Talaud'),
('7105', '71', 'Kab. Minahasa Selatan'),
('7106', '71', 'Kab. Minahasa Utara'),
('7107', '71', 'Kab. Bolaang Mongondow Utara'),
('7108', '71', 'Kab. Siau Tagulandang Biaro'),
('7109', '71', 'Kab. Minahasa Tenggara'),
('7110', '71', 'Kab. Bolaang Mongondow Selatan'),
('7111', '71', 'Kab. Bolaang Mongondow Timur'),
('7171', '71', 'Kota Manado'),
('7172', '71', 'Kota Bitung'),
('7173', '71', 'Kota Tomohon'),
('7174', '71', 'Kota Kotamobagu'),
('7201', '72', 'Kab. Banggai Kepulauan'),
('7202', '72', 'Kab. Banggai'),
('7203', '72', 'Kab. Morowali'),
('7204', '72', 'Kab. Poso'),
('7205', '72', 'Kab. Donggala'),
('7206', '72', 'Kab. Toli-toli'),
('7207', '72', 'Kab. Buol'),
('7208', '72', 'Kab. Parigi Moutong'),
('7209', '72', 'Kab. Tojo Una-una'),
('7210', '72', 'Kab. Sigi'),
('7271', '72', 'Kota Palu'),
('7301', '73', 'Kab. Kepulauan Selayar'),
('7302', '73', 'Kab. Bulukumba'),
('7303', '73', 'Kab. Bantaeng'),
('7304', '73', 'Kab. Jeneponto'),
('7305', '73', 'Kab. Takalar'),
('7306', '73', 'Kab. Gowa'),
('7307', '73', 'Kab. Sinjai'),
('7308', '73', 'Kab. Maros'),
('7309', '73', 'Kab. Pangkajene Dan Kepulauan'),
('7310', '73', 'Kab. Barru'),
('7311', '73', 'Kab. Bone'),
('7312', '73', 'Kab. Soppeng'),
('7313', '73', 'Kab. Wajo'),
('7314', '73', 'Kab. Sidenreng Rappang'),
('7315', '73', 'Kab. Pinrang'),
('7316', '73', 'Kab. Enrekang'),
('7317', '73', 'Kab. Luwu'),
('7318', '73', 'Kab. Tana Toraja'),
('7322', '73', 'Kab. Luwu Utara'),
('7325', '73', 'Kab. Luwu Timur'),
('7326', '73', 'Kab. Toraja Utara'),
('7371', '73', 'Kota Makassar'),
('7372', '73', 'Kota Parepare'),
('7373', '73', 'Kota Palopo'),
('7401', '74', 'Kab. Buton'),
('7402', '74', 'Kab. Muna'),
('7403', '74', 'Kab. Konawe'),
('7404', '74', 'Kab. Kolaka'),
('7405', '74', 'Kab. Konawe Selatan'),
('7406', '74', 'Kab. Bombana'),
('7407', '74', 'Kab. Wakatobi'),
('7408', '74', 'Kab. Kolaka Utara'),
('7409', '74', 'Kab. Buton Utara'),
('7410', '74', 'Kab. Konawe Utara'),
('7471', '74', 'Kota Kendari'),
('7472', '74', 'Kota Baubau'),
('7501', '75', 'Kab. Boalemo'),
('7502', '75', 'Kab. Gorontalo'),
('7503', '75', 'Kab. Pohuwato'),
('7504', '75', 'Kab. Bone Bolango'),
('7505', '75', 'Kab. Gorontalo Utara'),
('7571', '75', 'Kota Gorontalo'),
('7601', '76', 'Kab. Majene'),
('7602', '76', 'Kab. Polewali Mandar'),
('7603', '76', 'Kab. Mamasa'),
('7604', '76', 'Kab. Mamuju'),
('7605', '76', 'Kab. Mamuju Utara'),
('8101', '81', 'Kab. Maluku Tenggara Barat'),
('8102', '81', 'Kab. Maluku Tenggara'),
('8103', '81', 'Kab. Maluku Tengah'),
('8104', '81', 'Kab. Buru'),
('8105', '81', 'Kab. Kepulauan Aru'),
('8106', '81', 'Kab. Seram Bagian Barat'),
('8107', '81', 'Kab. Seram Bagian Timur'),
('8108', '81', 'Kab. Maluku Barat Daya'),
('8109', '81', 'Kab. Buru Selatan'),
('8171', '81', 'Kota Ambon'),
('8172', '81', 'Kota Tual'),
('8201', '82', 'Kab. Halmahera Barat'),
('8202', '82', 'Kab. Halmahera Tengah'),
('8203', '82', 'Kab. Kepulauan Sula'),
('8204', '82', 'Kab. Halmahera Selatan'),
('8205', '82', 'Kab. Halmahera Utara'),
('8206', '82', 'Kab. Halmahera Timur'),
('8207', '82', 'Kab. Pulau Morotai'),
('8271', '82', 'Kota Ternate'),
('8272', '82', 'Kota Tidore Kepulauan'),
('9101', '91', 'Kab. Fakfak'),
('9102', '91', 'Kab. Kaimana'),
('9103', '91', 'Kab. Teluk Wondama'),
('9104', '91', 'Kab. Teluk Bintuni'),
('9105', '91', 'Kab. Manokwari'),
('9106', '91', 'Kab. Sorong Selatan'),
('9107', '91', 'Kab. Sorong'),
('9108', '91', 'Kab. Raja Ampat'),
('9109', '91', 'Kab. Tambrauw'),
('9110', '91', 'Kab. Maybrat'),
('9171', '91', 'Kota Sorong'),
('9401', '94', 'Kab. Merauke'),
('9402', '94', 'Kab. Jayawijaya'),
('9403', '94', 'Kab. Jayapura'),
('9404', '94', 'Kab. Nabire'),
('9408', '94', 'Kab. Kepulauan Yapen'),
('9409', '94', 'Kab. Biak Numfor'),
('9410', '94', 'Kab. Paniai'),
('9411', '94', 'Kab. Puncak Jaya'),
('9412', '94', 'Kab. Mimika'),
('9413', '94', 'Kab. Boven Digoel'),
('9414', '94', 'Kab. Mappi'),
('9415', '94', 'Kab. Asmat'),
('9416', '94', 'Kab. Yahukimo'),
('9417', '94', 'Kab. Pegunungan Bintang'),
('9418', '94', 'Kab. Tolikara'),
('9419', '94', 'Kab. Sarmi'),
('9420', '94', 'Kab. Keerom'),
('9426', '94', 'Kab. Waropen'),
('9427', '94', 'Kab. Supiori'),
('9428', '94', 'Kab. Mamberamo Raya'),
('9429', '94', 'Kab. Nduga'),
('9430', '94', 'Kab. Lanny Jaya'),
('9431', '94', 'Kab. Mamberamo Tengah'),
('9432', '94', 'Kab. Yalimo'),
('9433', '94', 'Kab. Puncak'),
('9434', '94', 'Kab. Dogiyai'),
('9435', '94', 'Kab. Intan Jaya'),
('9436', '94', 'Kab. Deiyai'),
('9471', '94', 'Kota Jayapura');

-- --------------------------------------------------------

--
-- Table structure for table `kegiatans`
--

CREATE TABLE `kegiatans` (
  `id_kegiatan` int(11) NOT NULL,
  `Nama_kegiatan` varchar(250) NOT NULL,
  `durasi_kegiatan` varchar(250) NOT NULL,
  `deskripsi_kegiatan` text NOT NULL,
  `harga_kegiatan` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kegiatans`
--

INSERT INTO `kegiatans` (`id_kegiatan`, `Nama_kegiatan`, `durasi_kegiatan`, `deskripsi_kegiatan`, `harga_kegiatan`) VALUES
(1, 'Belajar Mengelola Tanah ', '25 Menit', '<p>Anda dapat berwisata sambil belajar tentang tipologi lahan yaitu lahan sawah, lahan kerinf, formulasi pupuk, pembenahan tanah, tes kit dan diseminasi hasil pembibitan padi dengan seru hanya di Kampung SInau.</p>\r\n', 8000),
(3, 'Belajar Menanam Padi ', '25 Menit', '<p>Desa Wisata Kampung Sinau sangat cocok untuk wisata edukasi khususnya bagi anak-anak. Tak hanya materi saja, anak-anak juga akan diajak praktik langsung menanam padi di sawah yang telah dikemas oleh Desa Wisata Kampung Sinau kedalam konsep sawah <em>indoor. </em>Ini juga mengajarkan anak untuk lebih dekat dengan alam dan tak takut kotor untuk belajar.</p>\r\n', 8000),
(4, 'Belajar Menanam Hidroponik ', '35 Menit', '<p>Aparat Desa Bedoho telah menyulap lahan seluas 600 meter persegi menjadi green house yang di dalamnya terdapat ribuan lubang tanam hidroponik. Anda dapat mendapatkan pengetahuan sekaligus cara menanam hidroponik dengan benar di Kampung Sinau. Tak hanya asyik untuk belajar dan berwisata, Desa Wisata Kampung Sinau juga sangat cocok bagi wisatawan untuk berburu foto.</p>\r\n', 15000),
(5, 'Belajar Budaya Jawa Timur ', '1 Jam', '<p>Desa Wisata Kampung Sinau menyuguhkan kehidupan sehari-hari masyarakat desa setempat sebagai sarana belajar budaya Jawa Timur. Berbagai hal, terutama kebiasan dan adat istiadat warga setempat dapat anda pelajari. Seperti Pertunjukan Reog Ponorogo saat acara Grebeg Suro, Pagelaran Wayang Kulit yang rutin dilaksanakan pada HUT RI dan masih banyak lainnya. Selain itu, wisatawan juga dapat belajar tarian Remo pada sanggar tari yang ada pada Desa Wisata Kampung Sinau Bedoho.</p>\r\n', 10000),
(6, 'Belajar Menanam Ubi', '30 Menit', '<p>ASD</p>\r\n', 5000);

-- --------------------------------------------------------

--
-- Table structure for table `keranjangs`
--

CREATE TABLE `keranjangs` (
  `id_keranjang` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp(),
  `harga_satuan` bigint(20) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `keranjangs`
--

INSERT INTO `keranjangs` (`id_keranjang`, `id_produk`, `id_member`, `tanggal`, `harga_satuan`, `jumlah`, `status`) VALUES
(7, 3, 2, '2020-11-09 15:12:58', 50000, 2, 1),
(15, 4, 1, '2020-11-28 17:22:27', 25000, 1, 1),
(17, 9, 1, '2020-11-28 17:25:35', 28500, 1, 1),
(56, 3, 10, '2021-07-07 02:08:02', 10000, 1, 1),
(61, 1, 5, '2021-07-14 12:00:25', 25000, 1, 1),
(74, 12, 8, '2021-07-16 13:52:08', 90000, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `konfigurasies`
--

CREATE TABLE `konfigurasies` (
  `id_konfigurasi` int(11) NOT NULL,
  `Nama_app` varchar(255) NOT NULL,
  `logo_app` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `embed_maps` text NOT NULL,
  `kontak_satu` varchar(15) DEFAULT NULL,
  `kontak_dua` varchar(15) DEFAULT NULL,
  `kontak_tiga` varchar(15) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `konfigurasies`
--

INSERT INTO `konfigurasies` (`id_konfigurasi`, `Nama_app`, `logo_app`, `email`, `embed_maps`, `kontak_satu`, `kontak_dua`, `kontak_tiga`, `status`, `created_at`, `update`) VALUES
(1, 'Kampung Sinau Tourism Village', 'kampung_sinau.png', 'desabedoho@gmail.com', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15819.741471614672!2d111.47373016883665!3d-7.5820145923263835!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e79bffb94c3cd2f%3A0x8ba42eaac6285753!2sBedoho%2C%20Jiwan%2C%20Madiun%2C%20East%20Java!5e0!3m2!1sen!2sid!4v1604857882120!5m2!1sen!2sid\" class=\"embed-responsive-item  frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>', '+62 857 4812 52', '', '', 1, '2020-10-29 18:11:16', '2020-11-27 20:42:16');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id_member` int(11) NOT NULL,
  `member` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `no_telpon` varchar(15) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id_member`, `member`, `tanggal_lahir`, `no_telpon`, `alamat`) VALUES
(1, 'Jumahid Sykes', '1999-02-19', '082137244888', 'Wonosari, Gunungkidul'),
(2, 'Polkana Sociollita', '0000-00-00', '08828229393003', 'Blok A Gunungkidul\r\n'),
(3, 'Mujahid Habibullah', '0000-00-00', '082137244805', ''),
(4, 'nova hanafi', '0000-00-00', '0895397630180', ''),
(5, 'Shakilla Faridhotus Aulia Ramadhan', '0000-00-00', '082132300457', ''),
(6, 'shakillaaaa', '0000-00-00', '0839382629', ''),
(7, 'Muhammad Dahana', '2021-04-14', '082231811811', ''),
(8, 'shakillaaaaaulia', '0000-00-00', '1234', ''),
(9, 'x', '0000-00-00', '123', ''),
(10, 'Christine Yenny', '0000-00-00', '08112321900', ''),
(11, 'Shakilla Fara', '0000-00-00', '082190765432', '');

-- --------------------------------------------------------

--
-- Table structure for table `produks`
--

CREATE TABLE `produks` (
  `id_produk` int(11) NOT NULL,
  `produk` varchar(255) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `harga` bigint(20) NOT NULL,
  `berat` float NOT NULL,
  `stok` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `produks`
--

INSERT INTO `produks` (`id_produk`, `produk`, `kategori`, `gambar`, `harga`, `berat`, `stok`, `status`, `keterangan`) VALUES
(1, 'Paket EduWisata Souvenir', 'Tiket', 'WhatsApp_Image_2020-11-28_at_22_28_18.jpeg', 25000, 0, 99, 1, '<p>Souvenir produk UMKM menarikk</p>\r\n'),
(2, 'Paket Edu Wisata', 'Tiket', 'WhatsApp_Image_2020-11-28_at_22_28_181.jpeg', 8000, 0, 993, 1, ''),
(3, 'Keripik Tempe Karibe (Rasa Original) 200 gram', 'Produk', 'IMG_9510.jpg', 10000, 0.2, 103, 1, '<p>Keripik tempe Karibe merupakan produk yang dihasilkan oleh UMKM &nbsp;yang terletak di Desa Bedoho, Kecamatan Jiwan Kabupaten Madiun. Keripik tempe Karibe memiliki banyak penggemar karena rasanya yang gurih dan renyah.Tentunya produksi keripik tempe di Desa Bedoho sangat mementingkan kualitas dan kehigienisan produk dan tentunya menggunakan &nbsp;bahan baku tempe berkualitas dan tanpa pengawet<br />\r\n&nbsp;</p>\r\n'),
(4, 'Keripik Tempe Karibe (Rasa Original) 250gram', 'Produk', 'IMG_95102.jpg', 12500, 0.25, 89, 1, '<p>Keripik tempe Karibe merupakan produk yang dihasilkan oleh UMKM &nbsp;yang terletak di Desa Bedoho, Kecamatan Jiwan Kabupaten Madiun. Keripik tempe Karibe memiliki banyak penggemar karena rasanya yang gurih dan renyah.Tentunya produksi keripik tempe di Desa Bedoho sangat mementingkan kualitas dan kehigienisan produk dan tentunya menggunakan &nbsp;bahan baku tempe berkualitas dan tanpa pengawet</p>\r\n'),
(5, 'Keripik Tempe Karibe (Rasa Original) 500 gram', 'Produk', 'IMG_95101.jpg', 25000, 0.5, 100, 1, '<p>Keripik tempe Karibe merupakan produk yang dihasilkan oleh UMKM &nbsp;yang terletak di Desa Bedoho, Kecamatan Jiwan Kabupaten Madiun. Keripik tempe Karibe memiliki banyak penggemar karena rasanya yang gurih dan renyah.Tentunya produksi keripik tempe di Desa Bedoho sangat mementingkan kualitas dan kehigienisan produk dan tentunya menggunakan &nbsp;bahan baku tempe berkualitas dan tanpa pengawet<br />\r\n&nbsp;</p>\r\n'),
(6, 'Donat Kentang', 'Produk', 'IMG_20201126_173220.jpg', 18000, 0.5, 100, 1, '<p>Donat yang empuk dan nikmat&nbsp;cocok untuk&nbsp;cemilan bersama keluarga atau teman tanpa bahan pengawet. 1 box isi 8</p>\r\n'),
(7, 'Ayam panggang Kecil', 'Produk', 'aa.png', 40000, 0.65, 100, 1, '<p>Ayam Panggang Khas Bedoho utuh 1 ekor dilengkapi dengan sambal bawang dan sambal tomat &nbsp;Porsi: 4-6 orang Berat bersih ayam: +/- 630 gram. Bumbu rempah yang kaya akan rasa, tanpa bahan pengawet, tanpa tambahan micin</p>\r\n'),
(10, 'Emping Melinjo Karibe 1 KG', 'Produk', 'image_2020-11-30_085816.png', 60000, 1, 100, 1, '<p>Emping Melinjo ini cukup digemari masyarakat karena sangat mementingkan kualitas produk dengan tidak menggunakan bahan pengawet dalam proses produksi yang masih dilakukan secara tradisional agar menghasilkan cita rasa yang alami.&nbsp;</p>\r\n'),
(11, 'Ayam panggang Sedang', 'Produk', 'aa.png', 70000, 0.65, 100, 1, '<p>Ayam Panggang Khas Bedoho utuh 1 ekor dilengkapi dengan sambal bawang dan sambal tomat &nbsp;Porsi: 4-6 orang Berat bersih ayam: +/- 630 gram. Bumbu rempah yang kaya akan rasa, tanpa bahan pengawet, tanpa tambahan micin</p>\r\n'),
(12, 'Ayam panggang Besar', 'Produk', 'aa.png', 90000, 0.65, 100, 1, '<p>Ayam Panggang Khas Bedoho utuh 1 ekor dilengkapi dengan sambal bawang dan sambal tomat &nbsp;Porsi: 4-6 orang Berat bersih ayam: +/- 630 gram. Bumbu rempah yang kaya akan rasa, tanpa bahan pengawet, tanpa tambahan micin</p>\r\n'),
(13, 'Paket Pilihan', 'Tiket', 'WhatsApp_Image_2020-11-28_at_22_28_181.jpeg', 0, 0, 993, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `produk_orders`
--

CREATE TABLE `produk_orders` (
  `id_produk_order` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `produk_orders`
--

INSERT INTO `produk_orders` (`id_produk_order`, `id_transaksi`, `id_produk`, `kategori`, `jumlah`, `date`) VALUES
(1, 1, 3, 'Produk', 1, '2020-11-06 22:11:10'),
(2, 1, 4, 'Produk', 1, '2020-11-06 22:11:10'),
(5, 3, 4, 'Produk', 1, '2020-11-07 08:20:27'),
(6, 4, 1, 'Tiket', 1, '2020-11-07 08:20:54'),
(7, 5, 1, 'Tiket', 1, '2020-11-07 08:21:58'),
(8, 5, 3, 'Produk', 1, '2020-11-07 08:21:58'),
(9, 5, 4, 'Produk', 1, '2020-11-07 08:21:58'),
(10, 4, 2, 'Tiket', 1, '2020-11-07 08:20:54'),
(11, 6, 1, 'Tiket', 2, '2020-11-07 19:21:14'),
(12, 7, 1, 'Tiket', 2, '2020-11-08 06:31:42'),
(13, 8, 3, 'Produk', 1, '2020-11-09 14:58:15'),
(14, 8, 4, 'Produk', 1, '2020-11-09 14:58:15'),
(15, 9, 2, 'Tiket', 1, '2020-11-09 15:01:06'),
(16, 9, 4, 'Produk', 1, '2020-11-09 15:01:06'),
(17, 10, 1, 'Tiket', 1, '2020-11-27 19:10:46'),
(18, 11, 2, 'Tiket', 1, '2020-11-28 15:14:11'),
(19, 12, 2, 'Tiket', 1, '2020-11-28 15:20:17'),
(20, 13, 2, 'Tiket', 1, '2020-11-28 17:59:49'),
(21, 14, 1, 'Tiket', 1, '2020-11-28 18:05:06'),
(22, 15, 2, 'Tiket', 1, '2020-11-28 18:07:10'),
(23, 16, 4, 'Produk', 1, '2020-11-28 18:11:07'),
(24, 17, 1, 'Tiket', 3, '2020-11-28 19:56:43'),
(25, 18, 2, 'Tiket', 1, '2020-11-28 20:02:23'),
(26, 19, 4, 'Produk', 1, '2020-11-29 15:36:29'),
(27, 20, 3, 'Produk', 1, '2020-11-29 16:02:11'),
(28, 21, 1, 'Tiket', 1, '2020-11-29 16:11:22'),
(29, 22, 3, 'Produk', 1, '2020-11-29 17:26:39'),
(30, 23, 1, 'Tiket', 1, '2020-11-30 14:16:54'),
(31, 24, 1, 'Tiket', 3, '2020-12-02 03:40:16'),
(32, 25, 4, 'Produk', 1, '2020-12-02 10:39:50'),
(33, 26, 1, 'Tiket', 1, '2020-12-13 14:58:13'),
(34, 27, 1, 'Tiket', 1, '2021-04-07 09:17:52'),
(35, 28, 2, 'Tiket', 1, '2021-04-07 09:34:42'),
(36, 29, 3, 'Produk', 2, '2021-04-07 09:46:35'),
(37, 30, 1, 'Tiket', 1, '2021-04-07 13:23:11'),
(38, 31, 2, 'Tiket', 1, '2021-04-12 11:52:47'),
(39, 32, 1, 'Tiket', 2, '2021-04-13 04:18:33'),
(40, 33, 1, 'Tiket', 2, '2021-04-13 17:02:12'),
(41, 34, 1, 'Tiket', 2, '2021-07-05 08:39:59'),
(42, 37, 1, 'Tiket', 1, '2021-07-06 07:53:33'),
(43, 40, 12, 'Produk', 2, '2021-07-06 14:00:38'),
(44, 41, 4, 'Produk', 1, '2021-07-07 00:47:32'),
(45, 42, 4, 'Produk', 1, '2021-07-07 01:06:15'),
(46, 43, 1, 'Tiket', 1, '2021-07-07 01:34:56'),
(47, 44, 3, 'Produk', 2, '2021-07-07 01:41:20'),
(48, 46, 1, 'Tiket', 2, '2021-07-07 02:02:11'),
(49, 47, 1, 'Tiket', 1, '2021-07-07 02:04:26'),
(50, 48, 4, 'Produk', 1, '2021-07-07 02:14:21'),
(51, 49, 4, 'Produk', 1, '2021-07-14 11:53:35');

-- --------------------------------------------------------

--
-- Table structure for table `provinsies`
--

CREATE TABLE `provinsies` (
  `id_prov` varchar(2) NOT NULL,
  `Nama_prov` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `provinsies`
--

INSERT INTO `provinsies` (`id_prov`, `Nama_prov`) VALUES
('11', 'Aceh'),
('12', 'Sumatera Utara'),
('13', 'Sumatera Barat'),
('14', 'Riau'),
('15', 'Jambi'),
('16', 'Sumatera Selatan'),
('17', 'Bengkulu'),
('18', 'Lampung'),
('19', 'Kepulauan Bangka Belitung'),
('21', 'Kepulauan Riau'),
('31', 'Dki Jakarta'),
('32', 'Jawa Barat'),
('33', 'Jawa Tengah'),
('34', 'Di Yogyakarta'),
('35', 'Jawa Timur'),
('36', 'Banten'),
('51', 'Bali'),
('52', 'Nusa Tenggara Barat'),
('53', 'Nusa Tenggara Timur'),
('61', 'Kalimantan Barat'),
('62', 'Kalimantan Tengah'),
('63', 'Kalimantan Selatan'),
('64', 'Kalimantan Timur'),
('65', 'Kalimantan Utara'),
('71', 'Sulawesi Utara'),
('72', 'Sulawesi Tengah'),
('73', 'Sulawesi Selatan'),
('74', 'Sulawesi Tenggara'),
('75', 'Gorontalo'),
('76', 'Sulawesi Barat'),
('81', 'Maluku'),
('82', 'Maluku Utara'),
('91', 'Papua Barat'),
('94', 'Papua');

-- --------------------------------------------------------

--
-- Table structure for table `rekenings`
--

CREATE TABLE `rekenings` (
  `id_rekening` int(11) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `no_rekening` varchar(50) NOT NULL,
  `Nama_rekening` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rekenings`
--

INSERT INTO `rekenings` (`id_rekening`, `bank`, `no_rekening`, `Nama_rekening`, `status`) VALUES
(2, 'BRI ', '388401030685538', 'BUMDES MITRA PRIYANGGA', 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaksies`
--

CREATE TABLE `transaksies` (
  `id_transaksi` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `id_rekening` int(11) NOT NULL,
  `tanggal_booking` date DEFAULT NULL,
  `penerima` varchar(255) DEFAULT NULL,
  `notelponpenerima` varchar(20) DEFAULT NULL,
  `provinsi_penerima` varchar(255) DEFAULT NULL,
  `kabupaten_penerima` varchar(255) DEFAULT NULL,
  `kurir` varchar(255) DEFAULT NULL,
  `paket_kurir` varchar(255) DEFAULT NULL,
  `kodepos_penerima` varchar(10) DEFAULT NULL,
  `alamat_penerima` text DEFAULT NULL,
  `berattotal` float DEFAULT NULL,
  `totalkirim` bigint(20) DEFAULT NULL,
  `totalpesanan` bigint(20) NOT NULL,
  `totalbayar` bigint(20) NOT NULL,
  `buktibayar` varchar(255) DEFAULT NULL,
  `statusbukti` int(11) NOT NULL DEFAULT 0,
  `verifikasibukti` int(11) NOT NULL DEFAULT 0,
  `statustransaksi` int(11) NOT NULL DEFAULT 0,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaksies`
--

INSERT INTO `transaksies` (`id_transaksi`, `id_member`, `id_rekening`, `tanggal_booking`, `penerima`, `notelponpenerima`, `provinsi_penerima`, `kabupaten_penerima`, `kurir`, `paket_kurir`, `kodepos_penerima`, `alamat_penerima`, `berattotal`, `totalkirim`, `totalpesanan`, `totalbayar`, `buktibayar`, `statusbukti`, `verifikasibukti`, `statustransaksi`, `date`, `updated_at`) VALUES
(1, 1, 2, NULL, 'Oliver Sykes', '012312213123', 'Jawa Barat', 'Kabupaten Bandung', 'jne', 'OKE', '72824', 'PBB D22 Bojongsoang', 1.7, 34000, 75000, 109000, NULL, 1, 0, 3, '2020-11-06 22:11:10', '2020-11-30 03:14:35'),
(3, 1, 2, NULL, 'Oliver Sykes', '012312213123', 'Bali', 'Kabupaten Badung', 'pos', 'Paket Kilat Khusus', '12312', 'Pdasd', 1, 20000, 25000, 45000, 'Screen_Shot_2020-11-27_at_19_03_52.png', 0, 2, 3, '2020-11-07 08:20:27', '2020-11-30 03:14:39'),
(4, 1, 2, '2020-11-21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8000, 8000, 'Screen_Shot_2020-10-02_at_08_51_24.png', 1, 1, 3, '2020-11-07 08:20:54', '2020-11-30 03:14:42'),
(5, 1, 2, NULL, 'Oliver Sykes', '012312213123', 'DI Yogyakarta', 'Kabupaten Gunung Kidul', 'jne', 'OKE', '2311', 'asd as', 1.7, 32000, 83000, 115000, NULL, 0, 0, 3, '2020-11-07 08:21:58', '2020-11-29 16:55:29'),
(6, 1, 2, '2020-11-25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16000, 16000, 'Screen_Shot_2020-10-02_at_08_51_241.png', 1, 1, 0, '2020-11-07 19:21:14', '2020-11-14 03:22:00'),
(7, 1, 2, '2020-11-25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16000, 16000, 'w644.jpeg', 1, 1, 2, '2020-11-08 06:31:42', '2020-11-14 03:20:37'),
(8, 2, 2, NULL, 'Polkana Sociollita', '08828229393003', 'DI Yogyakarta', 'Kabupaten Gunung Kidul', 'jne', 'YES', '2232323', 'gunungkidul blok a', 1.7, 62000, 75000, 137000, 'logo.png', 0, 0, 0, '2020-11-09 14:58:15', '2020-11-20 12:09:45'),
(9, 2, 2, '2020-11-29', 'Polkana Sociollita', '08828229393003', 'DI Yogyakarta', 'Kabupaten Gunung Kidul', 'jne', 'YES', '2232323', 'gunungkidul blok a', 1, 31000, 55000, 86000, 'VP.png', 1, 1, 2, '2020-11-09 15:01:06', '2020-11-20 12:05:41'),
(10, 1, 2, '2020-11-30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8000, 8058, NULL, 0, 0, 3, '2020-11-27 19:10:46', '2020-11-29 16:52:44'),
(11, 4, 2, '2020-11-29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30000, 30230, 'kisspng-barbecue-chicken-roast-chicken-tandoori-chicken-grill-chicken-png-free-png-images-toppng-5b6d41d3945ee4_3045433615338869316077.png', 1, 1, 1, '2020-11-28 15:14:11', '2020-11-28 19:42:16'),
(12, 4, 2, '2020-11-29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30000, 30841, 'E68958EC-DEAC-4ED7-9F85-4086C6ABDB2F.png', 1, 1, 0, '2020-11-28 15:20:17', '2020-11-28 15:29:13'),
(13, 4, 2, '2020-11-29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30000, 30291, 'kisspng-barbecue-chicken-roast-chicken-tandoori-chicken-grill-chicken-png-free-png-images-toppng-5b6d41d3945ee4_30454336153388693160771.png', 1, 1, 0, '2020-11-28 17:59:49', '2020-11-28 18:07:20'),
(14, 1, 2, '2020-11-30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8000, 8509, 'WhatsApp_Image_2020-11-28_at_22_28_18.jpeg', 1, 1, 0, '2020-11-28 18:05:06', '2020-11-28 19:58:10'),
(15, 5, 2, '2020-11-30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30000, 30935, 'bumdes_4.jpg', 1, 1, 0, '2020-11-28 18:07:10', '2020-11-28 19:38:01'),
(16, 5, 2, NULL, 'Shakilla Faridhotus Aulia Ramadhan', '082132300457', 'Jawa Tengah', 'Kabupaten Klaten', 'jne', 'OKE', '89080', 'hikhl', 1, 16000, 25000, 41461, 'download1.jpg', 1, 0, 0, '2020-11-28 18:11:07', '2021-07-07 01:01:04'),
(17, 4, 2, '2020-11-30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24000, 24550, 'kisspng-barbecue-chicken-roast-chicken-tandoori-chicken-grill-chicken-png-free-png-images-toppng-5b6d41d3945ee4_30454336153388693160772.png', 1, 1, 0, '2020-11-28 19:56:43', '2020-11-28 19:57:29'),
(18, 4, 2, '2020-12-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30000, 30193, NULL, 0, 0, 3, '2020-11-28 20:02:23', '2021-04-07 09:19:18'),
(19, 4, 2, NULL, 'nova hanafi', '0895397630180', 'Banten', 'Kabupaten Lebak', 'tiki', 'ECO', '11', 'aaa', 1, 27000, 12500, 40296, 'png-clipart-barbecue-chicken-fried-chicken-chicken-sandwich-fried-chicken-barbecue-food.png', 1, 0, 0, '2020-11-29 15:36:29', '2020-11-29 15:37:09'),
(20, 4, 2, NULL, 'nova hanafi', '0895397630180', 'Maluku Utara', 'Kabupaten Halmahera Tengah', 'pos', 'REG', '11', 'aa', 1, 119000, 10000, 129614, 'S__24584200.jpg', 1, 1, 1, '2020-11-29 16:02:11', '2020-11-29 16:09:35'),
(21, 4, 2, '2020-12-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8000, 8746, 'S__245842001.jpg', 1, 1, 1, '2020-11-29 16:11:22', '2020-11-29 17:28:47'),
(22, 4, 2, NULL, 'nova hanafi', '0895397630180', 'Bali', 'Kabupaten Buleleng', 'jne', 'OKE', '0', 'kok', 1, 28000, 10000, 38240, NULL, 0, 0, 3, '2020-11-29 17:26:39', '2021-04-07 09:19:12'),
(23, 4, 2, '2020-12-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8000, 8672, 'S__245842002.jpg', 1, 0, 0, '2020-11-30 14:16:54', '2020-11-30 14:17:27'),
(24, 5, 2, '2020-12-09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24000, 24311, NULL, 0, 0, 3, '2020-12-02 03:40:16', '2021-04-07 09:19:05'),
(25, 5, 2, NULL, 'Shakilla Faridhotus Aulia Ramadhan', '082132300457', 'Jawa Barat', 'Kabupaten Bandung', 'jne', 'OKE', '89080', 'Bandung', 1, 17000, 12500, 30341, 'image_2020-12-02_174125.png', 1, 1, 2, '2020-12-02 10:39:50', '2021-07-04 14:18:36'),
(26, 5, 2, '2020-12-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30000, 30311, NULL, 0, 0, 3, '2020-12-13 14:58:13', '2021-04-07 09:19:01'),
(27, 6, 2, '2021-03-09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30000, 30114, 'download.png', 1, 1, 0, '2021-04-07 09:17:52', '2021-04-07 09:18:48'),
(28, 5, 2, '2021-04-09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8000, 8839, NULL, 0, 0, 3, '2021-04-07 09:34:42', '2021-04-12 13:48:12'),
(29, 5, 2, NULL, 'Shakilla Faridhotus Aulia Ramadhan', '082132300457', 'Jawa Timur', 'Kabupaten Bojonegoro', 'jne', 'OKE', '71171', 'Ds Karangsonp, Magetan', 1, 16000, 20000, 36387, NULL, 0, 0, 3, '2021-04-07 09:46:35', '2021-04-23 01:53:35'),
(30, 5, 2, '2021-04-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30000, 30488, 'fara.jpeg', 1, 1, 0, '2021-04-07 13:23:11', '2021-04-07 13:24:13'),
(31, 6, 2, '2021-04-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8000, 8346, '97820085-7df9-4477-ba99-226271f1af4f.jpg', 1, 1, 0, '2021-04-12 11:52:47', '2021-04-12 13:49:32'),
(32, 6, 2, '2021-04-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50000, 50903, NULL, 0, 0, 3, '2021-04-13 04:18:33', '2021-04-23 01:53:32'),
(33, 7, 2, '2021-04-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50000, 50745, '321310_332030583573629_1625423658_n.jpg', 1, 1, 1, '2021-04-13 17:02:12', '2021-04-20 05:59:52'),
(34, 5, 2, '2021-07-08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50000, 50159, NULL, 0, 0, 0, '2021-07-05 08:39:59', '2021-07-05 08:39:59'),
(35, 5, 2, '2021-07-08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50000, 50159, NULL, 0, 0, 0, '2021-07-05 08:41:14', '2021-07-05 08:41:14'),
(36, 5, 2, '2021-07-08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50000, 50159, 'PROSES_BISNIS_USULAN_(3).PNG', 1, 0, 0, '2021-07-05 08:41:14', '2021-07-05 08:43:27'),
(37, 8, 2, '2021-07-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25000, 25695, NULL, 0, 0, 0, '2021-07-06 07:53:33', '2021-07-06 07:53:33'),
(38, 8, 2, '2021-07-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25000, 25695, NULL, 0, 0, 0, '2021-07-06 07:54:43', '2021-07-06 07:54:43'),
(39, 8, 2, '2021-07-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25000, 25695, NULL, 0, 0, 0, '2021-07-06 07:55:28', '2021-07-06 07:55:28'),
(40, 9, 2, NULL, 'x', '123', 'Jawa Timur', 'Kabupaten Sidoarjo', 'jne', 'OKE', '1', 'x', 1.3, 7000, 180000, 0, NULL, 0, 0, 0, '2021-07-06 14:00:38', '2021-07-06 14:00:38'),
(41, 5, 2, NULL, 'Shakilla Faridhotus Aulia Ramadhan', '082132300457', 'Jawa Timur', 'Kabupaten Magetan', 'jne', 'OKE', '77126', 'Ds. Karangsono RT 13 RW 03 Kec. Barat Kab. Magetan', 1, 6000, 12500, 19047, 'IMG-20210531-WA0027.jpg', 1, 1, 1, '2021-07-07 00:47:32', '2021-07-07 00:53:00'),
(42, 10, 2, NULL, 'Christine Yenny', '08112321900', 'Jawa Barat', 'Kabupaten Bandung', 'pos', 'Paket Kilat Khusus', '72121', 'Jalan Adhyaksa 3 no. 22b, Sukapura, Dayeuhkolot, Kab. Bandung', 1, 19000, 12500, 31878, NULL, 0, 0, 0, '2021-07-07 01:06:15', '2021-07-07 01:06:15'),
(43, 10, 2, '2021-07-06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25000, 25761, 'download2.jpg', 1, 1, 0, '2021-07-07 01:34:56', '2021-07-07 01:38:12'),
(44, 10, 2, NULL, 'Christine Yenny', '08112321900', 'Jawa Barat', 'Kabupaten Bandung', 'pos', 'Paket Kilat Khusus', '71212', 'Jalan Adhyaksa 3 no. 22b, Sukapura, Bandung', 1, 19000, 20000, 39168, NULL, 0, 0, 0, '2021-07-07 01:41:20', '2021-07-07 01:41:20'),
(45, 10, 2, NULL, 'Christine Yenny', '08112321900', 'Jawa Barat', 'Kabupaten Bandung', 'pos', 'Paket Kilat Khusus', '71212', 'Jalan Adhyaksa 3 no. 22b, Sukapura, Bandung', 1, 19000, 20000, 39168, 'IMG-20210531-WA00271.jpg', 1, 1, 1, '2021-07-07 01:42:11', '2021-07-07 01:45:23'),
(46, 10, 0, '2021-07-21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50000, 50689, NULL, 0, 0, 0, '2021-07-07 02:02:11', '2021-07-07 02:02:11'),
(47, 10, 2, '2021-07-21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25000, 25287, NULL, 0, 0, 0, '2021-07-07 02:04:26', '2021-07-07 02:04:26'),
(48, 11, 2, NULL, 'Shakilla Fara', '082190765432', 'Jawa Timur', 'Kabupaten Magetan', 'jne', 'OKE', '76543', 'Desa Karangsono RT 13/03, Barat, Magetan', 1, 6000, 12500, 18866, NULL, 0, 0, 0, '2021-07-07 02:14:21', '2021-07-07 02:14:21'),
(49, 5, 2, NULL, 'Shakilla Faridhotus Aulia Ramadhan', '082132300457', 'Jawa Barat', 'Kabupaten Bandung', 'jne', 'OKE', '76543', 'Adhyaksa 3 no 22b', 1, 17000, 12500, 29995, NULL, 0, 0, 0, '2021-07-14 11:53:35', '2021-07-14 11:53:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `id_member` int(11) DEFAULT NULL,
  `id_Admin` int(11) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(60) NOT NULL,
  `avatar` varchar(255) NOT NULL DEFAULT 'user.jpg',
  `token` varchar(255) NOT NULL,
  `role` int(11) NOT NULL COMMENT '1:SA,2:A,3:member',
  `status` int(11) NOT NULL COMMENT '1:aktif,2:banned',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `id_member`, `id_Admin`, `email`, `password`, `avatar`, `token`, `role`, `status`, `created_at`) VALUES
(1, NULL, 1, 'desabedoho@gmail.com', '556f4f434585c2cb8ad5bbd51b5005ef', 'IMG_0744.jpg', '3252d89d93bfb1327e2d3aae9187b565dac6d085', 1, 1, '2020-11-03 17:04:42'),
(2, 1, NULL, 'jmhdoaoe@gmail.com', 'bf91184832ff2e9b92e94ef61ad52e53', 'Screen_Shot_2020-11-18_at_20_45_52.png', '9147936ef5bbf086f1414c68f3b668dcf1b91165', 3, 1, '2020-11-03 17:15:47'),
(3, 2, NULL, 'akukuh.panuntun@gmail.com', '3fc0a7acf087f549ac2b266baf94b8b1', 'user.jpg', 'd7a5a2c74796a5f770af1169ff2ecb88546299d6', 3, 1, '2020-11-09 14:53:58'),
(4, 4, NULL, 'nopekhanafi06@gmail.com', 'e00ee3170fb9e0aadf4b6c51b1244c64', '—Pngtree—delicious_roast_chicken_4572920.png', '8e09dcbb17acb36cb865b561ad900d5d48a774a4', 3, 1, '2020-11-28 15:13:35'),
(5, 5, NULL, 'shakillaaulia1@gmail.com', 'f0f2d00aa7525d598041e83e5f57609d', 'fara2.jpg', 'ef0489582783dba04de82b03c8f91dca615654b5', 3, 1, '2020-11-28 17:55:11'),
(6, NULL, 2, '404notfounddddddd@gmail.com', '5161546e8e70399c98aea0a10629a89a', 'user.jpg', 'a40ce8111b88f03efc3ae2264fccc1a35086b349', 1, 1, '2020-11-30 03:05:44'),
(7, 6, NULL, 'shakillaauliaa26@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'user.jpg', '385ea4f52b36561b2e902328fe117f3770e7998b', 3, 1, '2021-04-07 09:17:05'),
(8, 7, NULL, 'dhanasyai@gmail.com', '202cb962ac59075b964b07152d234b70', '66109_402820083161345_1268235471_n.jpg', 'a260dcaef2b3c9b815645a7c468d6529e40fee99', 3, 1, '2021-04-13 17:00:23'),
(9, 8, NULL, 'shakillaauliaa6@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'user.jpg', 'b7c122ccbcbaf5b6cea3e49971a299153689682a', 3, 1, '2021-07-06 07:52:23'),
(10, 9, NULL, 'x@gmail.com', 'fdb0a9767d5842b5e8274d2ad3d2ea35', 'user.jpg', 'ed6bfab5719accb35f533c08f47e4ec41bc8182d', 3, 1, '2021-07-06 13:57:11'),
(11, 10, NULL, 'christineyenny1@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'user.jpg', '9d2af20e17b30ee61a12e864f7e5bf72cb092160', 3, 1, '2021-07-07 01:03:56'),
(12, 11, NULL, 'shakillafarr@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'user.jpg', '7395aa31a50f4cb7995ee8226510f2785ffde6e0', 3, 1, '2021-07-07 02:11:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acara`
--
ALTER TABLE `acara`
  ADD PRIMARY KEY (`id_acara`);

--
-- Indexes for table `Admins`
--
ALTER TABLE `Admins`
  ADD PRIMARY KEY (`id_Admin`);

--
-- Indexes for table `alur_transaksies`
--
ALTER TABLE `alur_transaksies`
  ADD PRIMARY KEY (`id_alur_transaksi`),
  ADD UNIQUE KEY `id_transaksi` (`id_transaksi`);

--
-- Indexes for table `artikels`
--
ALTER TABLE `artikels`
  ADD PRIMARY KEY (`id_artikel`);

--
-- Indexes for table `email_sents`
--
ALTER TABLE `email_sents`
  ADD PRIMARY KEY (`id_email_sent`);

--
-- Indexes for table `etikets`
--
ALTER TABLE `etikets`
  ADD PRIMARY KEY (`id_etiket`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id_event`);

--
-- Indexes for table `galeries`
--
ALTER TABLE `galeries`
  ADD PRIMARY KEY (`id_galeri`);

--
-- Indexes for table `kabupatens`
--
ALTER TABLE `kabupatens`
  ADD PRIMARY KEY (`id_kab`);

--
-- Indexes for table `kegiatans`
--
ALTER TABLE `kegiatans`
  ADD PRIMARY KEY (`id_kegiatan`);

--
-- Indexes for table `keranjangs`
--
ALTER TABLE `keranjangs`
  ADD PRIMARY KEY (`id_keranjang`);

--
-- Indexes for table `konfigurasies`
--
ALTER TABLE `konfigurasies`
  ADD PRIMARY KEY (`id_konfigurasi`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `produks`
--
ALTER TABLE `produks`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `produk_orders`
--
ALTER TABLE `produk_orders`
  ADD PRIMARY KEY (`id_produk_order`);

--
-- Indexes for table `provinsies`
--
ALTER TABLE `provinsies`
  ADD PRIMARY KEY (`id_prov`);

--
-- Indexes for table `rekenings`
--
ALTER TABLE `rekenings`
  ADD PRIMARY KEY (`id_rekening`);

--
-- Indexes for table `transaksies`
--
ALTER TABLE `transaksies`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `tidak bole sama` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acara`
--
ALTER TABLE `acara`
  MODIFY `id_acara` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `Admins`
--
ALTER TABLE `Admins`
  MODIFY `id_Admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `alur_transaksies`
--
ALTER TABLE `alur_transaksies`
  MODIFY `id_alur_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `artikels`
--
ALTER TABLE `artikels`
  MODIFY `id_artikel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `email_sents`
--
ALTER TABLE `email_sents`
  MODIFY `id_email_sent` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `etikets`
--
ALTER TABLE `etikets`
  MODIFY `id_etiket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id_event` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `galeries`
--
ALTER TABLE `galeries`
  MODIFY `id_galeri` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `kegiatans`
--
ALTER TABLE `kegiatans`
  MODIFY `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `keranjangs`
--
ALTER TABLE `keranjangs`
  MODIFY `id_keranjang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `konfigurasies`
--
ALTER TABLE `konfigurasies`
  MODIFY `id_konfigurasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id_member` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `produks`
--
ALTER TABLE `produks`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `produk_orders`
--
ALTER TABLE `produk_orders`
  MODIFY `id_produk_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `rekenings`
--
ALTER TABLE `rekenings`
  MODIFY `id_rekening` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transaksies`
--
ALTER TABLE `transaksies`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
